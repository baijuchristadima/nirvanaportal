﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Nirvana_Portal.Data.Ghq;
using Portal.Data;
using Portal.Data.Ghq;
using Portal.Service.Interface;
using Portal.UI.Models.Ghq;

namespace Portal.UI.Controllers
{
    [Authorize(Roles = "GhqNtt,GhqUser")]

    public class GhqNttController : Controller
    {
        public IGhqService _IGhqService;
        public IAttachmentService _IAttachmentService;
        public static string ghqFileLocation = @"D:\Attachments\";
        public static string infordocslocation = @"D:\InforDocs\";
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public GhqNttController(IGhqService ghqService, IAttachmentService attachmentService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _IGhqService = ghqService;
            _IAttachmentService = attachmentService;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult GhqDashboard()
        {
            return View("../Ghq/View_Dashboard");
        }
        public IActionResult LpoLoad()
        {
            return View("../Ghq/View_LpoUploadList");
        }
        [Authorize(Roles = "GhqNtt")]
        public IActionResult StaffveriLoad()
        {
            return View("../Ghq/View_StaffveriLoad");
        }
        public IActionResult GhqInvoicesLoad()
        {
            return View("../Ghq/View_InvoiceList");
        }
        public IActionResult ApprovedListLoad()
        {
            return View("../Ghq/View_ApprovedList");
        }
        [Authorize(Roles = "GhqNtt")]
        public IActionResult SoaLoad()
        {
            return View("../Ghq/View_SoaLoad");
        }
        [Authorize(Roles = "GhqNtt")]
        public IActionResult ReportLoad()
        {
            return View("../Ghq/View_ReportLoad");
        }
        public IActionResult FileUpload()
        {
            return View("../Ghq/View_FileUpload");
        }
        public IActionResult InvoiceDetailsVerifiedLoad(long Id = 0)
        {
            var Data = _IGhqService.GetInvoice(Id);
            var model =
            new ghqinvoicemodal
            {
                id = Data.id,
                lpono = Data.lpono==null?"": Data.lpono.ToString(),
                lpodate = Data.lpodate==null?"": Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = Data.invoiceno==null?"": Data.invoiceno.ToString(),
                invoicedate = Data.invoicedate==null?"": Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
            };
            return View("../Ghq/View_VerifiedInvoiceDetails", model);
        }
        public IActionResult InvoiceDetails(long Id = 0)
        {
            var Data = _IGhqService.GetInvoice(Id);
            var model =
            new ghqinvoicemodal
            {
                id = Data.id,
                lpono = Data.lpono == null ? "" : Data.lpono.ToString(),
                lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = Data.invoiceno == null ? "" : Data.invoiceno.ToString(),
                invoicedate = Data.invoicedate == null ? "" : Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
            };
            return View("../Ghq/View_InvoiceDetails", model);
        }

        public IActionResult LpoDetails(long Id = 0)
        {

            var Data = _IGhqService.GetInvoice(Id);
            var model =
            new ghqinvoicemodal
            {
                id = Data.id,
                lpono = Data.lpono == null ? "" : Data.lpono.ToString(),
                lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = Data.invoiceno == null ? "" : Data.invoiceno.ToString(),
                invoicedate = Data.invoicedate == null ? "" : Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
            };
            return View("../Ghq/View_LpoDetails", model);
        }
        [HttpGet]
        public ActionResult GetUploadList(string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "", string lpotodate = "", string lpofromdate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serfromdate) ? DateTime.MinValue : DateTime.ParseExact(serfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(sertodate) ? DateTime.MaxValue : DateTime.ParseExact(sertodate, "dd/MM/yyyy", null);
                DateTime LpoFrom = string.IsNullOrEmpty(lpofromdate) ? DateTime.MinValue : DateTime.ParseExact(lpofromdate, "dd/MM/yyyy", null);
                DateTime LpoTo = string.IsNullOrEmpty(lpotodate) ? DateTime.MaxValue : DateTime.ParseExact(lpotodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetUploadList().OrderByDescending(x => x.uploadedon).Where(x =>
                  (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                  && (x.uploadedon >= From && x.uploadedon<= To) && (x.lpodate >= LpoFrom && x.lpodate<= LpoTo)
                  ).Select(x => new ghqinvoicemodal
                  {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                lpodate = x.lpodate == null ? "" : x.lpodate.Value.ToString("dd-MMM-yyyy"),
                uploaduser = x.uploaduser.fullname.ToString(),
                uploadon = x.uploadedon == null ? "" : x.uploadedon.Value.ToString("dd-MMM-yyyy"),
                uploadcomments=x.uploadedcomments==null?"":x.uploadedcomments.ToString()
                }); 
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetStaffveriLoad(string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serfromdate) ? DateTime.MinValue : DateTime.ParseExact(serfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(sertodate) ? DateTime.MaxValue : DateTime.ParseExact(sertodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetStaffverificationload().OrderByDescending(x => x.createddate).Where(x =>
                (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                && (x.invoicedate >= From && x.invoicedate <= To)
                && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
            ).Select(x => new ghqinvoicemodal
            {
                id = x.id,
                lpono = x.lpono==null?"": x.lpono.ToString(),
                lpodate = x.lpodate==null?"":x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno==null?"": x.invoiceno.ToString(),
                invoicedate = x.invoicedate==null?"": x.invoicedate.Value.ToString("dd-MMM-yyyy"),
            }); ;
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetOpenInvoicesLoad(string serlpono = "", string serinvoiceno = "", string serlpofromdate = "", string serlpotodate = "", string serinvfromdate = "", string serinvtodate = "" , string serpurpose = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime lpoFrom = string.IsNullOrEmpty(serlpofromdate) ? DateTime.MinValue : DateTime.ParseExact(serlpofromdate, "dd/MM/yyyy", null);
                DateTime lpoTo = string.IsNullOrEmpty(serlpotodate) ? DateTime.MaxValue : DateTime.ParseExact(serlpotodate, "dd/MM/yyyy", null);
                DateTime invFrom = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime invTo = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);


                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetCustomerInvoicesLoad().OrderByDescending(x => x.createddate).Where(x =>
             (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.invoicedate >= invFrom && x.invoicedate <= invTo)
              && (x.lpodate >= lpoFrom && x.lpodate <= lpoTo)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
              && (x.purpose == serpurpose || serpurpose == "")
            ).Select(x => new ghqinvoicemodal
            {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                lpodate = x.lpodate == null ? "" : x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                verifiedby = x.accveriuser.fullname.ToString(),
                verifiedon = x.nttverifiedon.Value.ToString("dd-MMM-yyyy"),
                lpopurpose = x.purpose == null ? "": x.purpose.ToString()
            });

                var list = Data.Skip((page - 1) * 250).Take(250).ToList();

                foreach (var item in list)
                {
                    if (item.lpono != null)
                    {
                       
                            string csvPath = ghqFileLocation + Path.GetFileName(item.lpono.Replace(@"/", "").ToString() + ".pdf");
                            if (System.IO.File.Exists(csvPath))
                            {
                                item.IsAttached = true;
                            }
                            else
                            {
                                item.IsAttached = false;
                            }

                       
                    }
                }
                var Results = new
                {
                    Data = list,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetApprovedInvoicesLoad(string serlpono = "", string serinvoiceno = "", string serlpofromdate = "", string serlpotodate = "", string serinvfromdate = "", string serinvtodate = "", string serapprovedfromdate = "", string serapprovedtodate = "", string serpurpose = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                serpurpose = serpurpose ?? "";
                DateTime lpoFrom = string.IsNullOrEmpty(serlpofromdate) ? DateTime.MinValue : DateTime.ParseExact(serlpofromdate, "dd/MM/yyyy", null);
                DateTime lpoTo = string.IsNullOrEmpty(serlpotodate) ? DateTime.MaxValue : DateTime.ParseExact(serlpotodate, "dd/MM/yyyy", null);
                DateTime invFrom = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime invTo = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetCustomerApprovedInvoicesLoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
               && (x.invoicedate >= invFrom && x.invoicedate <= invTo)
               && (x.lpodate >= lpoFrom && x.lpodate <= lpoTo)
               && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.purpose == serpurpose || serpurpose == "")
            ).Select(x => new ghqinvoicemodal
            {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                lpodate = x.lpodate == null ? "" : x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                verifiedby = x.accveriuser.fullname.ToString(),
                verifiedon = x.nttverifiedon.Value.ToString("dd-MMM-yyyy"),
                ghqverifiedby = x.ghqveriuser.fullname.ToString(),
                ghqverifiedon = x.ghqverifiedon.Value.ToString("dd-MMM-yyyy"),

            });

                var list = Data.Skip((page - 1) * 250).Take(250).ToList();

                foreach (var item in list)
                {
                    if (item.invoiceno != null)
                    {
                        try
                        {
                            string csvPath = ghqFileLocation + Path.GetFileName(item.invoiceno.Replace(@"/", "").ToString() + ".pdf");
                            if (System.IO.File.Exists(csvPath))
                            {
                                item.IsAttached = true;
                            }
                            else
                            {
                                item.IsAttached = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        item.IsAttached = false;
                    }
                }

                var Results = new
                {
                    Data = list,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetSoaLoad(string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "", string serlpofromdate = "", string serlpotodate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime invFrom = string.IsNullOrEmpty(serfromdate) ? DateTime.MinValue : DateTime.ParseExact(serfromdate, "dd/MM/yyyy", null);
                DateTime invTo = string.IsNullOrEmpty(sertodate) ? DateTime.MaxValue : DateTime.ParseExact(sertodate, "dd/MM/yyyy", null);
                DateTime lpoFrom = string.IsNullOrEmpty(serlpofromdate) ? DateTime.MinValue : DateTime.ParseExact(serlpofromdate, "dd/MM/yyyy", null);
                DateTime lpoTo = string.IsNullOrEmpty(serlpotodate) ? DateTime.MaxValue : DateTime.ParseExact(serlpotodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x => (x.paystatus == 1) && (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.invoicedate >= invFrom && x.invoicedate<= invTo)
              && (x.lpodate >= lpoFrom && x.lpodate <= lpoTo)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
              ).Select(x => new ghqsoamodal
              {
                  id = x.id,
                  lpono = x.lpono == null ? "" : x.lpono.ToString(),
                  lpodate = x.lpodate == null ? "" : x.lpodate.Value.ToString("dd-MMM-yyyy"),
                  invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                  invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                  debit = x.debit.ToString("N2"),
                  credit=x.credit.ToString("N2"),
                  balance=(x.debit-x.credit).ToString("N2"),
                  lpoid = x.lpoid
              });

                var list = Data.Skip((page - 1) * 250).Take(250).ToList();
                foreach (var item in list)
                {
                    if (item.invoiceno != null)
                    {
                        try
                        {
                            string csvPath = ghqFileLocation + Path.GetFileName(item.invoiceno.Replace(@"/", "").ToString() + ".pdf");
                            if (System.IO.File.Exists(csvPath))
                            {
                                item.IsAttached = true;
                            }
                            else
                            {
                                item.IsAttached = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        item.IsAttached = false;
                    }
                }


                var Results = new
                {
                    Data = list,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetInvoicedetails(int invoiceid = 0, int page = 1)
        {
            var Data = _IGhqService.GetInvoiceDetail(invoiceid).Select(x => new ghqinvoicedetailmodal
            {
                id = x.id,
                code = x.code==null ? "": x.code.ToString(),
                bookno = x.bookno==null?"": x.bookno.ToString(),
                tic_class = x.tic_class==null?"": x.tic_class.ToString(),
                refno = x.refno==null?"": x.refno.ToString(),
                pax = x.pax==null?"": x.pax.ToString(),
                route = x.route==null?"": x.route.ToString(),
                fare = x.fare.ToString("N2"),
                yq = x.yq.ToString("N2"),
                tax = x.tax.ToString("N2"),
                sc = x.sc.ToString("N2"),
                total = (x.fare + x.tax + x.yq + x.sc).ToString("N2"),
                status = x.status,
                ticket_excess_baggage_pices = x.ticket_excess_baggage_pices,
                ticket_excess_baggage = x.ticket_excess_baggage

            }).ToList(); ;

            var datalist = Data.ToList();
            decimal totalfare = 0;
            decimal totalyq = 0;
            decimal totaltax = 0;
            decimal totalsc = 0;
            decimal totalinvoice = 0;
            decimal excesstotalfare = 0;
            decimal excesstotalyq = 0;
            decimal excesstotaltax = 0;
            decimal excesstotalsc = 0;
            decimal excesstotalinvoice = 0;
            if (datalist.Count != 0)
            {
                foreach (var item in datalist)
                {
                    if (item.ticket_excess_baggage == null || item.ticket_excess_baggage == "0" || item.ticket_excess_baggage_pices == null || item.ticket_excess_baggage_pices =="0")
                    {
                        decimal fare = Convert.ToDecimal(item.fare);
                        decimal yq = Convert.ToDecimal(item.yq);
                        decimal tax = Convert.ToDecimal(item.tax);
                        decimal sc = Convert.ToDecimal(item.sc);
                        decimal invoice = Convert.ToDecimal(item.total);
                        totalfare = fare + totalfare;
                        totalyq = yq + totalyq;
                        totaltax = tax + totaltax;
                        totalsc = sc + totalsc;
                        totalinvoice = invoice + totalinvoice;
                    }
                    else
                    {
                        decimal fare = Convert.ToDecimal(item.fare);
                        decimal yq = Convert.ToDecimal(item.yq);
                        decimal tax = Convert.ToDecimal(item.tax);
                        decimal sc = Convert.ToDecimal(item.sc);
                        decimal invoice = Convert.ToDecimal(item.total);
                        excesstotalfare = fare + excesstotalfare;
                        excesstotalyq = yq + excesstotalyq;
                        excesstotaltax = tax + excesstotaltax;
                        excesstotalsc = sc + excesstotalsc;
                        excesstotalinvoice = invoice + excesstotalinvoice;
                    }
                    
                }
            }
            var Results = new
            {
                Data = Data.ToList(),
                page,
                Total = Data.Count(),
                TotalFare = totalfare.ToString("N2"),
                TotalYq = totalyq.ToString("N2"),
                TotalTax = totaltax.ToString("N2"),
                TotalSc = totalsc.ToString("N2"),
                TotalInvoice = totalinvoice.ToString("N2"),
                ExcessTotalFare = excesstotalfare.ToString("N2"),
                ExcessTotalYq = excesstotalyq.ToString("N2"),
                ExcessTotalTax = excesstotaltax.ToString("N2"),
                ExcessTotalSc = excesstotalsc.ToString("N2"),
                ExcessTotalInvoice = excesstotalinvoice.ToString("N2")
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult GetLpodetails(int invoiceid = 0, int page = 1)
        {
            var Data = _IGhqService.GetLpoDetail(invoiceid).Select(x => new ghqlpodetalismodal
            {
                id = x.id,
                lpodate = x.lpodate == null ? "" : x.lpodate.ToString("dd-MMM-yyyy"),
                route = x.route == null ? "" : x.route.ToString(),
                pax = x.pax == null ? "" : x.pax.ToString(),
                refno = x.refno == null ? "" : x.refno.ToString(),
                lpoclass = x.lpoclass == null ? "" : x.lpoclass.ToString(),
                paxdob = x.paxdob == null ? "" : x.paxdob.ToString(),

            }).ToList(); ;
            var Results = new
            {
                Data = Data.ToList(),
                page,
                Total = Data.Count()
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult GetInvoicedetailsVerified(int invoiceid = 0, int page = 1)
        {
            var Data = _IGhqService.GetInvoiceDetail(invoiceid).Where(x => x.status > 0).Select(x => new ghqinvoicedetailmodal
            {
                id = x.id,
                code = x.code == null ? "" : x.code.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                tic_class = x.tic_class == null ? "" : x.tic_class.ToString(),
                refno = x.refno == null ? "" : x.refno.ToString(),
                pax = x.pax == null ? "" : x.pax.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare.ToString("N2"),
                yq = x.yq.ToString("N2"),
                tax = x.tax.ToString("N2"),
                sc = x.sc.ToString("N2"),
                total = (x.fare + x.tax + x.yq + x.sc).ToString("N2"),
                ticket_excess_baggage = x.ticket_excess_baggage,
                ticket_excess_baggage_pices = x.ticket_excess_baggage_pices

            }).ToList(); ;

            var Datas = Data.ToList();
            decimal totfare = 0;
            decimal totyq = 0;
            decimal tottax = 0;
            decimal totalsc = 0;
            decimal totinvoice = 0;
            decimal excesstotfare = 0;
            decimal excesstotyq = 0;
            decimal excesstottax = 0;
            decimal excesstotalsc = 0;
            decimal excesstotinvoice = 0;

            if (Datas.Count != 0)
            {
                foreach (var item in Datas)
                {
                    if (item.ticket_excess_baggage == null || item.ticket_excess_baggage == "0" || item.ticket_excess_baggage == null || item.ticket_excess_baggage_pices == "0")
                    {
                        decimal fare = Convert.ToDecimal(item.fare);
                        decimal yq = Convert.ToDecimal(item.yq);
                        decimal tax = Convert.ToDecimal(item.tax);
                        decimal sc = Convert.ToDecimal(item.sc);
                        decimal invoice = Convert.ToDecimal(item.total);

                        totfare = fare + totfare;
                        totyq = yq + totyq;
                        tottax = tax + tottax;
                        totalsc = sc + totalsc;
                        totinvoice = invoice + totinvoice;
                    }
                    else
                    {
                        decimal fare = Convert.ToDecimal(item.fare);
                        decimal yq = Convert.ToDecimal(item.yq);
                        decimal tax = Convert.ToDecimal(item.tax);
                        decimal sc = Convert.ToDecimal(item.sc);
                        decimal invoice = Convert.ToDecimal(item.total);

                        excesstotfare = fare + excesstotfare;
                        excesstotyq = yq + excesstotyq;
                        excesstottax = tax + excesstottax;
                        excesstotalsc = sc + excesstotalsc;
                        excesstotinvoice = invoice + excesstotinvoice;
                    }
                }
            }


            var Results = new
            {
                Data = Data.ToList(),
                page,
                Total = Data.Count(),
                TotalFare = totfare.ToString("N2"),
                TotalYq = totyq.ToString("N2"),
                TotalTax = tottax.ToString("N2"),
                TotalSc = totalsc.ToString("N2"),
                TotalInvoice = totinvoice.ToString("N2"),
                ExcessTotalFare = excesstotfare.ToString("N2"),
                ExcessTotalYq = excesstotyq.ToString("N2"),
                ExcessTotalTax = excesstottax.ToString("N2"),
                ExcessTotalSc = excesstotalsc.ToString("N2"),
                ExcessTotalInvoice = excesstotinvoice.ToString("N2"),
            };
            return Json(Results);
        }
        [HttpGet]
        public ActionResult GhqInvoicedetails(long id = 0)
        {
            var Data = _IGhqService.GhqInvoicedetails(id).Select(x => new ghqinvoicedetailmodal
            {
                id = x.id,
                code = x.code == null ? "" : x.code.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                tic_class = x.tic_class == null ? "" : x.tic_class.ToString(),
                refno = x.refno == null ? "" : x.refno.ToString(),
                pax = x.pax == null ? "" : x.pax.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare.ToString("N2"),
                yq = x.yq.ToString("N2"),
                tax = x.tax.ToString("N2"),
                sc = x.sc.ToString("N2"),
                total = (x.fare + x.tax + x.yq + x.sc).ToString("N2")

            }).FirstOrDefault();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddInvoiceDetail(ghqinvoicedetailmodal ghqinvoicedetailmodal)
        {
            try
            {
                ghqinvoicedetail obj = new ghqinvoicedetail();
                // var Data = _IGhqService.GhqInvoicedetails(ghqinvoicedetailmodal.id).FirstOrDefault();
                obj.code = ghqinvoicedetailmodal.code;
                obj.bookno = ghqinvoicedetailmodal.bookno;
                obj.tic_class = ghqinvoicedetailmodal.tic_class;
                obj.refno = ghqinvoicedetailmodal.refno;
                obj.pax = ghqinvoicedetailmodal.pax;
                obj.route = ghqinvoicedetailmodal.route;
                obj.fare = Convert.ToDouble(ghqinvoicedetailmodal.fare);
                obj.yq = Convert.ToDouble(ghqinvoicedetailmodal.yq);
                obj.tax = Convert.ToDouble(ghqinvoicedetailmodal.tax);
                obj.sc = Convert.ToDouble(ghqinvoicedetailmodal.sc);
                obj.invoiceid = ghqinvoicedetailmodal.lpoid;
                _IGhqService.AddGhqInvoicedetails(obj);
                return Json("Success");
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoice(ghqinvoicedetailmodal ghqinvoicedetailmodal)
        {
            try
            {
                var Data = _IGhqService.GhqInvoicedetails(ghqinvoicedetailmodal.id).FirstOrDefault();
                Data.code = ghqinvoicedetailmodal.code;
                Data.bookno = ghqinvoicedetailmodal.bookno;
                Data.tic_class = ghqinvoicedetailmodal.tic_class;
                Data.refno = ghqinvoicedetailmodal.refno;
                Data.pax = ghqinvoicedetailmodal.pax;
                Data.route = ghqinvoicedetailmodal.route;
                Data.fare = Convert.ToDouble(ghqinvoicedetailmodal.fare);
                Data.yq = Convert.ToDouble(ghqinvoicedetailmodal.yq);
                Data.tax = Convert.ToDouble(ghqinvoicedetailmodal.tax);
                Data.sc = Convert.ToDouble(ghqinvoicedetailmodal.sc);
                _IGhqService.UpdateGhqInvoicedetails(Data);
                return Json("Success");
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DetleteInvoice(long Id)
        {
            try
            {
                var Data = _IGhqService.GhqInvoicedetails(Id).FirstOrDefault();
                Data.status = 0;
                _IGhqService.UpdateGhqInvoicedetails(Data);
                return Json("Success");
            }
            catch
            {
                throw;
            }
        }
        [HttpGet]
        public ActionResult exportinvoicesload(string selectedIDs)
        {
            string[] Ids = selectedIDs.Split(',');
            try
            {               
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetInvoiceDetail().Where(x => Ids.Contains(x.ghqinvoice.lpono) && x.status > 0 && x.ghqinvoice.ghqverifiedby!=null && x.ghqinvoice.ghqverifiedon!=null)
           .Select(x => new ghqinvoicedetailsexportmodal
            {

                TRAVEL_AGENCY = "2",
                LPO_ID = x.ghqinvoice.lpono == null ? "" : x.ghqinvoice.lpono.ToString(),
                LPO_DATE = x.ghqinvoice.lpodate == null ? "" : x.ghqinvoice.lpodate.ToString(),
                REF_NO = x.refno == null ? "" : x.refno.ToString(),
                INV_NO = x.ghqinvoice.invoiceno == null ? "" : x.ghqinvoice.invoiceno.ToString(),
                INV_DATE = x.ghqinvoice.invoicedate == null ? "" : x.ghqinvoice.invoicedate.ToString(),
                INV_AMOUNT = _IGhqService.GetInvoiceDetail().Where(y => y.invoiceid == x.invoiceid && y.status > 0).Select(y => y.total).Sum().ToString(),
                TIC_NO = x.bookno.ToString(),
                TIC_DATE = x.issuedate.ToString(),
                TIC_AMOUNT = x.total.ToString(),
                TIC_WEIGHT = "",
                TIC_ROUTE=x.route.ToString(),
                TICKET_CLASS=x.tic_class,
                TICKET_AIR_LINE="",
                CITY_CODE="",
                ACCEPTED= "CLEARED",
                REMARKS="",
                UPDATE_STATUS="",
                TKT_TYPE="",
                NAME_ON_TICKET="",
                TICKET_FARE=x.fare.ToString(),
                FUEL_CHARGE=x.yq.ToString(),
                TAX="0",
                SERVICE_CHARGE=x.sc.ToString(),
                DISCOUNT="0",
                TIC_WEIGHT_UNIT="",
                TAX_REGISTRATION_NO= "TRN100294794100003",
                TAX_TOTAL_AMOUNT="0",
                TAX_TYPE= "VAT",
                TAX_RATE= "0",
                TAX_AMOUNT= "0"

           }).ToList();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("TRAVEL_AGENCY,LPO_ID,LPO_DATE,REF_NO,INV_NO,INV_DATE,INV_AMOUNT,TIC_NO,TIC_DATE,TIC_AMOUNT,TIC_WEIGHT,TIC_ROUTE,TICKET_CLASS,TICKET_AIR_LINE,CITY_CODE,ACCEPTED/DISPUTED,REMARKS,UPDATE_STATUS,TKT_TYPE,NAME_ON_TICKET,TICKET_FARE,FUEL_CHARGE,TAX,SERVICE_CHARGE,DISCOUNT,TIC_WEIGHT_UNIT,TAX_REGISTRATION_NO,TAX_TOTAL_AMOUNT,TAX_TYPE,TAX_RATE,TAX_AMOUNT");
                foreach (var rows in Data)
                {
                    stringBuilder.AppendLine($"{rows.TRAVEL_AGENCY},{ rows.LPO_ID},{ rows.LPO_DATE},{ rows.REF_NO},{ rows.INV_NO},{ rows.INV_DATE},{ rows.INV_AMOUNT},{ rows.TIC_NO},{ rows.TIC_DATE},{ rows.TIC_AMOUNT},{ rows.TIC_WEIGHT},{ rows.TIC_ROUTE},{ rows.TICKET_CLASS},{ rows.TICKET_AIR_LINE},{ rows.CITY_CODE},{ rows.ACCEPTED},{ rows.REMARKS},{ rows.UPDATE_STATUS},{ rows.TKT_TYPE},{ rows.NAME_ON_TICKET},{ rows.TICKET_FARE},{ rows.FUEL_CHARGE},{ rows.TAX},{ rows.SERVICE_CHARGE},{ rows.DISCOUNT},{ rows.TIC_WEIGHT_UNIT},{ rows.TAX_REGISTRATION_NO},{ rows.TAX_TOTAL_AMOUNT},{ rows.TAX_TYPE},{ rows.TAX_RATE},{ rows.TAX_AMOUNT}");
                }
                string filename = string.Format("{0}.csv", Ids.Last());
                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", filename);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult exportSoaLoad(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "")
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                  && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
            ).Select(x => new ghqsoamodal
            {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                lpodate= x.lpodate == null ? "" : x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                debit = x.debit.ToString(),
                credit = x.credit.ToString(),
                balance = (x.debit - x.credit).ToString()
            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "SOAList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("SOA-List");

                        worksheet.Range("A1", "G1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "LPO Number";
                        worksheet.Cell(1, 3).Value = "Invoice No";
                        worksheet.Cell(1, 4).Value = "LPO Date";
                        worksheet.Cell(1, 5).Value = "Invoice Date";
                        worksheet.Cell(1, 6).Value = "Debit";
                        worksheet.Cell(1, 7).Value = "Credit";
                        worksheet.Cell(1, 8).Value = "Balance";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].debit;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].credit;
                            worksheet.Cell(index + 1, 8).Value =
                           Data[index - 1].balance;
                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("E" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("G" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("H" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "H").AdjustToContents();
                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetReportLoad(string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "", string serpassenger = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                serpassenger = serpassenger ?? "";
                DateTime From = string.IsNullOrEmpty(serfromdate) ? DateTime.MinValue : DateTime.ParseExact(serfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(sertodate) ? DateTime.MaxValue : DateTime.ParseExact(sertodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetInvoiceDetail().OrderByDescending(x => x.createddate).Where(x =>
                  (x.ghqinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                  && (x.ghqinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                  && (x.ghqinvoice.invoicedate >= From && x.ghqinvoice.invoicedate<= To)
                  && (x.pax.ToLower().Contains(serpassenger.ToLower()) || serpassenger == "")
                )
              .Select(x => new ghqreport
              {
                  invoiceno = x.ghqinvoice.invoiceno.ToString(),
                  invoicedate = x.ghqinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                  lpono = x.ghqinvoice.lpono.ToString(),
                  lpodate = x.ghqinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                  tclass = x.tic_class == null ? "" : x.tic_class.ToString(),
                  name = x.pax == null ? "" : x.pax.ToString(),
                  referenceNo = x.refno == null ? "" : x.refno.ToString(),
                  bookingnumber = x.bookno == null ? "" : x.bookno.ToString(),
                  route = x.route == null ? "" : x.route.ToString(),
                  destination = x.dest == null ? "" : x.dest.ToString(),
                  purpose = x.ghqinvoice.purpose == null ? "" : x.ghqinvoice.purpose.ToString(),
                  fare = x.fare.ToString(),
                  tax = x.tax.ToString(),
                  yq = x.yq.ToString(),
                  service_charge = x.sc.ToString(),
                  total = x.total.ToString(),
                  payStatus = x.ghqinvoice.paystatus.ToString(),
                  status = "",
                  tkt_type = "",
                  pax = x.pax.ToString()
                 
              }); 
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }


        }
        [HttpGet]
        public ActionResult exportReport(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "")
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _IGhqService.GetInvoiceDetail()
              .Select(x => new ghqreport
              {
                  invoiceno = x.ghqinvoice.invoiceno.ToString(),
                  invoicedate = x.ghqinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                  lpono = x.ghqinvoice.lpono.ToString(),
                  lpodate = x.ghqinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                  tclass = x.tic_class == null ? "" : x.tic_class.ToString(),
                  name = x.pax == null ? "" : x.pax.ToString(),
                  referenceNo = x.refno == null ? "" : x.refno.ToString(),
                  bookingnumber = x.bookno == null ? "" : x.bookno.ToString(),
                  route = x.route == null ? "" : x.route.ToString(),
                  destination = x.dest == null ? "" : x.dest.ToString(),
                  purpose = x.ghqinvoice.purpose == null ? "" : x.ghqinvoice.purpose.ToString(),
                  fare = x.fare.ToString(),
                  tax = x.tax.ToString(),
                  yq = x.yq.ToString(),
                  service_charge = x.sc.ToString(),
                  total = x.total.ToString(),
                  payStatus = x.ghqinvoice.paystatus.ToString(),
                  status = "",
                  tkt_type = ""
              }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "InvoiceReport.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("InvoiceReport");
                        worksheet.Range("A1", "G1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Invoice No";
                        worksheet.Cell(1, 3).Value = "invoice Date";
                        worksheet.Cell(1, 4).Value = "Lpo No";
                        worksheet.Cell(1, 5).Value = "Lpo Date";
                        worksheet.Cell(1, 6).Value = "Class";
                        worksheet.Cell(1, 7).Value = "Name";
                        worksheet.Cell(1, 8).Value = "Reference No";
                        worksheet.Cell(1, 9).Value = "Booking Number";
                        worksheet.Cell(1, 10).Value = "Route";
                        worksheet.Cell(1, 11).Value = "Destination";
                        worksheet.Cell(1, 12).Value = "Purpose";
                        worksheet.Cell(1, 13).Value = "Fare";
                        worksheet.Cell(1, 14).Value = "Tax";
                        worksheet.Cell(1, 15).Value = "Yq";
                        worksheet.Cell(1, 16).Value = "Service Charge";
                        worksheet.Cell(1, 17).Value = "Total";
                        worksheet.Cell(1, 18).Value = "Paid";
                        worksheet.Cell(1, 19).Value = "Status";
                        worksheet.Cell(1, 20).Value = "Tkt type";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].tclass;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].name;
                            worksheet.Cell(index + 1, 8).Value =
                           Data[index - 1].referenceNo;


                           worksheet.Cell(index + 1, 9).Value =
                           Data[index - 1].bookingnumber;
                            worksheet.Cell(index + 1, 10).Value =
                           Data[index - 1].route;
                            worksheet.Cell(index + 1, 11).Value =
                           Data[index - 1].destination;
                            worksheet.Cell(index + 1, 12).Value =
                           Data[index - 1].purpose;
                            worksheet.Cell(index + 1, 13).Value =
                           Data[index - 1].fare;
                            worksheet.Cell(index + 1, 14).Value =
                           Data[index - 1].tax;
                            worksheet.Cell(index + 1, 15).Value =
                           Data[index - 1].yq;

                            worksheet.Cell(index + 1, 16).Value =
                           Data[index - 1].service_charge;
                            worksheet.Cell(index + 1, 17).Value =
                           Data[index - 1].total;
                            worksheet.Cell(index + 1, 18).Value =
                           Data[index - 1].payStatus;
                            worksheet.Cell(index + 1, 19).Value =
                          Data[index - 1].status;
                            worksheet.Cell(index + 1, 20).Value =
                         Data[index - 1].tkt_type;
                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("E" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("G" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("H" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "H").AdjustToContents();
                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult attachmentcopy(string invoiceno)
        {
            if (invoiceno.Length > 0)
            {
                try
                {
                    string csvPath = ghqFileLocation + Path.GetFileName(invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    if (System.IO.File.Exists(csvPath))
                    {
                        return PhysicalFile(csvPath, "text/plain", invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    }
                    else
                    {
                        return null;
                    }
                  
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }

        [HttpGet]
        public bool GetAttachment(string invoiceno)
        {
            if (invoiceno.Length > 0)
            {
                try
                {
                    string csvPath = ghqFileLocation + Path.GetFileName(invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    if (System.IO.File.Exists(csvPath))
                    {
                        return true;
                       // return PhysicalFile(csvPath, "text/plain", invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    }
                    else
                    {
                        return false;
                       // return null;
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return false;
                //return null;
            }

        }

        [HttpGet]
        public ActionResult Getinforattachments(long id = 0)
        {
            var Data = _IAttachmentService.Getinforattachments().Where(x => x.invoiceid == id && x.type == "Ghq").Select(x => new
            {
                path = x.path
            }).Distinct().ToList();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }
        [HttpGet]
        public ActionResult infordocdownload(string file)
        {
            if (file.Length > 0)
            {
                try
                {
                    string csvPath = infordocslocation + Path.GetFileName(file.ToString());
                    return PhysicalFile(csvPath, "text/plain", file);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApproveInvoice(ghqinvoicemodal ghqinvoicemodal)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _IGhqService.GetStaffverificationload().Where(x => x.invoiceno == ghqinvoicemodal.invoiceno).FirstOrDefault();
                Data.nttverifiedby = user.Id.ToString();
                Data.nttverifiedon = DateTime.Now;
                Data.status = 2;
                _IGhqService.Updatecorporateinvoice(Data);
                ViewBag.Section = "Success";

                var model =
                new ghqinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono == null ? "" : Data.lpono,
                    lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno == null ? "" : Data.invoiceno,
                    invoicedate = Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../Ghq/View_InvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GhqApproveInvoice(ghqinvoicemodal ghqinvoicemodal)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _IGhqService.GetCustomerInvoicesLoad().Where(x => x.invoiceno == ghqinvoicemodal.invoiceno).FirstOrDefault();
                Data.ghqverifiedby = user.Id.ToString();
                Data.ghqverifiedon = DateTime.Now;
                Data.status = 2;
                _IGhqService.Updatecorporateinvoice(Data);
                ViewBag.Section = "Success";

                var model =
                new ghqinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono == null ? "" : Data.lpono,
                    lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno == null ? "" : Data.invoiceno,
                    invoicedate = Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../Ghq/View_VerifiedInvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IActionResult> GhqRejectInvoiceVerified(string invno)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _IGhqService.GetCustomerInvoicesLoad().Where(x => x.invoiceno == invno).FirstOrDefault();
                //Data.ghqverifiedby = user.Id.ToString();
                //Data.ghqverifiedon = DateTime.Now;
                //Data.status = 2;
                Data.ghqverifiedby = null;
                Data.ghqverifiedon = null;
                Data.invoiceno = null;
                Data.invoicedate = null;
                Data.status = 0;
                Data.rejdate = DateTime.Now;
                Data.rejuser = user.Id.ToString();
                Data.rejreason = "";
                _IGhqService.Updatecorporateinvoice(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IActionResult> GhqRejectInvoice(string invno)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _IGhqService.GetStaffverificationload().Where(x => x.invoiceno == invno).FirstOrDefault();
                Data.ghqverifiedby = null;
                Data.ghqverifiedon = null;
                Data.invoiceno = null;
                Data.invoicedate = null;
                Data.status = 0;
                Data.rejdate = DateTime.Now;
                Data.rejuser = user.Id.ToString();
                Data.rejreason = "";
                _IGhqService.Updatecorporateinvoice(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoiceMain(ghqinvoicemodal ghqinvoicemodal)
        {
            try
            {
                var Data = _IGhqService.GetStaffverificationload().Where(x => x.invoiceno == ghqinvoicemodal.invoiceno).FirstOrDefault();
                if (Data != null)
                {
                    Data.lpono = ghqinvoicemodal.lpono;
                    Data.lpodate = string.IsNullOrEmpty(ghqinvoicemodal.lpodate) ? (DateTime?)null : DateTime.ParseExact(ghqinvoicemodal.lpodate, "dd-MMM-yyyy", null);
                    Data.invoicedate = string.IsNullOrEmpty(ghqinvoicemodal.invoicedate) ? (DateTime?)null : DateTime.ParseExact(ghqinvoicemodal.invoicedate, "dd-MMM-yyyy", null);
                    _IGhqService.Updatecorporateinvoice(Data);
                }

                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult UploadFile(string GhqTicketInvoiceMain,List<IFormFile> file)
        {
            foreach (var i in file)
            {
              
                var ext  = Path.GetExtension(i.FileName);
                if (ext == ".csv")
                {
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "Documents/LPCSV/");
                    var fname = Path.GetFileNameWithoutExtension(i.FileName);

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    FileInfo fileInfo = new FileInfo(i.FileName);

                    string fileName = fname + fileInfo.Extension;

                    string fileNameWithPath = Path.Combine(path, fileName);

                    using (var stream = new FileStream(fileNameWithPath, FileMode.Create))
                    {
                        i.CopyTo(stream);
                    }
                    //DataTable dataTable = GhqTourController.readCSV(fileNameWithPath, ';');

                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[8] { 
                    new DataColumn("LPO_NO", typeof(string)),
                    new DataColumn("LPO_DATE", typeof(string)),
                    new DataColumn("TICKET_ROUTE", typeof(string)),
                    new DataColumn("PASSENGER_NAME", typeof(string)),
                    new DataColumn("REFERENCE_NO", typeof(string)),
                    new DataColumn("TICKET_CLASS", typeof(string)),
                    new DataColumn("PASSENGER_DOB", typeof(string)),
                    new DataColumn("LPO_PURPOSE",typeof(string)) });

                    var contents = System.IO.File.ReadAllText(fileNameWithPath);
                  
                    foreach (string row in contents.Split('\n'))
                    {
                       
                        if (!string.IsNullOrEmpty(row))
                        {
                            int i1 = 0;
                            dt.Rows.Add();
                           
                            //Execute a loop over the columns.
                            foreach (string cell in row.Split(';'))
                            {

                                dt.Rows[dt.Rows.Count - 1][i1] = cell.ToString();
                                i1++;
                            }
                        }
                    }


                   if (dt != null && dt.Rows.Count > 0)
                    {
                        
                        if (dt.Columns.Count >= 8)
                        {
                            for (int n = 1; n <= dt.Rows.Count - 1; n++)
                            {
                                ghqinvoice main = new ghqinvoice();
                                main.lpono = dt.Rows[n]["LPO_NO"].ToString();
                                main.purpose = dt.Rows[n]["LPO_PURPOSE"].ToString();
                                string lpodate = dt.Rows[n]["LPO_DATE"].ToString();
                                main.lpodate = string.IsNullOrEmpty(lpodate) ? (DateTime?)null : Convert.ToDateTime(lpodate);
                                main.uploadedby =  "6d528d47-0943-4054-9384-64cff1b0dd83";
                                main.uploadedon = DateTime.Now;
                                main.uploadedcomments = GhqTicketInvoiceMain;
                                var Data = _IGhqService.GetUploadList();
                                string[] the_array = Data.Select(i => i.lpono.ToString()).ToArray();

                                var LPONOs = dt.AsEnumerable().Select(s => s.Field<string>("LPO_NO")).Distinct().ToList();
                                bool has = the_array.Contains(main.lpono);
                                if (has == true)
                                {
                                    var item = Data.Where(s => s.lpono == main.lpono).ToList();
                                    if (item[0].id > 0)
                                    {
                                        ghqticketlpodetails GhqTicketLpoDetails = new ghqticketlpodetails();
                                        GhqTicketLpoDetails.lpoid = item[0].id;
                                        
                                        GhqTicketLpoDetails.lpodate = Convert.ToDateTime(dt.Rows[n]["LPO_DATE"]);
                                        GhqTicketLpoDetails.lpoclass = dt.Rows[n]["TICKET_CLASS"].ToString();
                                        GhqTicketLpoDetails.pax = dt.Rows[n]["PASSENGER_NAME"].ToString();
                                        GhqTicketLpoDetails.refno = dt.Rows[n]["REFERENCE_NO"].ToString();
                                        GhqTicketLpoDetails.route = dt.Rows[n]["TICKET_ROUTE"].ToString();
                                        string paxdate = dt.Rows[n]["PASSENGER_DOB"].ToString();
                                       // GhqTicketLpoDetails.paxdob = string.IsNullOrEmpty(paxdate) ? (DateTime?)null : Convert.ToDateTime(paxdate);
                                         GhqTicketLpoDetails.paxdob = dt.Rows[n]["PASSENGER_DOB"].ToString();
                                        GhqTicketLpoDetails.status = 1;
                                        var retVal = _IGhqService.AddGhqticketlpodetails(GhqTicketLpoDetails);

                                    }

                                }
                                else
                                {

                                    ghqinvoice retVal1 = _IGhqService.AddGhqinvoice(main);

                                    if (retVal1.id > 0)
                                    {
                                        ghqticketlpodetails GhqTicketLpoDetails = new ghqticketlpodetails();
                                        GhqTicketLpoDetails.lpoid = retVal1.id;
                                        string lpdate = dt.Rows[n]["LPO_DATE"].ToString();
                                       // GhqTicketLpoDetails.lpodate = string.IsNullOrEmpty(lpdate) ? (DateTime?)null : Convert.ToDateTime(lpdate);
                                        GhqTicketLpoDetails.lpodate = Convert.ToDateTime(dt.Rows[n]["LPO_DATE"]);
                                        GhqTicketLpoDetails.lpoclass = dt.Rows[n]["TICKET_CLASS"].ToString();
                                        GhqTicketLpoDetails.pax = dt.Rows[n]["PASSENGER_NAME"].ToString();
                                        GhqTicketLpoDetails.refno = dt.Rows[n]["REFERENCE_NO"].ToString();
                                        GhqTicketLpoDetails.route = dt.Rows[n]["TICKET_ROUTE"].ToString();
                                        string paxdate = dt.Rows[n]["PASSENGER_DOB"].ToString();
                                      
                                        
                                        GhqTicketLpoDetails.paxdob = dt.Rows[n]["PASSENGER_DOB"].ToString();
                                        GhqTicketLpoDetails.status = 1;
                                        var retVal = _IGhqService.AddGhqticketlpodetails(GhqTicketLpoDetails);

                                    }
                                }
                            }
                        }
                   }                
                        DataTable ds = dt;
                }
                else
                {
                    string path = Path.Combine(Directory.GetCurrentDirectory(), "Documents/LPDOC/");
                    var fname = Path.GetFileNameWithoutExtension(i.FileName);
                    //create folder if not exist
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    FileInfo fileInfo = new FileInfo(i.FileName);
                    string fileName = fname + fileInfo.Extension;

                    string fileNameWithPath = Path.Combine(path, fileName);

                    using (var stream = new FileStream(fileNameWithPath, FileMode.Create))
                    {
                        i.CopyTo(stream);
                    }
                }              
            }
            return View("../Ghq/View_LpoUploadList");
        }
    }

}

