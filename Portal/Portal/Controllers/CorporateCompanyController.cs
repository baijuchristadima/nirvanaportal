﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Portal.Data.Migrations;
using Portal.Service.Interface;
using Portal.UI.Models.Corporate;

namespace Portal.UI.Controllers
{
    [Authorize(Roles = "CorporateUser")]
    public class CorporateCompanyController : Controller
    {
        public static string CorporateFileLocation = @"D:\Attachments\";
       
        public ICorporateService _ICorporateService;
        public CorporateCompanyController(ICorporateService corporateService)
        {
            _ICorporateService = corporateService;
        }

        public IActionResult InvoicesLoad()
            {
                return View("../CorporateCompany/View_InvoicesLoad");
            }
            public IActionResult SoaLoad()
            {
                return View("../CorporateCompany/View_SoaLoad");
            }
            public IActionResult VerifiedInvoiceDetails()
            {
                return View("../CorporateCompany/View_VerifiedInvoiceDetails");
            }
            [HttpGet]
            public ActionResult GetInvoicesLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serpaystatus = 0, int page = 1)
            {
                try
                {
                    serlpono = serlpono ?? "";
                    serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                    var Data = _ICorporateService.GetCustomerInvoicesLoad().OrderBy(x => x.invoicedate).ThenBy(x=>x.invoiceno).Where(x =>
                  (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                   && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                   && (x.paystatus == serpaystatus || serpaystatus == 0)
                   && (x.invoicedate >= From && x.invoicedate <= To)
                   && (x.companyid == sercustomer || sercustomer == 0)
                ).Select(x => new corporateinvoicemodal
                {
                    id = x.id,
                    companyname = x.corporatecompany.name.ToString(),
                    lpono = x.lpono==null?"":x.lpono.ToString(),
                    lpodate = x.lpodate==null?"": x.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = x.invoiceno==null?"": x.invoiceno.ToString(),
                    invoicedate = x.invoicedate==null?"": x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                    verifiedby = x.verifieduser.fullname.ToString(),
                    verifiedon = x.verifiedon.Value.ToString("dd-MMM-yyyy"),
                    paystatus = x.paystatus
                });
                    var Results = new
                    {
                        Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                        page,
                        Total = Data.Count()
                    };
                    return Json(Results);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            [HttpGet]
            public ActionResult GetSoaLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", string companycode = "", int page = 1)
            {
                try
                {
                    serlpono = serlpono ?? "";
                    serinvoiceno = serinvoiceno ?? "";
                companycode = companycode ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                    var Data = _ICorporateService.GetSOALoad().OrderBy(x => x.corporateinvoice.invoicedate).Where(x =>
                  (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                    && (x.corporateinvoice.invoicedate >= From && x.corporateinvoice.invoicedate <= To)
                    && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                    && (x.corporateinvoice.companyid == sercustomer || sercustomer == 0)
                    && (x.paystatus == 1)
                    && (x.corporateinvoice.corporatecompany.code.ToLower().Contains(companycode.ToLower()) || companycode == "")
                ).Select(x => new corporatesoamodal
                {
                    id = x.id,
                    companyname = x.corporateinvoice.corporatecompany.name.ToString(),
                    companycode = x.corporateinvoice.corporatecompany.code.ToString(),
                    lpono = x.corporateinvoice.lpono==null?"": x.corporateinvoice.lpono.ToString(),
                    lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = x.corporateinvoice.invoiceno==null?"": x.corporateinvoice.invoiceno.ToString(),
                    invoicedate = x.corporateinvoice.invoicedate==null?"": x.corporateinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                    debit=x.debit.ToString("N2"),
                    credit =x.credit.ToString("N2"),
                    balance = (x.debit-x.credit).ToString("N2"),
                    invoiceid=x.corporateinvoice.id

                });

                var Datas = Data.Skip((page - 1) * 250).Take(250).ToList();
                decimal amount = 0;
                if (Datas.Count != 0)
                {
                    foreach (var item in Datas)
                    {
                        decimal balance = Convert.ToDecimal(item.balance);
                        amount = balance + amount;
                    }
                }
                var Results = new
                    {
                        Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                        page,
                        Total = Data.Count(),
                        TotOS = amount.ToString("#0.00")
                };
                    return Json(Results);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            [HttpGet]
            public ActionResult GetCustomer()
            {
                var Data = _ICorporateService.GetCorporateLoad().Where(x => x.status > 0).ToList();
                var Results = new
                {
                    Data = Data
                };
                return Json(Results);

            }

        [HttpGet]
        public ActionResult InvoiceDetailsVerified(int invoiceid = 0, int page = 1)
        {

            try
            {
                var Data = _ICorporateService.GetInvoice(invoiceid);
                var model =
                new corporateinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono==null?"":Data.lpono.ToString(),
                    lpodate = Data.lpodate==null?"": Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno==null?"": Data.invoiceno.ToString(),
                    invoicedate = Data.invoicedate==null?"": Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../CorporateCompany/View_VerifiedInvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           

        }
        [HttpGet]
        public ActionResult GetInvoicedetailsVerified(int invoiceid = 0, int page = 1)
        {
           try
            {
                var Data = _ICorporateService.GetInvoiceDetail(invoiceid).Where(x => x.status > 0).Select(x => new corporateinvoicedetailsmodal
                {
                    servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                    bookno = x.bookno == null ? "" : x.bookno.ToString(),
                    guest = x.guest == null ? "" : x.guest,
                    from = x.from == null ? "" : x.from.Value.ToString("dd-MMM-yyyy"),
                    to = x.to == null ? "" : x.to.Value.ToString("dd-MMM-yyyy"),
                    provider = x.provider == null ? "" : x.provider.ToString(),
                    route = x.route == null ? "" : x.route.ToString(),
                    fare = x.fare.ToString("N2"),
                    tax = x.tax.ToString("N2"),
                    vat = x.vat.ToString("N2"),
                    sc = x.sc.ToString("N2"),
                    total = (x.fare + x.tax + x.vat + x.sc).ToString("N2")

                }).ToList(); ;
                var Results = new
                {
                    Data = Data.ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        [HttpGet]
        public ActionResult exportinvoicesload(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serpaystatus = 0)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetInvoiceDetail().OrderBy(x => x.corporateinvoice.invoicedate).ThenBy(x => x.corporateinvoice.invoiceno).Where(x =>
                (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                && (x.corporateinvoice.paystatus == serpaystatus || serpaystatus == 0)
                && (x.corporateinvoice.invoicedate >= From && x.corporateinvoice.invoicedate <= To)
                && (x.corporateinvoice.companyid == sercustomer || sercustomer == 0) && x.status > 0 && x.corporateinvoice.verifiedby != null && x.corporateinvoice.verifiedon != null && x.corporateinvoice.status > 1
            ).Select(x => new corporateinvoicedetailsexportmodal
            {
                companyname = x.corporateinvoice.corporatecompany.name,
                lpono = x.corporateinvoice.lpono == null ? "" : x.corporateinvoice.lpono.ToString(),
                lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MM-yyyy"),
                invoiceno = x.corporateinvoice.invoiceno == null ? "" : x.corporateinvoice.invoiceno.ToString(),
                invoicedate = x.corporateinvoice.invoicedate==null?"": x.corporateinvoice.invoicedate.Value.ToString("dd-MM-yyyy"),
                servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                guest = x.guest == null ? "" : x.guest.ToString(),
                from = x.from == null ? "" : x.from.Value.ToString("dd-MMM-yyyy"),
                to = x.to == null ? "" : x.to.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider == null ? "" : x.provider.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare,
                tax = x.tax,
                vat = x.vat,
                sc = x.sc,
                total = (x.fare + x.tax + x.vat + x.sc),
                issuedate = x.issuedate == null ? "" : x.issuedate.Value.ToString("dd-MMM-yyyy"),
                type = x.type == null?"":x.type.ToString()

            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "InvoiceList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("Invoice-List");
                        worksheet.Range("A1", "S1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Company Name";
                        worksheet.Cell(1, 3).Value = "LPO Number";
                        worksheet.Cell(1, 4).Value = "LPO Date";
                        worksheet.Cell(1, 5).Value = "Invoice No";
                        worksheet.Cell(1, 6).Value = "Invoice Date";
                        worksheet.Cell(1, 7).Value = "Servicetype";
                        worksheet.Cell(1, 8).Value = "Voucher#";
                        worksheet.Cell(1, 9).Value = "Guest";
                        worksheet.Cell(1, 10).Value = "Service From";
                        worksheet.Cell(1, 11).Value = "Service To";
                        worksheet.Cell(1, 12).Value = "Provider";
                        worksheet.Cell(1, 13).Value = "Route/City";
                        worksheet.Cell(1, 14).Value = "Type";
                        worksheet.Cell(1, 15).Value = "Fare";
                        worksheet.Cell(1, 16).Value = "Tax";
                        worksheet.Cell(1, 17).Value = "Vat";
                        worksheet.Cell(1, 18).Value = "SC";
                        worksheet.Cell(1, 19).Value = "Total";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].companyname;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].servicetype;


                            worksheet.Cell(index + 1, 8).Value = Data[index - 1].bookno;
                            worksheet.Cell(index + 1, 9).Value = Data[index - 1].guest;
                            worksheet.Cell(index + 1, 10).Value = Data[index - 1].from;
                            worksheet.Cell(index + 1, 11).Value = Data[index - 1].to;
                            worksheet.Cell(index + 1, 12).Value = Data[index - 1].provider;
                            worksheet.Cell(index + 1, 13).Value = Data[index - 1].route;

                            worksheet.Cell(index + 1, 14).Value = Data[index - 1].type;

                            worksheet.Cell(index + 1, 15).Value = Data[index - 1].fare;
                            worksheet.Cell(index + 1, 16).Value = Data[index - 1].tax;
                            worksheet.Cell(index + 1, 17).Value = Data[index - 1].vat;
                            worksheet.Cell(index + 1, 18).Value = Data[index - 1].sc;
                            worksheet.Cell(index + 1, 19).Value = Data[index - 1].total;





                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";

                            worksheet.Cell("J" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("K" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";

                            worksheet.Cell("O" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("P" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("Q" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("R" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("S" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "S").AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult exportSoaLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "",string serinvtodate="", string sertodate = "")
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetSOALoad().OrderBy(x => x.corporateinvoice.invoicedate).ThenBy(x=>x.corporateinvoice.invoiceno).Where(x =>
              (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                && (x.corporateinvoice.invoicedate >= From && x.corporateinvoice.invoicedate <= To)
                && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                && (x.corporateinvoice.companyid == sercustomer || sercustomer == 0)
            ).Select(x => new corporatesoamodal
            {
                id = x.id,
                companyname = x.corporateinvoice.corporatecompany.name,
                lpono = x.corporateinvoice.lpono==null?"": x.corporateinvoice.lpono.ToString(),
                lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MM-yyyy"),
                invoiceno = x.corporateinvoice.invoiceno==null?"": x.corporateinvoice.invoiceno.ToString(),
                invoicedate = x.corporateinvoice.invoicedate==null?"": x.corporateinvoice.invoicedate.Value.ToString("dd-MM-yyyy"),
                debit = x.debit.ToString(),
                credit = x.credit.ToString(),
                balance = (x.debit-x.credit).ToString()
            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "SOAList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("SOA-List");

                        worksheet.Range("A1", "I1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Company Name";
                        worksheet.Cell(1, 3).Value = "LPO Number";
                        worksheet.Cell(1, 4).Value = "LPO Date";
                        worksheet.Cell(1, 5).Value = "Invoice No";
                        worksheet.Cell(1, 6).Value = "Invoice Date";
                        worksheet.Cell(1, 7).Value = "Debit";
                        worksheet.Cell(1, 8).Value = "Credit";
                        worksheet.Cell(1, 9).Value = "Balance";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].companyname;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].debit;

                            worksheet.Cell(index + 1, 8).Value =
                            Data[index - 1].credit;
                            worksheet.Cell(index + 1, 9).Value =
                            Data[index - 1].balance;
                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("G" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("H" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("I" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "I").AdjustToContents();
                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public ActionResult GetRMDetails(long id = 0)
        {
            var Data = _ICorporateService.GetrmdatavalueDetail().Where(x => x.corporateInvoicedetail.invoiceid == id).Select(x => new
            {
                code = x.rmname,
                value = x.rmvalue
            }).Distinct().ToList();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }
      
        [HttpGet]
        public ActionResult attachmentcopy(string invoiceno)
        {
            if (invoiceno.Length > 0)
            {
                try
                {
                    string csvPath = CorporateFileLocation + Path.GetFileName(invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    return PhysicalFile(csvPath, "text/plain", invoiceno.Replace(@"/", "").ToString()+".pdf");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }
    }
}
