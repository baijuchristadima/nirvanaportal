﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Portal.Data;
using Portal.UI.Models.Settings;

namespace Portal.UI.Controllers
{
    public class SettingsController : Controller
    {        
        private readonly ILogger<SettingsController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public SettingsController(ILogger<SettingsController> logger,  UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _logger = logger;           
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }       
        [HttpPost]
        [ValidateAntiForgeryToken]
        private async Task<IActionResult> CreateRole(UsertypeModel UsertypeModel)
        {
            bool x = await _roleManager.RoleExistsAsync(UsertypeModel.name);
            if (!x)
            {                  
                var role = new IdentityRole();
                role.Name = UsertypeModel.name;
                IdentityResult result= await _roleManager.CreateAsync(role);
               if(result.Succeeded)
                    return View(UsertypeModel);
            }
            return View(UsertypeModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(RegistrationModel UseregistrationModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser appUser = new ApplicationUser
                {
                    UserName = UseregistrationModel.Name,
                    Email = UseregistrationModel.Email
                };

                IdentityResult result = await _userManager.CreateAsync(appUser, UseregistrationModel.Password);
                if (result.Succeeded)
                {                    
                    var rolresult= await _userManager.AddToRoleAsync(appUser, "Corporate");
                    if(rolresult.Succeeded)
                    return RedirectToAction("Index");
                }                  
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError("", error.Description);
                }
            }
            return View(UseregistrationModel);
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View("../Settings/View_RegisterNewUser");
        }
    }
}