﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using Portal.Data;
using Portal.Data.Corporate;
using Portal.Service.Interface;
using Portal.UI.Models.Corporate;

namespace Portal.UI.Controllers
{
    [Authorize(Roles = "CorporateNtt")]
    public class CorporateController : Controller
    {
        public ICorporateService _ICorporateService;
        public IAttachmentService _IAttachmentService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public static string CorporateFileLocation = @"D:\Attachments\";
        public static string infordocslocation = @"D:\InforDocs\";
        public CorporateController(ICorporateService corporateService, IAttachmentService attachmentService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _ICorporateService = corporateService;
            _IAttachmentService = attachmentService;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult CorporateLoad()
        {
            return View("../Corporate/View_CorporateLoad");
        }
        public IActionResult StaffveriLoad()
        {
            return View("../Corporate/View_StaffveriLoad");
        }
        public IActionResult InvoicesLoad()
        {
            return View("../Corporate/View_InvoicesLoad");
        }
        public IActionResult SoaLoad()
        {
            return View("../Corporate/View_SoaLoad");
        }       
        [HttpGet]
        public ActionResult GetCorporateLoad(string sercode = "", string sername = "", string serregno = "", string serinvformat="", string sercurrencyId = "", int page = 1)
        {
            try
            {
                sercode = sercode ?? "";
                sername = sername ?? "";
                serregno = serregno ?? "";
                serinvformat = serinvformat ?? "";
                sercurrencyId = sercurrencyId ?? "";               
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetCorporateLoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.code.ToLower().Contains(sercode.ToLower()) || sercode == "")
              && (x.name.ToLower().Contains(sername.ToLower()) || sername == "")
              && (x.invformat.ToLower().Contains(serinvformat.ToLower()) || serinvformat == "")
              && (x.regno.ToLower().Contains(serregno.ToLower()) || serregno == "")
              && (x.currency.ToLower().Contains(sercurrencyId.ToLower()) || sercurrencyId == "")
            ).OrderByDescending(x=>x.createddate).Select(x => new corporatecompanymodal
            {
                id = x.id,
                code = x.code==null?"": x.code.ToString(),
                name =x.name==null?"": x.name.ToString(),
                address = x.firstaddress==null?"": x.firstaddress.ToString(),
                secaddress=x.secondaddress==null?"":x.secondaddress.ToString(),
                thirdaddress=x.thirdaddress==null?"": x.thirdaddress.ToString(),
                forthaddress=x.forthaddress==null ? "" : x.forthaddress.ToString(),
                currency=x.currency==null?"": x.currency.ToString(),
                phone = x.phone==null?"": x.phone.ToString(),
                mail = x.mail==null?"": x.mail.ToString(),
                vat= x.regno==null?"": x.regno.ToString(),
                invformat = x.invformat==null?"": x.invformat.ToString(),              
                status = x.status              
            }); ;
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 150).Take(150).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetStaffveriLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetStaffverificationload().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.companyid == sercustomer || sercustomer == 0) && x.corporatecompany.status>0
            ).Select(x => new corporateinvoicemodal
            {
                id = x.id,
                companyname = x.corporatecompany.name.ToString(),
                lpono = x.lpono==null ? "":x.lpono.ToString(),
                lpodate = x.lpodate==null? "": x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno==null? "": x.invoiceno.ToString(),
                invoicedate = x.invoicedate==null? "":x.invoicedate.Value.ToString("dd-MMM-yyyy"),
            }); ;
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetInvoicesLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serpaystatus = 0, int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetCustomerInvoicesLoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.companyid == sercustomer || sercustomer == 0)
               && (x.paystatus == serpaystatus || serpaystatus == 0) && x.status>1
            ).Select(x => new corporateinvoicemodal
            {

                id = x.id,
                companyname = x.corporatecompany.name.ToString(),
                lpono = x.lpono==null?"" : x.lpono.ToString(),
                lpodate = x.lpodate==null?"": x.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.invoiceno==null?"":x.invoiceno.ToString(),
                invoicedate = x.invoicedate==null?"": x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                verifiedby = x.verifieduser.fullname.ToString(),
                verifiedon = x.verifiedon.Value.ToString("dd-MMM-yyyy"),
                paystatus = x.paystatus
            });
                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult GetSoaLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", string companycode= "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                companycode = companycode ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
                  && (x.corporateinvoice.invoicedate >= From && x.corporateinvoice.invoicedate <= To)
               && (x.corporateinvoice.corporatecompany.id == sercustomer || sercustomer == 0)
                && (x.corporateinvoice.corporatecompany.code.ToLower().Contains(companycode.ToLower()) || companycode == "")
            ).Select(x => new corporatesoamodal
            {
                id = x.id,
                companyname = x.corporateinvoice.corporatecompany.name.ToString(),
                companycode = x.corporateinvoice.corporatecompany.code.ToString(),
                lpono = x.corporateinvoice.lpono==null?"" : x.corporateinvoice.lpono.ToString(),
                lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.corporateinvoice.invoiceno==null?"": x.corporateinvoice.invoiceno.ToString(),
                invoicedate = x.corporateinvoice.invoicedate==null?"": x.corporateinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                debit = x.debit.ToString("N2"),
                credit = x.credit.ToString("N2"),
                balance = (x.debit - x.credit).ToString("N2"),
                invoiceid=x.corporateinvoice.id

            }); ;
                var Datas = Data.Skip((page - 1) * 250).Take(250).ToList();
                decimal amount = 0;
                if (Datas.Count != 0)
                {                 
                    foreach (var item in Datas)
                    {
                        decimal balance = Convert.ToDecimal(item.balance);
                        amount = balance + amount;
                    }
                }

                var Results = new
                {
                    Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    page,
                    Total = Data.Count(),
                    TotOS = amount.ToString("#0.00")
                };
             
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult exportSoaLoad(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "")
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.corporateinvoice.companyid == sercustomer || sercustomer == 0)
            ).Select(x => new corporatesoamodal
            {
                id = x.id,
                companyname = x.corporateinvoice.corporatecompany.name.ToString(),
                lpono = x.corporateinvoice.lpono==null?"": x.corporateinvoice.lpono.ToString(),
                lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.corporateinvoice.invoiceno==null?"": x.corporateinvoice.invoiceno.ToString(),
                invoicedate = x.corporateinvoice.invoicedate==null ?"":x.corporateinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                debit = x.debit.ToString(),
                credit = x.credit.ToString(),
                balance = (x.debit - x.credit).ToString(),
            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "SOAList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("SOA-List");

                        worksheet.Range("A1", "J1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Company Name";
                        worksheet.Cell(1, 3).Value = "LPO Number";
                        worksheet.Cell(1, 4).Value = "LPO Date";
                        worksheet.Cell(1, 5).Value = "Invoice No";
                        worksheet.Cell(1, 6).Value = "Invoice Date";
                        worksheet.Cell(1, 7).Value = "Debit";
                        worksheet.Cell(1, 8).Value = "Credit";
                        worksheet.Cell(1, 9).Value = "Balance";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value =index;                          
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].companyname;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].debit;
                            worksheet.Cell(index + 1, 8).Value =
                            Data[index - 1].credit;
                            worksheet.Cell(index + 1, 9).Value =
                            Data[index - 1].balance;
                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("G" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("H" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("I" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "J").AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public ActionResult exportinvoicesload(long sercustomer = 0, string serlpono = "", string serinvoiceno = "", string serfromdate = "", string sertodate = "", long serpaystatus = 0)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetInvoiceDetail().OrderBy(x => x.invoiceid).Where(x =>
              (x.corporateinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
              && (x.corporateinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
              && (x.corporateinvoice.paystatus == serpaystatus || serpaystatus == 0)
              && (x.corporateinvoice.companyid == sercustomer || sercustomer == 0) && x.status>0 && x.corporateinvoice.status>1 && x.corporateinvoice.verifiedby!=null && x.corporateinvoice.verifiedon!=null
            ).Select(x => new corporateinvoicedetailsexportmodal
            {

                companyname = x.corporateinvoice.corporatecompany.name.ToString(),
                lpono = x.corporateinvoice.lpono==null?"": x.corporateinvoice.lpono.ToString(),
                lpodate = x.corporateinvoice.lpodate==null?"": x.corporateinvoice.lpodate.Value.ToString("dd-MMM-yyyy"),
                invoiceno = x.corporateinvoice.invoiceno==null?"" : x.corporateinvoice.invoiceno.ToString(),
                invoicedate = x.corporateinvoice.invoicedate==null?"": x.corporateinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                servicetype = x.servicetype==null?"": x.servicetype.ToString(),
                bookno = x.bookno==null?"": x.bookno.ToString(),
                guest = x.guest==null?"": x.guest.ToString(),
                from =x.from==null? "" : x.from.Value.ToString("dd-MMM-yyyy"),
                to =   x.to==null?  "" : x.to.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider==null?"": x.provider.ToString(),
                route = x.route==null?"": x.route.ToString(),
                fare = x.fare,
                tax = x.tax,
                vat = x.vat,
                sc = x.sc,
                total = (x.fare + x.tax + x.vat + x.sc)

            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "InvoiceList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("Invoice-List");
                        worksheet.Range("A1", "S1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Company Name";
                        worksheet.Cell(1, 3).Value = "LPO Number";
                        worksheet.Cell(1, 4).Value = "LPO Date";
                        worksheet.Cell(1, 5).Value = "Invoice No";
                        worksheet.Cell(1, 6).Value = "Invoice Date";
                        worksheet.Cell(1, 7).Value = "Servicetype";
                        worksheet.Cell(1, 8).Value = "Voucher#";
                        worksheet.Cell(1, 9).Value = "Guest";
                        worksheet.Cell(1, 10).Value = "Service From";
                        worksheet.Cell(1, 11).Value = "Service To";
                        worksheet.Cell(1, 12).Value = "Provider";
                        worksheet.Cell(1, 13).Value = "Route/City";
                        worksheet.Cell(1, 14).Value = "Type";
                        worksheet.Cell(1, 15).Value = "Fare";
                        worksheet.Cell(1, 16).Value = "Tax";
                        worksheet.Cell(1, 17).Value = "Vat";
                        worksheet.Cell(1, 18).Value = "SC";
                        worksheet.Cell(1, 19).Value = "Total";


                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].companyname;
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].lpono;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].lpodate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].servicetype;


                            worksheet.Cell(index + 1, 8).Value = Data[index - 1].bookno;
                            worksheet.Cell(index + 1, 9).Value = Data[index - 1].guest;
                            worksheet.Cell(index + 1, 10).Value = Data[index - 1].from;
                            worksheet.Cell(index + 1, 11).Value = Data[index - 1].to;
                            worksheet.Cell(index + 1, 12).Value = Data[index - 1].provider;
                            worksheet.Cell(index + 1, 13).Value = Data[index - 1].route;
                            worksheet.Cell(index + 1, 14).Value = Data[index - 1].type;

                            worksheet.Cell(index + 1, 15).Value = Data[index - 1].fare;
                            worksheet.Cell(index + 1, 16).Value = Data[index - 1].tax;
                            worksheet.Cell(index + 1, 17).Value = Data[index - 1].vat;
                            worksheet.Cell(index + 1, 18).Value = Data[index - 1].sc;
                            worksheet.Cell(index + 1, 19).Value = Data[index - 1].total;





                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("F" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";

                            worksheet.Cell("J" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";
                            worksheet.Cell("K" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";

                            worksheet.Cell("O" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("P" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("Q" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("R" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("S" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "S").AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult downloadcorporate(string sercode = "", string sername = "", string serregno = "", string serinvformat = "", string sercurrencyId = "")
        {
            try
            {
                sercode = sercode ?? "";
                sername = sername ?? "";

                serregno = serregno ?? "";
                serinvformat = serinvformat ?? "";
                sercurrencyId = sercurrencyId ?? "";
                
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _ICorporateService.GetCorporateLoad().OrderByDescending(x => x.createddate).Where(x =>
               (x.code.ToLower().Contains(sercode.ToLower()) || sercode == "")
              && (x.name.ToLower().Contains(sername.ToLower()) || sername == "")
              && (x.invformat.ToLower().Contains(serinvformat.ToLower()) || serinvformat == "")
               && (x.regno.ToLower().Contains(serregno.ToLower()) || serregno == "")
              && (x.currency.ToLower().Contains(sercurrencyId.ToLower()) || sercurrencyId == "")
            ).Select(x => new corporatecompanymodal
            {
                id = x.id,
                code = x.code == null ? "" : x.code.ToString(),
                name = x.name == null ? "" : x.name.ToString(),
                address = x.firstaddress == null ? "" : x.firstaddress.ToString(),
                secaddress = x.secondaddress == null ? "" : x.secondaddress.ToString(),
                thirdaddress = x.thirdaddress == null ? "" : x.thirdaddress.ToString(),
                forthaddress = x.forthaddress == null ? "" : x.forthaddress.ToString(),
                currency = x.currency == null ? "" : x.currency.ToString(),
                phone = x.phone == null ? "" : x.phone.ToString(),
                mail = x.mail == null ? "" : x.mail.ToString(),
                vat = x.regno == null ? "" : x.regno.ToString(),
                invformat = x.invformat == null ? "" : x.invformat.ToString(),
                status = x.status
            }).ToList(); 

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "CorporateList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("Corporate-List");
                        worksheet.Range("A1", "M1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";
                        worksheet.Cell(1, 2).Value = "Company Code";
                        worksheet.Cell(1, 3).Value = "Company Name";
                        worksheet.Cell(1, 4).Value = "Address 1 ";
                        worksheet.Cell(1, 5).Value = "Addrees 2";
                        worksheet.Cell(1, 6).Value = "Address 3";
                        worksheet.Cell(1, 7).Value = "Address 4";
                        worksheet.Cell(1, 8).Value = "Phone";
                        worksheet.Cell(1, 9).Value = "Email";
                        worksheet.Cell(1, 10).Value = "Invoice Format";
                        worksheet.Cell(1, 11).Value = "Currency";                       
                        worksheet.Cell(1, 12).Value = "Registation#";                        
                        worksheet.Cell(1, 13).Value = "Status";

                        for (int index = 1; index <= Data.Count; index++)
                        {
                            var Status = Data[index - 1].status == 1 ? "Active" : "Inactive";
                            worksheet.Cell(index + 1, 1).Value = index;
                            worksheet.Cell(index + 1, 2).Value =Data[index - 1].code;
                            worksheet.Cell(index + 1, 3).Value = Data[index - 1].name;
                            worksheet.Cell(index + 1, 4).Value = Data[index - 1].address;
                            worksheet.Cell(index + 1, 5).Value = Data[index - 1].secaddress;
                            worksheet.Cell(index + 1, 6).Value = Data[index - 1].thirdaddress;
                            worksheet.Cell(index + 1, 7).Value = Data[index - 1].forthaddress;
                            worksheet.Cell(index + 1, 8).Value = Data[index - 1].phone;
                            worksheet.Cell(index + 1, 9).Value = Data[index - 1].mail;
                            worksheet.Cell(index + 1, 10).Value = Data[index - 1].invformat;
                            worksheet.Cell(index + 1, 11).Value = Data[index - 1].currency;
                            worksheet.Cell(index + 1, 12).Value = Data[index - 1].vat;
                            worksheet.Cell(index + 1, 13).Value = Status;
                        }
                        worksheet.Columns("A", "M").AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [HttpGet]
        public ActionResult GetCustomer()
        {
            var Data = _ICorporateService.GetCorporateLoad().Where(x => x.status > 0).ToList();
            var Results = new
            {
                Data = Data
            };
            return Json(Results);

        }
        [HttpGet]
        public ActionResult InvoiceDetails(int invoiceid = 0, int page = 1)
        {
           try
            {
                var Data = _ICorporateService.GetInvoice(invoiceid);
                var model =
                new corporateinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono==null?"": Data.lpono,
                    lpodate = Data.lpodate==null?"":Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno==null?"" : Data.invoiceno,
                    invoicedate = Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../Corporate/View_InvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpGet]
        public ActionResult InvoiceDetailsVerified(int invoiceid = 0, int page = 1)
        {
            try
            {
                var Data = _ICorporateService.GetInvoice(invoiceid);
                var model =
                new corporateinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono == null ? "" : Data.lpono,
                    lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno == null ? "" : Data.invoiceno,
                    invoicedate = Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../Corporate/View_VerifiedInvoiceDetails", model);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        [HttpGet]
        public ActionResult GetInvoicedetailsVerified(int invoiceid = 0,int page=1)
        {
           try
            {
                var Data = _ICorporateService.GetInvoiceDetail(invoiceid).Where(x=>x.status>0).Select(x => new corporateinvoicedetailsmodal
                {
                    servicetype = x.servicetype==null?"": x.servicetype.ToString(),
                    bookno =x.bookno==null?"": x.bookno.ToString(),
                    guest = x.guest==null?"":x.guest.ToString(),
                    from =x.from==null?"": x.from.Value.ToString("dd-MMM-yyyy"),
                    to =x.to==null?"": x.to.Value.ToString("dd-MMM-yyyy"),
                    provider = x.provider==null?"":x.provider.ToString(),
                    route = x.route==null?"":x.route.ToString(),
                    fare = x.fare.ToString("N2"),
                    tax = x.tax.ToString("N2"),
                    vat = x.vat.ToString("N2"),
                    sc = x.sc.ToString("N2"),
                    total = (x.fare + x.tax + x.vat + x.sc).ToString("N2")

                }).ToList(); ;

                var Datas = Data.ToList();
                decimal totalfare = 0;
                decimal totaltax = 0;
                decimal totalsc = 0;
                decimal totalinvoice = 0;
                if (Datas.Count != 0)
                {
                    foreach (var item in Datas)
                    {
                        decimal fare = Convert.ToDecimal(item.fare);
                        decimal tax = Convert.ToDecimal(item.tax);
                        decimal sc = Convert.ToDecimal(item.sc);
                        decimal invoice = Convert.ToDecimal(item.total);
                        totalfare = fare + totalfare;
                        totaltax = tax + totaltax;
                        totalsc = sc + totalsc;
                        totalinvoice = invoice + totalinvoice;
                    }
                }

                var Results = new
                {
                    Data = Data.ToList(),
                    page,
                    Total = Data.Count(),
                    TotalFare = totalfare.ToString("N2"),
                    TotalTax = totaltax.ToString("N2"),
                    TotalSc = totalsc.ToString("N2"),
                    TotalInvoice = totalinvoice.ToString("N2")
                };
                return Json(Results);
            }
    
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult GetInvoicedetails(int invoiceid = 0, int page = 1)
        {
            var Data = _ICorporateService.GetInvoiceDetail(invoiceid).OrderBy(x=>x.billinginvoiceLine).Select(x => new corporateinvoicedetailsmodal
            {
                id=x.id,
                status=x.status,
                servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                guest = x.guest == null ? "" : x.guest.ToString(),
                from = x.from == null ? "" : x.from.Value.ToString("dd-MMM-yyyy"),
                to = x.to == null ? "" : x.to.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider == null ? "" : x.provider.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare.ToString("N2"),
                tax = x.tax.ToString("N2"),
                vat = x.vat.ToString("N2"),
                sc = x.sc.ToString("N2"),
                total = (x.fare + x.tax + x.vat + x.sc).ToString("N2")
            }).ToList(); ;

            var Datas = Data.ToList();
            decimal totalfare = 0;
            decimal totaltax = 0;
            decimal totalsc = 0;
            decimal totalinvoice = 0;

            if (Datas.Count != 0)
            {
                foreach (var item in Datas)
                {
                    decimal fare = Convert.ToDecimal(item.fare);
                    decimal tax = Convert.ToDecimal(item.tax);
                    decimal sc = Convert.ToDecimal(item.sc);
                    decimal invoice = Convert.ToDecimal(item.total);
                    totalfare = fare + totalfare;
                    totaltax = tax + totaltax;
                    totalsc = sc + totalsc;
                    totalinvoice = invoice + totalinvoice;
                }
            }

            var Results = new
            {
                Data = Data.ToList(),
                page,
                Total = Data.Count(),
                TotalFare = totalfare.ToString("N2"),
                TotalTax = totaltax.ToString("N2"),
                TotalSc = totalsc.ToString("N2"),
                TotalInvoice = totalinvoice.ToString("N2")
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult CorporateInvoicedetails(long id = 0)
        {
            var Data = _ICorporateService.GetInvoiceDetail().Where(x => x.id == id).Select(x => new corporateinvoicedetailsmodal
            {
                id = x.id,
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                from = x.from == null ? "" : x.from.Value.ToString("dd-MMM-yyyy"),
                to = x.to == null ? "" : x.to.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider == null ? "" : x.provider.ToString(),
                guest = x.guest == null ? "" : x.guest.ToString(),
                servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                fare = x.fare.ToString(),
                tax = x.tax.ToString(),
                vat = x.vat.ToString(),
                sc = x.sc.ToString(),
                total = (x.fare + x.tax + x.vat + x.sc).ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                billinginvoiceLine = x.billinginvoiceLine,
                issuedate = x.issuedate == null ? "" : x.issuedate.Value.ToString("dd-MMM-yyyy"),
                type = x.type == null ? "" : x.type.ToString(),
                status=x.status
            }) 
            .FirstOrDefault();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult companydetails(long id = 0)
        {
            var Data = _ICorporateService.GetCorporateLoad().Where(x => x.id == id).Select(x => new corporatecompanymodal
            {
                id = x.id,
                code = x.code == null ? "" : x.code.ToString(),
                name = x.name == null ? "" : x.name.ToString(),
                address = x.firstaddress == null ? "" : x.firstaddress.ToString(),
                secaddress = x.secondaddress == null ? "" : x.secondaddress.ToString(),
                thirdaddress = x.thirdaddress == null ? "" : x.thirdaddress.ToString(),
                forthaddress = x.forthaddress == null ? "" : x.forthaddress.ToString(),
                currency = x.currency == null ? "" : x.currency.ToString(),
                phone = x.phone == null ? "" : x.phone.ToString(),
                mail = x.mail == null ? "" : x.mail.ToString(),
                vat = x.regno == null ? "" : x.regno.ToString(),
                invformat = x.invformat == null ? "" : x.invformat,
                status=x.status 
            }).FirstOrDefault();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoice(corporateinvoicedetailsmodal corporateinvoicedetailsmodal)
        {
            try
            {
                var Data = _ICorporateService.GetInvoiceDetail().Where(x=>x.id== corporateinvoicedetailsmodal.id).FirstOrDefault();               
               if(Data==null)
                {
                    corporateInvoicedetail corporateInvoicedetail = new corporateInvoicedetail();
                    corporateInvoicedetail.bookno = corporateinvoicedetailsmodal.bookno;
                    corporateInvoicedetail.from = string.IsNullOrEmpty(corporateinvoicedetailsmodal.from) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicedetailsmodal.from, "dd-MMM-yyyy", null);
                    corporateInvoicedetail.to = string.IsNullOrEmpty(corporateinvoicedetailsmodal.to) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicedetailsmodal.to, "dd-MMM-yyyy", null);
                    corporateInvoicedetail.provider = corporateinvoicedetailsmodal.provider;
                    corporateInvoicedetail.guest = corporateinvoicedetailsmodal.guest;
                    corporateInvoicedetail.servicetype = corporateinvoicedetailsmodal.servicetype;
                    corporateInvoicedetail.fare = Convert.ToDouble(corporateinvoicedetailsmodal.fare);
                    corporateInvoicedetail.tax = Convert.ToDouble(corporateinvoicedetailsmodal.tax);
                    corporateInvoicedetail.vat = Convert.ToDouble(corporateinvoicedetailsmodal.vat);
                    corporateInvoicedetail.sc = Convert.ToDouble(corporateinvoicedetailsmodal.sc);
                    corporateInvoicedetail.route = corporateinvoicedetailsmodal.route;
                    corporateInvoicedetail.billinginvoiceLine = corporateinvoicedetailsmodal.billinginvoiceLine;
                    corporateInvoicedetail.issuedate = string.IsNullOrEmpty(corporateinvoicedetailsmodal.issuedate) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicedetailsmodal.issuedate, "dd-MMM-yyyy", null); 
                    corporateInvoicedetail.type = corporateinvoicedetailsmodal.type;
                    corporateInvoicedetail.invoiceid = corporateinvoicedetailsmodal.invoiceid;
                    corporateInvoicedetail.status = corporateinvoicedetailsmodal.status;
                    _ICorporateService.AddInvoicedetails(corporateInvoicedetail);
                }                    
               else
                {
                    Data.bookno = corporateinvoicedetailsmodal.bookno;
                    Data.provider = corporateinvoicedetailsmodal.provider;
                    Data.guest = corporateinvoicedetailsmodal.guest;
                    Data.servicetype = corporateinvoicedetailsmodal.servicetype;
                    Data.fare = Convert.ToDouble(corporateinvoicedetailsmodal.fare);
                    Data.tax = Convert.ToDouble(corporateinvoicedetailsmodal.tax);
                    Data.vat = Convert.ToDouble(corporateinvoicedetailsmodal.vat);
                    Data.sc = Convert.ToDouble(corporateinvoicedetailsmodal.sc);
                    Data.route = corporateinvoicedetailsmodal.route;
                    Data.billinginvoiceLine = corporateinvoicedetailsmodal.billinginvoiceLine;
                    Data.issuedate = string.IsNullOrEmpty(corporateinvoicedetailsmodal.issuedate)? (DateTime?)null :  DateTime.ParseExact(corporateinvoicedetailsmodal.issuedate, "dd-MMM-yyyy", null);
                    Data.type = corporateinvoicedetailsmodal.type;
                    Data.status= corporateinvoicedetailsmodal.status;
                    Data.from = string.IsNullOrEmpty(corporateinvoicedetailsmodal.from) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicedetailsmodal.from, "dd-MMM-yyyy", null);
                    Data.to = string.IsNullOrEmpty(corporateinvoicedetailsmodal.to) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicedetailsmodal.to, "dd-MMM-yyyy", null);
                    _ICorporateService.UpdateInvoicedetails(Data);
                }                
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DetleteInvoice(long Id)
        {
            try
            {
                var Data = _ICorporateService.GetInvoiceDetail().Where(x=>x.id==Id).FirstOrDefault();
                Data.status = 0;
                _ICorporateService.UpdateInvoicedetails(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DetleteCompany(long Id)
        {
            try
            {
                var Data = _ICorporateService.GetCorporateLoad().Where(x => x.id == Id).FirstOrDefault();
                Data.status = 0;
                _ICorporateService.Updatecorporatecompany(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateCorporate(corporatecompanymodal corporatecompanymodal)
        {

            if (corporatecompanymodal.vat == null)
            {
                corporatecompanymodal.vat = "";
            }
            try
            {
                var Data = _ICorporateService.GetCorporateLoad().Where(x => x.id == corporatecompanymodal.id).FirstOrDefault();
                Data.code = corporatecompanymodal.code;
                Data.name = corporatecompanymodal.name;
                Data.firstaddress = corporatecompanymodal.address;                
                Data.secondaddress = corporatecompanymodal.secaddress;
                Data.thirdaddress = corporatecompanymodal.thirdaddress;
                Data.forthaddress = corporatecompanymodal.forthaddress;
                Data.phone = corporatecompanymodal.phone;
                Data.mail = corporatecompanymodal.mail;
                Data.invformat = corporatecompanymodal.invformat;
                Data.currency = corporatecompanymodal.currency;
                Data.regno = corporatecompanymodal.vat;
                Data.status = corporatecompanymodal.status;
                _ICorporateService.Updatecorporatecompany(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  async Task<IActionResult> ApproveInvoice(corporateinvoicemodal corporateinvoicemodal)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _ICorporateService.GetStaffverificationload().Where(x=>x.invoiceno== corporateinvoicemodal.invoiceno).FirstOrDefault();
                Data.verifiedby = user.Id.ToString();
                Data.verifiedon = DateTime.Now;
                Data.status = 2;
                _ICorporateService.Updatecorporateinvoice(Data);
                ViewBag.Section = "Success";
               
                var model =
                new corporateinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono == null ? "" : Data.lpono,
                    lpodate = Data.lpodate == null ? "" : Data.lpodate.Value.ToString("dd-MMM-yyyy"),
                    invoiceno = Data.invoiceno == null ? "" : Data.invoiceno,
                    invoicedate = Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                };
                return View("../Corporate/View_InvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        [HttpGet]
        public ActionResult GetRMDetails(long id = 0)
        {
            var Data = _ICorporateService.GetrmdatavalueDetail().Where(x => x.corporateInvoicedetail.invoiceid == id).Select(x => new
            {
                code = x.rmname,
                id = x.id,
                value = x.rmvalue
            }).Distinct().ToList();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateRMValue(long RmId, string RMvalue)
        {
            try
            {
                var Data = _ICorporateService.GetrmdatavalueDetail().Where(x => x.id == RmId).FirstOrDefault();
                Data.rmvalue = RMvalue;
                _ICorporateService.Updatermdatavalue(Data);
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet]
        public ActionResult attachmentcopy(string invoiceno)
        {
            if (invoiceno.Length > 0)
            {
                try
                {
                    string csvPath = CorporateFileLocation + Path.GetFileName(invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    return PhysicalFile(csvPath, "text/plain", invoiceno.Replace(@"/", "").ToString() + ".pdf");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }

        [HttpGet]
        public ActionResult Getinforattachments(long id = 0)
        {
            var Data = _IAttachmentService.Getinforattachments().Where(x => x.invoiceid == id && x.type== "Corporate").Select(x => new
            {
                path = x.path              
            }).Distinct().ToList();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult infordocdownload(string file)
        {
            if (file.Length > 0)
            {
                try
                {
                    string csvPath = infordocslocation + Path.GetFileName(file.ToString());
                    return PhysicalFile(csvPath, "text/plain", file);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoiceMain(corporateinvoicemodal corporateinvoicemodal)
        {
            try
            {
                var Data = _ICorporateService.GetStaffverificationload().Where(x => x.invoiceno == corporateinvoicemodal.invoiceno).FirstOrDefault();
                if(Data!=null)
                {
                    Data.lpono = corporateinvoicemodal.lpono;
                    Data.lpodate = string.IsNullOrEmpty(corporateinvoicemodal.lpodate) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicemodal.lpodate, "dd-MMM-yyyy", null); 
                    Data.invoicedate = string.IsNullOrEmpty(corporateinvoicemodal.invoicedate) ? (DateTime?)null : DateTime.ParseExact(corporateinvoicemodal.invoicedate, "dd-MMM-yyyy", null);
                    _ICorporateService.Updatecorporateinvoice(Data);
                }
               
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
