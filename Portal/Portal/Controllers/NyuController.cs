﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Nirvana_Portal.Data.Nyu;
using Portal.Data;
using Portal.Service.Interface;
using Portal.UI.Models.Nyu;

namespace Portal.UI.Controllers
{
    [Authorize(Roles = "NyuUser,NyuNtt")]
    public class NyuController : Controller
    {
        public INyuService _INyuService;
        public IAttachmentService _IAttachmentService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public static string nyuFileLocation = @"D:\Attachments\";
        public static string infordocslocation = @"D:\InforDocs\";
        public NyuController(INyuService nyuService, IAttachmentService attachmentService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _INyuService = nyuService;
            _IAttachmentService = attachmentService;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        [HttpGet]
        public IActionResult StaffveriLoad()
        {
            return View("../Nyu/View_StaffveriLoad");
        }
        [HttpGet]
        public IActionResult OpenInvoicesLoad()
        {
            return View("../Nyu/View_OpenInvoicesLoad");
        }
        [HttpGet]
        public IActionResult SoaLoad()
        {
            return View("../Nyu/View_SoaLoad");
        }
        [HttpGet]
        public IActionResult ArchievesInvoicesLoad()
        {
            return View("../Nyu/View_ArchievesInvoicesLoad");
        }

        [HttpGet]
        public ActionResult GetStaffveriLoad(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serinvstatus = 0, int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _INyuService.GetStaffverificationload().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
               && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
              && (x.status == serinvstatus || serinvstatus == 0)
            ).Select(x => new nyuinvoicemodal
            {
                id = x.id,
                lpono = x.lpono==null?"":x.lpono.ToString(),
                invoiceno = x.invoiceno==null?"": x.invoiceno.ToString(),
                invoicedate = x.invoicedate==null?"": x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                travelcode= x.travelcode==null?"":x.travelcode.ToString(),
                authcode=x.authcode==null?"":x.authcode.ToString(),
                status = x.status
            }); ;

                var datalist = Data.Skip((page - 1) * 250).Take(250).ToList();
                foreach (var item in datalist)
                {
                    var details = _INyuService.GetInvoiceDetail(item.id).Select(x => new nyuinvoicedetailmodal
                    {
                        servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                        bookno = x.bookno == null ? "" : x.bookno.ToString(),
                        pax = x.pax == null ? "" : x.pax.ToString(),
                        fromdate = x.fromdate == null ? "" : x.fromdate.Value.ToString("dd-MMM-yyyy"),
                        todate = x.todate == null ? "" : x.todate.Value.ToString("dd-MMM-yyyy"),
                        provider = x.provider == null ? "" : x.provider.ToString(),
                        route = x.route == null ? "" : x.route.ToString(),
                        fare = x.fare.ToString("N2"),
                        tax = x.tax.ToString("N2"),
                        vat = x.vat.ToString("N2"),
                        total = x.total.ToString("N2"),
                        authcode = x.authcode == null ? "" : x.authcode.ToString(),
                        account = x.account == null ? "" : x.account.ToString(),
                        fund = x.fund == null ? "" : x.fund.ToString(),
                        org = x.org == null ? "" : x.org.ToString(),
                        project = x.project == null ? "" : x.project.ToString(),
                        program = x.program == null ? "" : x.program.ToString()
                    }).ToList(); ;

                    item.nyuinvoicedetailmodal = details;
                }


                var Results = new
                {
                    //Data = Data.Skip((page - 1) * 250).Take(250).ToList(),
                    Data = datalist,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult GetOpenInvoicesLoad(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _INyuService.GetCustomerInvoicesLoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
               && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.paystatus == 1)
            ).Select(x => new nyuinvoicemodal
            {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                verifiedby = x.verifieduser.fullname,
                verifiedon = x.verifiedon.Value.ToString("dd-MMM-yyyy"),
            });

                var list = Data.Skip((page - 1) * 250).Take(250).ToList();

                foreach (var item in list)
                {
                    if (item.invoiceno != null)
                    {
                       
                            string csvPath = nyuFileLocation + Path.GetFileName(item.invoiceno.Replace(@"/", "").ToString() + ".pdf");
                        if (System.IO.File.Exists(csvPath))
                        {
                            item.IsAttached = true;
                        }
                        else
                        {
                            item.IsAttached = false;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                var Results = new
                {
                    Data = list,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult GetArchieveInvoicesLoad(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serpaystatus = 0, int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _INyuService.GetCustomerInvoicesLoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
               && (x.invoicedate >= From && x.invoicedate <= To)
              && (x.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.paystatus == serpaystatus || serpaystatus == 0)
            ).Select(x => new nyuinvoicemodal
            {
                id = x.id,
                lpono = x.lpono == null ? "" : x.lpono.ToString(),
                invoiceno = x.invoiceno == null ? "" : x.invoiceno.ToString(),
                invoicedate = x.invoicedate == null ? "" : x.invoicedate.Value.ToString("dd-MMM-yyyy"),
                verifiedby = x.verifieduser.fullname,
                verifiedon = x.verifiedon.Value.ToString("dd-MMM-yyyy"),
                paystatus = x.paystatus
            });

                var list = Data.Skip((page - 1) * 250).Take(250).ToList();

                foreach (var item in list)
                {
                    if (item.invoiceno != null)
                    {

                        string csvPath = nyuFileLocation + Path.GetFileName(item.invoiceno.Replace(@"/", "").ToString() + ".pdf");
                        if (System.IO.File.Exists(csvPath))
                        {
                            item.IsAttached = true;
                        }
                        else
                        {
                            item.IsAttached = false;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                var Results = new
                {
                    Data = list,
                    page,
                    Total = Data.Count()
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult GetSoaLoad(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", int page = 1)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _INyuService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x =>
                (x.nyuinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                    && (x.nyuinvoice.invoicedate >= From && x.nyuinvoice.invoicedate <= To)
                && (x.nyuinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
              ).Select(x => new nyusoamodal
              {
                  id = x.id,
                  lpono = x.nyuinvoice.lpono == null ? "" : x.nyuinvoice.lpono.ToString(),
                  invoiceno = x.nyuinvoice.invoiceno == null ? "" : x.nyuinvoice.invoiceno.ToString(),
                  invoicedate = x.nyuinvoice.invoicedate == null ? "" : x.nyuinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                  debit = x.debit.ToString("N2"),
                  credit = x.credit.ToString("N2"),
                  balance = (x.debit-x.credit).ToString("N2"),
                  invoiceid =x.nyuinvoice.id
              });

                var Datas = Data.Skip((page - 1) * 250).Take(250).ToList();
                decimal amount = 0;
                if (Datas.Count != 0)
                {
                    foreach (var item in Datas)
                    {
                        decimal balance = Convert.ToDecimal(item.balance);
                        amount = balance + amount;
                    }
                }

                foreach (var item in Datas)
                {
                    if (item.invoiceno != null)
                    {

                        string csvPath = nyuFileLocation + Path.GetFileName(item.invoiceno.Replace(@"/", "").ToString() + ".pdf");
                        if (System.IO.File.Exists(csvPath))
                        {
                            item.IsAttached = true;
                        }
                        else
                        {
                            item.IsAttached = false;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                var Results = new
                {
                    Data = Datas,
                    page,
                    Total = Data.Count(),
                    TotalAmt = amount.ToString("N2")
                };
                return Json(Results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult exportSoaLoad( string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "")
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;
                var Data = _INyuService.GetSOALoad().OrderByDescending(x => x.createddate).Where(x =>
              (x.nyuinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
                  && (x.nyuinvoice.invoicedate >= From && x.nyuinvoice.invoicedate <= To)
              && (x.nyuinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")              
            ).Select(x => new nyusoamodal
            {
                id = x.id,
                lpono = x.nyuinvoice.lpono == null ? "" : x.nyuinvoice.lpono.ToString(),
                invoiceno = x.nyuinvoice.invoiceno == null ? "" : x.nyuinvoice.invoiceno.ToString(),
                invoicedate = x.nyuinvoice.invoicedate == null ? "" : x.nyuinvoice.invoicedate.Value.ToString("dd-MMM-yyyy"),
                debit = x.debit.ToString(),
                credit = x.credit.ToString(),
                balance = (x.debit - x.credit).ToString()
            }).ToList();

                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = "SOAList.xlsx";
                try
                {
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet =
                        workbook.Worksheets.Add("SOA-List");

                        worksheet.Range("A1", "G1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(1, 1).Value = "Sl#";                      
                        worksheet.Cell(1, 2).Value = "LPO Number";                        
                        worksheet.Cell(1, 3).Value = "Invoice No";
                        worksheet.Cell(1, 4).Value = "Invoice Date";
                        worksheet.Cell(1, 5).Value = "Debit";
                        worksheet.Cell(1, 6).Value = "Credit";
                        worksheet.Cell(1, 7).Value = "Balance";
                        for (int index = 1; index <= Data.Count; index++)
                        {
                            worksheet.Cell(index + 1, 1).Value = index;                           
                            worksheet.Cell(index + 1, 2).Value =
                            Data[index - 1].lpono;                          
                            worksheet.Cell(index + 1, 3).Value =
                            Data[index - 1].invoiceno;
                            worksheet.Cell(index + 1, 4).Value =
                            Data[index - 1].invoicedate;
                            worksheet.Cell(index + 1, 5).Value =
                            Data[index - 1].debit;
                            worksheet.Cell(index + 1, 6).Value =
                            Data[index - 1].credit;
                            worksheet.Cell(index + 1, 7).Value =
                            Data[index - 1].balance;
                            worksheet.Cell("D" + (index + 1)).Style.DateFormat.Format = "dd-MMM-yyyy";                           
                            worksheet.Cell("E" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("F" + (index + 1)).Style.NumberFormat.Format = "0.00";
                            worksheet.Cell("G" + (index + 1)).Style.NumberFormat.Format = "0.00";
                        }
                        worksheet.Columns("A", "G").AdjustToContents();
                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public IActionResult InvoiceDetailsVerifiedLoad(long invoiceid = 0)
        {
           try
            {
                var Data = _INyuService.GetInvoice(invoiceid);
                var model =
                new nyuinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono==null?"": Data.lpono.ToString(),
                    invoiceno = Data.invoiceno==null?"": Data.invoiceno.ToString(),
                    invoicedate = Data.invoicedate==null?"": Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                    travelcode = Data.travelcode==null ? "" : Data.travelcode.ToString(),
                    authcode = Data.authcode==null ? "": Data.authcode.ToString()
                };
                return View("../Nyu/View_VerifiedInvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        [HttpGet]
        public IActionResult InvoiceDetails(long invoiceid = 0)
        {
           try
            {
                var Data = _INyuService.GetInvoice(invoiceid);
                var model =
                new nyuinvoicemodal
                {
                    id = Data.id,
                    lpono = Data.lpono == null ? "" : Data.lpono.ToString(),
                    invoiceno = Data.invoiceno == null ? "" : Data.invoiceno.ToString(),
                    invoicedate = Data.invoicedate == null ? "" : Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                    travelcode = Data.travelcode == null ? "" : Data.travelcode.ToString(),
                    authcode = Data.authcode == null ? "" : Data.authcode.ToString(),
                };
                return View("../Nyu/View_InvoiceDetails", model);
            }
           catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult GetInvoicedetailsVerified(int invoiceid = 0, int page = 1)
        {
           try
            {
                var Data = _INyuService.GetInvoiceDetail(invoiceid).Where(x => x.status > 0).Select(x => new nyuinvoicedetailmodal
                {
                    servicetype = x.servicetype==null?"":x.servicetype.ToString(),
                    bookno = x.bookno==null?"": x.bookno.ToString(),
                    pax = x.pax==null?"": x.pax.ToString(),
                    fromdate = x.fromdate == null ? "" : x.fromdate.Value.ToString("dd-MMM-yyyy"),
                    todate = x.todate == null ? "" : x.todate.Value.ToString("dd-MMM-yyyy"),
                    provider = x.provider==null?"": x.provider.ToString(),
                    route = x.route==null ? "" : x.route.ToString(),
                    fare = x.fare.ToString("N2"),
                    tax = x.tax.ToString("N2"),
                    vat = x.vat.ToString("N2"),
                    total = x.total.ToString("N2"),
                    authcode = x.authcode==null?"": x.authcode.ToString(),
                    account = x.account==null ? "": x.account.ToString(),
                    fund = x.fund==null?"": x.fund.ToString(),
                    org = x.org==null?"": x.org.ToString(),
                    project = x.project==null?"": x.project.ToString(),
                    program = x.program==null?"": x.program.ToString()
                }).ToList(); ;

                var datalist = Data.ToList();
                decimal amount = 0;
                if (datalist.Count != 0)
                {
                    foreach (var item in datalist)
                    {
                        decimal balance = Convert.ToDecimal(item.total);
                        amount = balance + amount;
                    }
                }


                var Results = new
                {
                    Data = Data.ToList(),
                    page,
                    Total = Data.Count(),
                    TotalAmount = amount.ToString("N2")
                };
                return Json(Results);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        [HttpGet]
        public ActionResult GetInvoicedetails(int invoiceid = 0, int page = 1)
        {
            var Data = _INyuService.GetInvoiceDetail(invoiceid).Select(x => new nyuinvoicedetailmodal
            {
                id=x.id,
                status=x.status,
                servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                pax = x.pax == null ? "" : x.pax.ToString(),
                fromdate = x.fromdate == null ? "" : x.fromdate.Value.ToString("dd-MMM-yyyy"),
                todate = x.todate == null ? "" : x.todate.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider == null ? "" : x.provider.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare.ToString("N2"),
                tax = x.tax.ToString("N2"),
                vat = x.vat.ToString("N2"),
                total = x.total.ToString("N2"),
                authcode = x.authcode == null ? "" : x.authcode.ToString(),
                account = x.account == null ? "" : x.account.ToString(),
                fund = x.fund == null ? "" : x.fund.ToString(),
                org = x.org == null ? "" : x.org.ToString(),
                project = x.project == null ? "" : x.project.ToString(),
                program = x.program == null ? "" : x.program.ToString(),
                serviceFee = x.servicefee.ToString("N2"),
            }).ToList(); ;
            var datalist = Data.ToList();
            decimal amount = 0;
            if (datalist.Count != 0)
            {
                foreach (var item in datalist)
                {
                    decimal balance = Convert.ToDecimal(item.total);
                    amount = balance + amount;
                }
            }

            var Results = new
            {
                Data = Data.ToList(),
                page,
                Total = Data.Count(),
                TotalAmount = amount.ToString("N2")
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult exportinvoicesload(string serlpono = "", string serinvoiceno = "", string serinvfromdate = "", string serinvtodate = "", long serpaystatus = 0)
        {
            try
            {
                serlpono = serlpono ?? "";
                serinvoiceno = serinvoiceno ?? "";
                DateTime From = string.IsNullOrEmpty(serinvfromdate) ? DateTime.MinValue : DateTime.ParseExact(serinvfromdate, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(serinvtodate) ? DateTime.MaxValue : DateTime.ParseExact(serinvtodate, "dd/MM/yyyy", null);
                int pageIndex = Convert.ToInt32(1) - 1;                
                    var Data = _INyuService.GetInvoiceDetail().OrderByDescending(x => x.createddate).Where(x => x.status > 0
             && (x.nyuinvoice.lpono.ToLower().Contains(serlpono.ToLower()) || serlpono == "")
               && (x.nyuinvoice.invoicedate >= From && x.nyuinvoice.invoicedate <= To)
              && (x.nyuinvoice.invoiceno.ToLower().Contains(serinvoiceno.ToLower()) || serinvoiceno == "")
               && (x.nyuinvoice.paystatus == serpaystatus||serpaystatus==0)
               )
            .Select(x => new nyuinvoicedetailexportmodal
            {
                ReferenceNo = x.nyuinvoice.lpono==null?"": x.nyuinvoice.lpono.ToString(),
                RequestorId = x.authcode==null?"": x.authcode.ToString(),
                InvoiceDate = x.nyuinvoice.invoicedate==null?"": x.nyuinvoice.invoicedate.ToString(),
                InvoiceNo = x.invoiceorder==null?"": x.invoiceorder.ToString(),
                NAME = x.pax==null?"": x.pax.ToString(),
                AUTHORISEDBY = x.nyuinvoice.authcode==null?"": x.nyuinvoice.authcode.ToString(),
                TRAVELCODE= x.nyuinvoice.travelcode==null?"": x.nyuinvoice.travelcode.ToString(),
                AirTicket=x.total.ToString(),
                Hotel=x.total.ToString(),
                GroundTransport=x.total.ToString(),
                OtherServices=x.total.ToString(),
                VAT=x.vat.ToString(),
                Exceptio="0",
                Total=x.total.ToString(),
                Fund=x.fund==null?"": x.fund.ToString(),
                Account= x.account==null?"": x.account.ToString(),
                ORG= x.org==null?"": x.org.ToString(),
                Project= x.project==null?"": x.project.ToString(),
                Progra=x.program==null?"": x.program.ToString(),
                TravelDate= x.fromdate==null?"":x.fromdate.ToString(),
                ReturnDate= x.todate==null?"": x.todate.ToString(),
                OriginCity= x.route==null?"": x.route.ToString(),
                SEC2= x.route == null ? "" : x.route.ToString(),
                SEC3 = x.route == null ? "" : x.route.ToString(),
                SEC4 = x.route == null ? "" : x.route.ToString(),
                SEC5 = x.route == null ? "" : x.route.ToString(),
                SEC6 = x.route == null ? "" : x.route.ToString(),
                FinalCity = x.dest == null ? "" : x.dest.ToString(),
                CHECK_IN = x.route == null ? "" : x.fromdate.ToString(),
                CHECK_OUT = x.route == null ? "" : x.todate.ToString()

            }).ToList();

                StringBuilder stringBuilder = new StringBuilder();               
                stringBuilder.AppendLine("ReferenceNo,RequestorId,InvoiceDate,InvoiceNo,NAME,AUTHORISEDBY,TRAVELCODE,AirTicket,Hotel,GroundTransport,OtherServices,VAT,Exception,Total,Fund,Account,ORG,Project,Program,TravelDate,ReturnDate,OriginCity,SEC2,SEC3,SEC4,SEC5,SEC6,FinalCity,CHECK_IN,CHECK_OUT");
                foreach (var rows in Data)
                {
                    stringBuilder.AppendLine($"{rows.ReferenceNo},{ rows.RequestorId},{ rows.InvoiceDate},{ rows.InvoiceNo},{ rows.NAME},{ rows.AUTHORISEDBY},{ rows.TRAVELCODE},{ rows.AirTicket},{ rows.Hotel},{ rows.GroundTransport},{ rows.OtherServices},{ rows.VAT},{ rows.Exceptio},{ rows.Total},{ rows.Fund},{ rows.Account},{ rows.ORG},{ rows.Project},{ rows.Progra},{ rows.TravelDate},{ rows.ReturnDate},{ rows.OriginCity},{ rows.SEC2},{ rows.SEC3},{ rows.SEC4},{ rows.SEC5},{ rows.SEC6},{ rows.FinalCity},{ rows.CHECK_IN},{ rows.CHECK_OUT}");
                }
                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "Travel Details-" + DateTime.Now.Date +  ".csv");

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult ModifyInvoicedetails(long id = 0)
        {
            var Data = _INyuService.GetInvoiceDetail().Where(x => x.id == id).Select(x => new nyuinvoicedetailmodal
            {
                id = x.id,
                servicetype = x.servicetype == null ? "" : x.servicetype.ToString(),
                bookno = x.bookno == null ? "" : x.bookno.ToString(),
                pax = x.pax == null ? "" : x.pax.ToString(),
                fromdate = x.fromdate == null ? "" : x.fromdate.Value.ToString("dd-MMM-yyyy"),
                todate = x.todate == null ? "" : x.todate.Value.ToString("dd-MMM-yyyy"),
                provider = x.provider == null ? "" : x.provider.ToString(),
                route = x.route == null ? "" : x.route.ToString(),
                fare = x.fare.ToString("N2"),
                tax = x.tax.ToString("N2"),
                vat = x.vat.ToString("N2"),
                total = x.total.ToString("N2"),
                authcode = x.authcode == null ? "" : x.authcode.ToString(),
                account = x.account == null ? "" : x.account.ToString(),
                fund = x.fund == null ? "" : x.fund.ToString(),
                org = x.org == null ? "" : x.org.ToString(),
                project = x.project == null ? "" : x.project.ToString(),
                program = x.program == null ? "" : x.program.ToString(),
                status =x.status

            }).FirstOrDefault();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoice(nyuinvoicedetailmodal nyuinvoicedetailmodal)
        {
            try
            {
                var Data = _INyuService.GetInvoiceDetail().Where(x => x.id == nyuinvoicedetailmodal.id).FirstOrDefault();
                if(Data==null)
                {
                    nyuInvoicedetail nyuInvoicedetail = new nyuInvoicedetail();
                    nyuInvoicedetail.invoiceid = nyuinvoicedetailmodal.invoiceid;
                    nyuInvoicedetail.servicetype = nyuinvoicedetailmodal.servicetype;
                    nyuInvoicedetail.fromdate = string.IsNullOrEmpty(nyuinvoicedetailmodal.fromdate) ? (DateTime?)null : DateTime.ParseExact(nyuinvoicedetailmodal.fromdate, "dd-MMM-yyyy", null);
                    nyuInvoicedetail.todate = string.IsNullOrEmpty(nyuinvoicedetailmodal.todate) ? (DateTime?)null : DateTime.ParseExact(nyuinvoicedetailmodal.todate, "dd-MMM-yyyy", null);
                    nyuInvoicedetail.bookno = nyuinvoicedetailmodal.bookno;
                    nyuInvoicedetail.pax = nyuinvoicedetailmodal.pax;
                    nyuInvoicedetail.provider = nyuinvoicedetailmodal.provider;
                    nyuInvoicedetail.route = nyuinvoicedetailmodal.route;
                    nyuInvoicedetail.fare = Convert.ToDouble(nyuinvoicedetailmodal.fare);
                    nyuInvoicedetail.tax = Convert.ToDouble(nyuinvoicedetailmodal.tax);
                    nyuInvoicedetail.vat = Convert.ToDouble(nyuinvoicedetailmodal.vat);
                    nyuInvoicedetail.authcode = nyuinvoicedetailmodal.authcode;
                    nyuInvoicedetail.account = nyuinvoicedetailmodal.account;
                    nyuInvoicedetail.fund = nyuinvoicedetailmodal.fund;
                    nyuInvoicedetail.org = nyuinvoicedetailmodal.org;
                    nyuInvoicedetail.project = nyuinvoicedetailmodal.project;
                    nyuInvoicedetail.program = nyuinvoicedetailmodal.program;
                    nyuInvoicedetail.status = nyuinvoicedetailmodal.status;
                    nyuInvoicedetail.total = Convert.ToDouble(nyuinvoicedetailmodal.fare) + Convert.ToDouble(nyuinvoicedetailmodal.tax) + Convert.ToDouble(nyuinvoicedetailmodal.vat);
                    _INyuService.AddInvoicedetails(nyuInvoicedetail);
                }
                else
                {
                    Data.servicetype = nyuinvoicedetailmodal.servicetype;
                    Data.bookno = nyuinvoicedetailmodal.bookno;
                    Data.pax = nyuinvoicedetailmodal.pax;
                    Data.fromdate = string.IsNullOrEmpty(nyuinvoicedetailmodal.fromdate) ? (DateTime?)null : DateTime.ParseExact(nyuinvoicedetailmodal.fromdate, "dd-MMM-yyyy", null);
                    Data.todate = string.IsNullOrEmpty(nyuinvoicedetailmodal.todate) ? (DateTime?)null : DateTime.ParseExact(nyuinvoicedetailmodal.todate, "dd-MMM-yyyy", null);
                    Data.provider = nyuinvoicedetailmodal.provider;
                    Data.route = nyuinvoicedetailmodal.route;
                    Data.fare = Convert.ToDouble(nyuinvoicedetailmodal.fare);
                    Data.tax = Convert.ToDouble(nyuinvoicedetailmodal.tax);
                    Data.vat = Convert.ToDouble(nyuinvoicedetailmodal.vat);
                    Data.authcode = nyuinvoicedetailmodal.authcode;
                    Data.account = nyuinvoicedetailmodal.account;
                    Data.fund = nyuinvoicedetailmodal.fund;
                    Data.org = nyuinvoicedetailmodal.org;
                    Data.project = nyuinvoicedetailmodal.project;
                    Data.program = nyuinvoicedetailmodal.program;
                    Data.status = nyuinvoicedetailmodal.status;
                    Data.total = Convert.ToDouble(nyuinvoicedetailmodal.fare) + Convert.ToDouble(nyuinvoicedetailmodal.tax) + Convert.ToDouble(nyuinvoicedetailmodal.vat);
                    _INyuService.UpdateInvoicedetails(Data);
                }
                
                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DetleteInvoice(long Id)
        {
            try
            {
                var Data = _INyuService.GetInvoiceDetail().Where(x => x.id == Id).FirstOrDefault();
                Data.status = 0;
                _INyuService.UpdateInvoicedetails(Data);
                return Json("Success");
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApproveInvoice(nyuinvoicemodal nyuinvoicemodal)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var Data = _INyuService.GetStaffverificationload().Where(x => x.invoiceno == nyuinvoicemodal.invoiceno).FirstOrDefault();
                Data.verifiedby = user.Id.ToString();
                Data.verifiedon = DateTime.Now;
                Data.status = 2;              
                _INyuService.Updatecorporateinvoice(Data);
                ViewBag.Section = "Success";
                var model =
               new nyuinvoicemodal
               {
                   id = Data.id,
                   lpono = Data.lpono == null ? "" : Data.lpono.ToString(),
                   invoiceno = Data.invoiceno == null ? "" : Data.invoiceno.ToString(),
                   invoicedate = Data.invoicedate == null ? "" : Data.invoicedate.Value.ToString("dd-MMM-yyyy"),
                   travelcode = Data.travelcode == null ? "" : Data.travelcode.ToString(),
                   authcode = Data.authcode == null ? "" : Data.authcode.ToString()
               };
                return View("../Nyu/View_InvoiceDetails", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);


        [HttpGet]
        public ActionResult attachmentcopy(string invoiceno)
        {
            if (invoiceno.Length > 0)
            {
                try
                {
                    string csvPath = nyuFileLocation + Path.GetFileName(invoiceno.Replace(@"/", "").ToString() + ".pdf");
                    return PhysicalFile(csvPath, "text/plain", invoiceno.Replace(@"/", "").ToString() + ".pdf");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }

        [HttpGet]
        public ActionResult Getinforattachments(long id = 0)
        {
            var Data = _IAttachmentService.Getinforattachments().Where(x => x.invoiceid == id && x.type == "Nyu").Select(x => new
            {
                path = x.path
            }).Distinct().ToList();
            var Results = new
            {
                Data
            };
            return Json(Results);
        }

        [HttpGet]
        public ActionResult infordocdownload(string file)
        {
            if (file.Length > 0)
            {
                try
                {
                    string csvPath = infordocslocation + Path.GetFileName(file.ToString());
                    return PhysicalFile(csvPath, "text/plain", file);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateInvoiceMain(nyuinvoicemodal nyuinvoicemodal)
        {
            try
            {
                var Data = _INyuService.GetStaffverificationload().Where(x => x.invoiceno == nyuinvoicemodal.invoiceno).FirstOrDefault();
                if (Data != null)
                {
                    Data.lpono = nyuinvoicemodal.lpono;
                    Data.travelcode = nyuinvoicemodal.travelcode;
                    Data.authcode = nyuinvoicemodal.authcode;
                    Data.invoicedate = string.IsNullOrEmpty(nyuinvoicemodal.invoicedate) ? (DateTime?)null : DateTime.ParseExact(nyuinvoicemodal.invoicedate, "dd-MMM-yyyy", null);
                    _INyuService.Updatecorporateinvoice(Data);
                }

                return Json("Success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}