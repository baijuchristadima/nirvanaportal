﻿$(document).ready(function () {
    if ($('#SuccessData').val() == "Success") {
        alert("Success");
        $("#btnadd").attr("disabled", true);
        $("#btnapprove").attr("disabled", true);
    }
    $('#divRmDetails').hide();
    var id = $('#id').val();
    loadtable("/Corporate/GetInvoicedetails?invoiceid=" + id, 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $('#btnDetails').click(function () {
        debugger;
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var status = 0;
        if ($('.ios-switch').hasClass('on')) {
            status = 1;
        }
        var corporateinvoicedetailsmodal = {

            bookno: $('#bookno').val(),
            from: $('#from').val(),
            to: $('#to').val(),
            provider: $('#provider').val(),
            guest: $('#guest').val(),
            servicetype: $('#servicetype').val(),
            fare: $('#fare').val(),
            tax: $('#tax').val(),
            vat: $('#vat').val(),
            sc: $('#sc').val(),
            route: $('#route').val(),
            billinginvoiceLine: $('#billinginvoiceLine').val(),
            issuedate: $('#issuedate').val(),
            type: $('#type').val(),
            id: $('#detailsId').val(),
            status: status,
            invoiceid: $('#id').val(),
        }
        $.ajax({
            url: '/Corporate/UpdateInvoice',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                corporateinvoicedetailsmodal: corporateinvoicedetailsmodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
    $('#btnadd').click(function () {
        $.magnificPopup.open({
            items: {
                src: "#modalForm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    });
    $('#btrm').click(function () {
        $('#divRmDetails').hide();
        var id = $('#id').val();
        GetRMDetails(id);
        $.magnificPopup.open({
            items: {
                src: "#modalrm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    });
    $('#btnsaveRm').click(function () {
        var RmId = $('#hdnRmId').val();
        var RMvalue = $('#txtRMvalue').val();
        $.ajax({
            url: '/Corporate/UpdateRMValue',            
            method: 'POST',            
            data: { RmId: RmId, RMvalue: RMvalue },            
            dataType: "json",            
            async: false
        }).done(function (result) {                
                var id = $('#id').val();               
                GetRMDetails(id);               
                $('#divRmDetails').hide();                
            });    
});
    $(document).on("click", ".fa-trash-o", function () {
        var id = $(this).data('detailsid');
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: '/Corporate/DetleteInvoice',
                method: 'POST',
                data: { Id: id },
                dataType: "json",
                async: false
            }).done(function (result) {
                debugger;
                window.location.reload();
            });
        }
    })
    $(document).on("click", ".fa-pencil", function () {
        var id = $(this).data('detailsid');
        $.magnificPopup.open({
            items: {
                src: "#modalForm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
        $.ajax({
            url: '/Corporate/CorporateInvoicedetails',
            method: 'GET',
            data: { Id: id },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            $('#detailsId').val(result.data.id);
            $('#bookno').val(result.data.bookno);
            $('#from').val(result.data.from);
            $('#to').val(result.data.to);
            $('#provider').val(result.data.provider);
            $('#guest').val(result.data.guest);
            $('#servicetype').val(result.data.servicetype);
            $('#fare').val(result.data.fare);
            $('#tax').val(result.data.tax);
            $('#vat').val(result.data.vat);
            $('#sc').val(result.data.sc);
            $('#route').val(result.data.route);
            $('#billinginvoiceLine').val(result.data.billinginvoiceLine);
            $('#issuedate').val(result.data.issuedate);
            $('#type').val(result.data.type);
            if ($('.ios-switch').hasClass('on')) {
                $('.ios-switch').removeClass('on')
            }
            if (result.data.status == 0) {
                $('.ios-switch').removeClass('on')
            }
            else
                $('.ios-switch').addClass('on')



        });
    });
    $(document).on("click", ".fa-edit", function () {        
        $('#hdnRmId').val($(this).data('rmid'));        
        $('#txtRMvalue').val($(this).data('rmvalue'));        
        $('#divRmDetails').show();        
    })    
});
function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th>Service Type</th><th>Voucher#</th><th>Guest/Passenger</th><th>Service From</th><th>Service To</th><th>Service Provider</th></th><th>Sector/City</th></th><th>VAT</th><th>Fare</th><th>Tax</th><th>SC</th><th>Total</th><th>Status</th><th>Actions</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++) {
            var Status = response[i].status == 1 ? "Active" : "Inactive";     
            divstring += '<tr><td>' + response[i].servicetype + '</td>';
            divstring += '<td>' + response[i].bookno + '</td>';
            divstring += '<td>' + response[i].guest + '</td>';
            divstring += '<td>' + response[i].from + '</td>';
            divstring += '<td>' + response[i].to + '</td>';
            divstring += '<td>' + response[i].provider + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].vat + '</td>';
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].sc + '</td>';
            divstring += '<td>' + response[i].total + '</td>';   
            divstring += '<td>' + Status + '</td>';          
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-pencil fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';            
            divstring += '<a href="#" title="Deactivate"><i class="fa fa-trash-o fa-lg" data-DetailsID="' + response[i].id + '"></i></a>'
            divstring += '</td></tr>';
        }
        divstring += '</tbody><tfoot><tr><th id="total" colspan="8" style="text-align: right;" class="total"><b>Total</b></th><td class="total"><b>' + result.totalFare + '</b></td><td class="total"><b>' + result.totalTax + '</b></td><td class="total"><b>' + result.totalSc + '</b></td><td class="total"><b>' + result.totalInvoice + '</b></td></tr></tfoot></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}
function GetRMDetails(Id) {
    $.ajax({
        url: '/Corporate/GetRMDetails',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        debugger;
        var response = result.data;
        var divstring = '<tbody>';
        $('#list-RmDetails').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].code + '</td>';
            divstring += '<td>' + response[i].value + '</td>';                                  
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-edit fa-lg" data-RMID="' + response[i].id + '" data-RMvalue="' + response[i].value + '"></i></a></tr>';
        }
        divstring += '</tbody>';
        $('#list-RmDetails').append(divstring);
    });
}

