﻿$(document).ready(function () {
    loadtable("/Corporate/GetStaffveriLoad", 1);
    GetCustomer();
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/Corporate/StaffveriLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoice();
    })   
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-print", function () {
        debugger;
        var id = $(this).data('detailsid');
        window.open("https://localhost:44364/Home/GetCorporateInvoice?Id=" + id, '_blank');
    })
    $(document).on("click", ".fa-paperclip", function () {
        var id = $(this).data('detailsid');
        Getinfordocs(id);
        $.magnificPopup.open({
            items: {
                src: "#modaldocs"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    })
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });

    $(document).on("click", ".fa-download", function () {
        var file = $(this).data('detailsid');
        showattachments(file);
    })
    $(document).on("click", ".fa-pencil", function () {
        var id = $(this).data('detailsid');       
        var lpono = $(this).data('lpono');
        var invoiceno = $(this).data('invoiceno');
        var lpodate = $(this).data('lpodate');
        var invoicedate = $(this).data('invoicedate');
        $.magnificPopup.open({
            items: {
                src: "#modalinv"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });   
        $('#Id').val(id);
        $('#lpono').val(lpono);
        $('#invno').val(invoiceno);
        $('#lpodate').val(lpodate);
        $('#invdate').val(invoicedate);                  
    });
    $('#btnDetails').click(function () {
        debugger;
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();       
        var corporateinvoicemodal = {

            lpono: $('#lpono').val(),
            lpodate: $('#lpodate').val(),
            invoicedate: $('#invdate').val(),
            invoiceno: $('#invno').val(),           
        }
        $.ajax({
            url: '/Corporate/UpdateInvoiceMain',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                corporateinvoicemodal: corporateinvoicemodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
});

function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        debugger;
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="thirtypercentage">Company Name</th><th class="fifteenpercentage">LPO#</th><th class="fifteenpercentage">LPO Date</th><th class="fifteenpercentage">Invoice No</th><th class="tenpercentage">Invoice Date</th><th class="tenpercentage">Action</th></tr></thead><tbody>';
        $('#list-StaffveriLoad').empty();
        $('#list-StaffveriLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';
            divstring += '<td>' + response[i].companyname + '</td>';
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';           
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-pencil" data-DetailsID="' + response[i].id + '" data-lpono="' + response[i].lpono + '" data-invoiceno="' + response[i].invoiceno + '" data-lpodate="' + response[i].lpodate + '" data-invoicedate="' + response[i].invoicedate + '"></i></a><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a><a href="#" title="Print"><i class="fa fa-print fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';            
            divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].id + '"></i></a>'
            divstring += '</td></tr>';
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-StaffveriLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoice(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        debugger;
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                debugger;
                footerstring += '<li class="active"><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                debugger;
                footerstring += '<li><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoice(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-StaffveriLoad-pagination').append(footerstring);

    });
}
function GetCustomer() {
    $.ajax({
        type: "GET",
        url: "/Corporate/GetCustomer",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '';
        $('#sercustomer').empty();
        $('<option value="0">Select All</option>').appendTo('#sercustomer');
        debugger;
        for (var i = 0; i < response.length; i++) {
            divstring += '<option Value="' + response[i].id + '">' + response[i].name + '</option>';
        }
        $('#sercustomer').append(divstring);

    });
}
function searchinvoice(page = 1) {
    var sercustomer = $('#sercustomer').val();
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var URL = "/Corporate/GetStaffveriLoad?sercustomer=" + sercustomer + "&serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate +
    "&serinvtodate=" + serinvtodate + "&page=" + page;
    loadtable(URL, page)
}
function showDetailsDialog(id) {
    window.open('/Corporate/InvoiceDetails?invoiceid=' + id, '_blank');
}
function Getinfordocs(Id) {
    $.ajax({
        url: '/Corporate/Getinforattachments',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        debugger;
        var response = result.data;
        var divstring = '<tbody>';
        $('#list-infordocs').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].path + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-download" data-DetailsID="' + response[i].path + '"></i></a></td></tr>';
        }
        divstring += '</tbody>';
        $('#list-infordocs').append(divstring);
    });

}
function showattachments(file) {
    window.open('/Corporate/infordocdownload?file=' + file);
}

