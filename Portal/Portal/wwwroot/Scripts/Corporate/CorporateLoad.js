﻿$(document).ready(function () {
    loadtable("/Corporate/GetCorporateLoad", 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $('#btnDetails').click(function () {
        debugger;
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var status = 0;
        if ($('.ios-switch').hasClass('on')) {
            status = 1;
        }
        var corporatecompanymodal = {
            code: $('#code').val(),
            name: $('#name').val(),
            address: $('#address').val(),
            secaddress: $('#secaddress').val(),
            thirdaddress: $('#thirdaddress').val(),
            forthaddress: $('#forthaddress').val(),
            phone: $('#phone').val(),
            mail: $('#mail').val(),
            invformat: $('#invformat').val(),
            currency: $('#currency').val(),
            status: status,
            id: $('#detailsId').val(),
            vat: $('#regno').val()
        }
        debugger
        $.ajax({
            url: '/Corporate/UpdateCorporate',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                corporatecompanymodal: corporatecompanymodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
        });
    });
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/Corporate/CorporateLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchcorporate();
    })
    $(document).on("click", "#btnexport", function () {
        downloadcorporate();
    })
    $(document).on("click", ".fa-trash-o", function () {
        var id = $(this).data('detailsid');
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        if (confirm("Are you sure want to delete?")) {
            debugger
            $.ajax({
                url: '/Corporate/DetleteCompany',
                method: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    Id: id
                },
                dataType: "json",
                async: false
            }).done(function (result) {
                debugger;
                window.location.reload();
            });
        }
    })
    $(document).on("click", ".fa-pencil", function () {
        debugger
        var id = $(this).data('detailsid');
        $.magnificPopup.open({
            items: {
                src: "#modalForm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
        $.ajax({
            url: '/Corporate/companydetails',
            method: 'GET',
            data: { Id: id },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            $('#detailsId').val(result.data.id);
            $('#code').val(result.data.code);
            $('#name').val(result.data.name);
            $('#address').val(result.data.address);
            $('#secaddress').val(result.data.secaddress);
            $('#thirdaddress').val(result.data.thirdaddress);
            $('#forthaddress').val(result.data.forthaddress);
            $('#phone').val(result.data.phone);
            $('#mail').val(result.data.mail);
            $('#invformat').val(result.data.invformat);
            $('#currency').val(result.data.currency);
            $('#status').val(result.data.status);
            $('#regno').val(result.data.vat);
            if ($('.ios-switch').hasClass('on')) {
                $('.ios-switch').removeClass('on')
            }
            if (result.data.status == 0) {
                $('.ios-switch').removeClass('on')
            }
            else
                $('.ios-switch').addClass('on')
        })
    });
});
function loadtable(URL, page) {
    var recordcount = 150;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;        
        var divstring = '<thead><tr><th>Sl.No</th><th>Company Code</th><th>Company Name</th><th>Address 1</th><th>Address 2</th><th>Phone</th><th>Mail</th><th>Format</th><th>Currency</th><th>Registration#</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        $('#list-corporate').empty();
        $('#list-corporate-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            var Status = response[i].status == 1 ? "Active" : "Inactive";
            divstring += '<tr><td>' + (i + 1) + '</td>';
            divstring += '<td>' + response[i].code + '</td>';
            divstring += '<td>' + response[i].name + '</td>';
            divstring += '<td>' + response[i].address + '</td>';
            divstring += '<td>' + response[i].secaddress + '</td>';
            //divstring += '<td>' + response[i].thirdaddress + '</td>';
            //divstring += '<td>' + response[i].forthaddress + '</td>';
            divstring += '<td>' + response[i].phone + '</td>';            
            divstring += '<td>' + response[i].mail + '</td>';
            divstring += '<td>' + response[i].invformat + '</td>';
            divstring += '<td>' + response[i].currency + '</td>';   
            divstring += '<td>' + response[i].vat + '</td>';   
            divstring += '<td>' + Status + '</td>';
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-pencil fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';           
            divstring += '<a href="#" title="Deactivate"><i class="fa fa-trash-o fa-lg" data-DetailsID="' + response[i].id + '"></i></a>'
            divstring += '</td></tr>';
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;      
        divstring += '</tbody></table >';
        $('#list-corporate').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchcorporate(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                
                footerstring += '<li class="active"><a href="#" onclick="searchcorporate(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                
                footerstring += '<li><a href="#" onclick="searchcorporate(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchcorporate(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-corporate-pagination').append(footerstring);

    });
}
function searchcorporate(page = 1) {
    var sercode = $('#sercode').val();  
    var sername = $('#sername').val();
    var serregno = $('#serregno').val();
    var serinvformat = $('#serinvformat').val();
    var sercurrencyId = $('#sercurrencyId').val();
    debugger;
    var URL = "/Corporate/GetCorporateLoad?sercode=" + sercode + "&sername=" + sername + "&serregno=" + serregno+ "&serinvformat=" + serinvformat + "&sercurrencyId=" + sercurrencyId + "&page=" + page;
    loadtable(URL, page)
}
function downloadcorporate(page = 1) {
    debugger;
    var sercode = $('#sercode').val();
    var sername = $('#sername').val();
    var serregno = $('#serregno').val();
    var serinvformat = $('#serinvformat').val();
    var sercurrencyId = $('#sercurrencyId').val();
    window.location.href = "/Corporate/downloadcorporate?sercode=" + sercode + "&sername=" + sername + "&serregno=" + serregno + "&serinvformat=" + serinvformat + "&sercurrencyId=" + sercurrencyId;

}