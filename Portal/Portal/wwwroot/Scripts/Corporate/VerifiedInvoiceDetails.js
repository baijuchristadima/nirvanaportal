﻿$(document).ready(function () {
    var id = $('#id').val();
    loadtable("/Corporate/GetInvoicedetailsVerified?invoiceid=" + id, 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $(document).on("click", ".fa-print", function () {
        debugger;
        var id = $(this).data('detailsid');
        window.open("https://localhost:44364/Home/GetCorporateInvoice?Id=" + id, '_blank');
    })
    $('#btrm').click(function () {
        var id = $('#id').val();
        GetRMDetails(id);
        $.magnificPopup.open({
            items: {
                src: "#modalrm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    });
});
function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        debugger
        var response = result.data;
        var totalfare = result.totalFare;
        var totaltax = result.totalTax;
        var totalsc = result.totalSc;
        var totalinvoice = result.totalInvoice;
        var divstring = '<thead><tr><th>Service Type</th><th>Voucher#</th><th>Guest/Passenger</th><th>Service From</th><th>Service To</th><th>Service Provider</th></th><th>Sector/City</th></th><th>VAT</th><th>Fare</th><th>Tax</th><th>SC</th><th>Total</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++)
        {
            divstring += '<tr><td>' + response[i].servicetype + '</td>';
            divstring += '<td>' + response[i].bookno + '</td>';
            divstring += '<td>' + response[i].guest + '</td>';
            divstring += '<td>' + response[i].from + '</td>';
            divstring += '<td>' + response[i].to + '</td>';
            divstring += '<td>' + response[i].provider + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].vat + '</td>';
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].sc + '</td>';
            divstring += '<td>' + response[i].total + '</td></tr >';
        }
        divstring += '</tbody><tfoot><tr><th id="total" colspan="8" style="text-align: right;" class="total"><b>Total</b></th><td class="total"><b>' + result.totalFare + '</b></td><td class="total"><b>' + result.totalTax + '</b></td><td class="total"><b>' + result.totalSc + '</b></td><td class="total"><b>' + result.totalInvoice + '</b></td></tr></tfoot></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}
function GetRMDetails(Id) {
    $.ajax({
        url: '/Corporate/GetRMDetails',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        debugger;
        var response = result.data;
        var divstring = '<tbody>';
        $('#list-RmDetails').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].code + '</td>';
            divstring += '<td>' + response[i].value + '</td></tr>';
        }
        divstring += '</tbody>';
        $('#list-RmDetails').append(divstring);
    });

}
