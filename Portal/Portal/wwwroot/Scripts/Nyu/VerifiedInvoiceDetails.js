﻿$(document).ready(function () {
    var id = $('#id').val();
    debugger;
    loadtable("/Nyu/GetInvoicedetailsVerified?invoiceid=" + id, 1);
});
function loadtable(URL, page) {
    debugger;
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var totalamount = result.totalAmount;
        var divstring = '<thead><tr><th>Service Type</th><th>Voucher#</th><th>Guest/Passenger</th><th>Service From</th><th>Service To</th><th>Service Provider</th><th>Route/City</th></th><th>Fare</th></th><th>Tax</th><th>VAT</th></th><th>Total</th><th>Requester</th><th>Account</th><th>Fund</th><th>Org</th><th>Project</th><th>Program</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++)
        {
            divstring += '<tr><td>' + response[i].servicetype + '</td>';
            divstring += '<td>' + response[i].bookno + '</td>';
            divstring += '<td>' + response[i].pax + '</td>';
            divstring += '<td>' + response[i].fromdate + '</td>';
            divstring += '<td>' + response[i].todate + '</td>';
            divstring += '<td>' + response[i].provider + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].vat + '</td>';
            divstring += '<td>' + response[i].total + '</td>';
            divstring += '<td>' + response[i].authcode + '</td>';
            divstring += '<td>' + response[i].account + '</td>';
            divstring += '<td>' + response[i].fund + '</td>';
            divstring += '<td>' + response[i].org + '</td>';
            divstring += '<td>' + response[i].project + '</td>';
            divstring += '<td>' + response[i].program + '</td></tr>';
        }
        divstring += '</tbody><tfoot><tr><th id="total" colspan="10" style="text-align: right;">Total</th><td>' + result.totalAmount + '</td></tr></tfoot></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}
