﻿$(document).ready(function () {
    if ($('#SuccessData').val() == "Success") {
        alert("Success");
        $("#btnadd").attr("disabled", true);
        $("#btnapprove").attr("disabled", true);
    }
    var id = $('#id').val();
    loadtable("/Nyu/GetInvoicedetails?invoiceid=" + id, 1);
    $('#btnadd').click(function () {
        $.magnificPopup.open({
            items: {
                src: "#modalForm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    });
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $('#btnDetails').click(function () {
        debugger;
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var status = 0;
        if ($('.ios-switch').hasClass('on')) {
            status = 1;
        }
        var nyuinvoicedetailmodal = {
            servicetype: $('#servicetype').val(),
            bookno: $('#bookno').val(),
            pax: $('#pax').val(),
            fromdate: $('#fromdate').val(),
            todate: $('#todate').val(),
            provider: $('#provider').val(),
            route: $('#route').val(),
            fare: $('#fare').val(),
            tax: $('#tax').val(),
            authcode: $('#authcode').val(),
            vat: $('#vat').val(),
            id: $('#detailsId').val(),
            account: $('#account').val(),
            fund: $('#fund').val(),
            org: $('#org').val(),
            project: $('#project').val(),
            program: $('#program').val(),
            status: status,
            invoiceid: $('#id').val()
        }
        $.ajax({
            url: '/Nyu/UpdateInvoice',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                nyuinvoicedetailmodal: nyuinvoicedetailmodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
    $(document).on("click", ".fa-trash-o", function () {
        debugger
        var id = $(this).data('detailsid');
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        if (confirm("Are you sure want to delete?")) {
            debugger
            $.ajax({
                url: '/Nyu/DetleteInvoice',
                method: 'POST',
                data: { __RequestVerificationToken: token,Id: id },
                dataType: "json",
                async: false
            }).done(function (result) {
                debugger;
                window.location.reload();
            });
        }
    })
    $(document).on("click", ".fa-pencil", function () {
        var id = $(this).data('detailsid');
        $.magnificPopup.open({
            items: {
                src: "#modalForm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
        $.ajax({
            url: '/Nyu/ModifyInvoicedetails',
            method: 'GET',
            data: { Id: id },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            $('#detailsId').val(result.data.id);
            $('#servicetype').val(result.data.servicetype);
            $('#bookno').val(result.data.bookno);
            $('#pax').val(result.data.pax);
            $('#fromdate').val(result.data.fromdate);
            $('#todate').val(result.data.todate);
            $('#provider').val(result.data.provider);
            $('#route').val(result.data.route);
            $('#fare').val(result.data.fare);
            $('#tax').val(result.data.tax);
            $('#vat').val(result.data.vat);
            $('#authcode').val(result.data.authcode);
            $('#account').val(result.data.account);
            $('#fund').val(result.data.fund);
            $('#org').val(result.data.org);
            $('#project').val(result.data.project);
            $('#program').val(result.data.program);
            if ($('.ios-switch').hasClass('on')) {
                $('.ios-switch').removeClass('on')
            }
            if (result.data.status == 0) {
                $('.ios-switch').removeClass('on')
            }
            else
                $('.ios-switch').addClass('on')



        });
    });
});
function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        debugger
        var totamount = result.totalAmount;
        debugger
        var divstring = '<thead><tr><th>Service Type</th><th>Voucher#</th><th>Guest/Passenger</th><th>Service From</th><th>Service To</th><th>Service Provider</th><th>Route/City</th></th><th>Service Fee</th><th>Fare</th></th><th>Tax</th><th>VAT</th></th><th>Total</th><th>Requester</th><th>Account</th><th>Fund</th><th>Org</th><th>Project</th><th>Program</th><th>Status</th><th>Actions</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++) {
            var Status = response[i].status == 1 ? "Active" : "Inactive";
            divstring += '<tr><td>' + response[i].servicetype + '</td>';
            divstring += '<td>' + response[i].bookno + '</td>';
            divstring += '<td>' + response[i].pax + '</td>';
            divstring += '<td>' + response[i].fromdate + '</td>';
            divstring += '<td>' + response[i].todate + '</td>';
            divstring += '<td>' + response[i].provider + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].serviceFee + '</td>';
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].vat + '</td>';
            divstring += '<td>' + response[i].total + '</td>';
            divstring += '<td>' + response[i].authcode + '</td>';
            divstring += '<td>' + response[i].account + '</td>';
            divstring += '<td>' + response[i].fund + '</td>';
            divstring += '<td>' + response[i].org + '</td>';
            divstring += '<td>' + response[i].project + '</td>';
            divstring += '<td>' + response[i].program + '</td>';
            divstring += '<td>' + Status + '</td>';
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-pencil fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';
            // divstring += '<td class="actions"><a onclick="showDetailsDialog(' + response[i].Id + ')"><i class="fa fa-pencil fa-lg"></i></a><a onclick="showDeleteClient(' + response[i].Id + ')"><i class="fa fa-trash-o fa-lg"></i></a></td></tr>';
            divstring += '<a href="#" title="Deactivate"><i class="fa fa-trash-o fa-lg" data-DetailsID="' + response[i].id + '"></i></a>'
            divstring += '</td></tr>';
        }
        divstring += '</tbody><tfoot><tr><th id="total" colspan="11" style="text-align: right;">Total</th><td>' + result.totalAmount + '</td></tr></tfoot></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}
