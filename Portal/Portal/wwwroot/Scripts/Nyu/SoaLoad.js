﻿$(document).ready(function () {
    loadtable("/Nyu/GetSoaLoad", 1);   
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/Nyu/SoaLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchsoaLoad();
    })
    $(document).on("click", "#btnExport", function () {
        exportsoaLoad();
    }) 
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })
});

function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        debugger
        var response = result.data;
        document.getElementById("totamount").value = result.totalAmt;
        debugger;
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="twentypercentage">LPO#</th><th class="thirtypercentage">Invoice No</th><th class="tenpercentage">Invoice Date</th><th class="tenpercentage">Debit</th><th class="tenpercentage">Credit</th><th class="tenpercentage">Balance</th><th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-SoaLoad').empty();
        $('#list-SoaLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';           
            divstring += '<td>' + response[i].lpono + '</td>';            
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].debit + '</td>';
            divstring += '<td>' + response[i].credit + '</td>';
            divstring += '<td>' + response[i].balance + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].invoiceid + '"></i></a>';

            if (response[i].isAttached == true) {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            } else {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip attached" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            }

           
            divstring += '</td></tr>';          }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody><tfoot><tr><th id="total" colspan="6" class="total" style="text-align: right;"><b>Total</b></th><td class="total"><b>' + result.totalAmt + '</b></td></tr></tfoot></table >';
        $('#list-SoaLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchsoaLoad(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                footerstring += '<li class="active"><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                footerstring += '<li><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchsoaLoad(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-SoaLoad-pagination').append(footerstring);

    });
}

function searchsoaLoad(page = 1) {   
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var URL = "/Nyu/GetSoaLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate + "&page=" + page;
    loadtable(URL, page)
}
function exportsoaLoad(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    window.location.href = "/Nyu/exportSoaLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate;

}
function showDetailsDialog(id) {
    window.open('/Nyu/InvoiceDetailsVerifiedLoad?invoiceid=' + id, '_blank');
}
