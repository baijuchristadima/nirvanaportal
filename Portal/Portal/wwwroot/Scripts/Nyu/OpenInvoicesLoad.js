﻿$(document).ready(function () {
    loadtable("/Nyu/GetOpenInvoicesLoad", 1);
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/Nyu/OpenInvoicesLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoice();
    })  
    $(document).on("click", "#btnExport", function () {
        exportinvoicesload();
    }) 
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })
});

function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="fifteenpercentage">LPO#</th><th class="fifteenpercentage">Invoice No</th><th class="fifteenpercentage">Invoice Date</th><th class="thirtypercentage">Verified By</th><th class="fifteenpercentage">Verified On</th><th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-InvoicesLoad').empty();
        $('#list-InvoicesLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].verifiedby + '</td>';
            divstring += '<td>' + response[i].verifiedon + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';
            if (response[i].isAttached == true) {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip attached" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            } else {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            }
           
            divstring += '</td></tr>';  
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-InvoicesLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoice(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                footerstring += '<li class="active"><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                footerstring += '<li><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoice(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-InvoicesLoad-pagination').append(footerstring);

    });
}

function searchinvoice(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var URL = "/Nyu/GetOpenInvoicesLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate +
        "&serinvtodate=" + serinvtodate + "&page=" + page;
    loadtable(URL, page)
}
function exportinvoicesload(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var serpaystatus = $('#serpaystatus').val();
    window.location.href = "/Nyu/exportinvoicesload?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate +
        "&serinvtodate=" + serinvtodate + "&serpaystatus=" + serpaystatus;
}
function showDetailsDialog(id) {
    window.open('/Nyu/InvoiceDetailsVerifiedLoad?invoiceid=' + id, '_blank');
}
function showattachments(invoiceno) {
    window.open('/Nyu/attachmentcopy?invoiceno=' + invoiceno);
}

