﻿$(document).ready(function () {
    var id = $('#id').val();
    loadtable("/CorporateCompany/GetInvoicedetailsVerified?invoiceid=" + id, 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $('#btrm').click(function () {
        var id = $('#id').val();
        GetRMDetails(id);
        $.magnificPopup.open({
            items: {
                src: "#modalrm"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    });
});
function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th>Service Type</th><th>Voucher#</th><th>Guest/Passenger</th><th>Service From</th><th>Service To</th><th>Service Provider</th></th><th>Sector/City</th></th><th>Fare</th><th>Tax</th><th>VAT</th><th>SC</th><th>Total</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].servicetype + '</td>';
            divstring += '<td>' + response[i].bookno + '</td>';
            divstring += '<td>' + response[i].guest + '</td>';            
            divstring += '<td>' + response[i].from + '</td>';
            divstring += '<td>' + response[i].to + '</td>';
            divstring += '<td>' + response[i].provider + '</td>';
            divstring += '<td>' + response[i].route + '</td>';            
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].vat + '</td>';
            divstring += '<td>' + response[i].sc + '</td>';
            divstring += '<td>' + response[i].total + '</td></tr >';                                  
        }
        divstring += '</tbody></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}
function GetRMDetails(Id) {
    $.ajax({
        url: '/CorporateCompany/GetRMDetails',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        debugger;
        var response = result.data;
        var divstring = '<tbody>';
        $('#list-RmDetails').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].code + '</td>';
            divstring += '<td>' + response[i].value + '</td></tr>';
        }
        divstring += '</tbody>';
        $('#list-RmDetails').append(divstring);
    });

}
