﻿$(document).ready(function () {
    loadtable("/CorporateCompany/GetSoaLoad", 1);
    GetCustomer();
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/CorporateCompany/SoaLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchsoaLoad();
    })
    $(document).on("click", "#btnexport", function () {
        exportsoaLoad();
    })
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-print", function () {
        debugger;
        var id = $(this).data('detailsid');
        window.open("https://localhost:44364/Home/ExportCustomers?Id=" + id, '_blank');
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })
});

function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        debugger;
        document.getElementById("totoutstanding").value = result.totOS;
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="twentypercentage">Company Name</th><th>Code</th><th class="tenpercentage">LPO#</th><th class="tenpercentage">LPO Date</th><th class="tenpercentage">Invoice No</th><th class="tenpercentage">Invoice Date</th><th class="tenpercentage">Debit</th><th class="tenpercentage">Credit</th><th class="tenpercentage">Balance</th><th class="tenpercentage">Action</th></tr></thead><tbody>';
        $('#list-SoaLoad').empty();
        $('#list-SoaLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';
            divstring += '<td>' + response[i].companyname + '</td>';
            divstring += '<td>' + response[i].companycode + '</td>';
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].debit + '</td>';
            divstring += '<td>' + response[i].credit + '</td>';
            divstring += '<td>' + response[i].balance + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].invoiceid + '"></i></a><a href="#" title="Print"><i class="fa fa-print fa-lg" data-DetailsID="' + response[i].invoiceid + '"></i></a>';
            divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            divstring += '</td></tr>';
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody><tfoot><tr><th id="total" colspan="9" class="total" style="text-align: right;"><b>Total<b></th><td class="total"><b>' + result.totOS + '<b></td></tr></tfoot></table >';
        $('#list-SoaLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchsoaLoad(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        debugger;
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                debugger;
                footerstring += '<li class="active"><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                debugger;
                footerstring += '<li><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchsoaLoad(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-SoaLoad-pagination').append(footerstring);

    });
}
function GetCustomer() {
    $.ajax({
        type: "GET",
        url: "/CorporateCompany/GetCustomer",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '';
        $('#sercustomer').empty();
        debugger;
        $('<option value="0">Select All</option>').appendTo('#sercustomer');
        for (var i = 0; i < response.length; i++) {
            divstring += '<option Value="' + response[i].id + '">' + response[i].name + '</option>';
        }
        $('#sercustomer').append(divstring);

    });
}
function searchsoaLoad(page = 1) {
    var sercustomer = $('#sercustomer').val();
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var companycode = $('#companycode').val();
    var URL = "/CorporateCompany/GetSoaLoad?sercustomer=" + sercustomer + "&serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate + "&companycode=" + companycode + "&page=" + page;
    loadtable(URL, page)
}

function exportsoaLoad(page = 1) {
    debugger;
    var sercustomer = $('#sercustomer').val();
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();

    window.location.href = "/CorporateCompany/exportSoaLoad?sercustomer=" + sercustomer + "&serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate;

}
function showDetailsDialog(id) {
    window.open('/CorporateCompany/InvoiceDetailsVerified?invoiceid=' + id, '_blank');
}
function showattachments(invoiceno) {
    window.open('/CorporateCompany/attachmentcopy?invoiceno=' + invoiceno);
}

