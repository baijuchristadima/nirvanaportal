﻿$(document).ready(function () {   
    loadtable("/GhqNtt/GetSoaLoad", 1); 
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/SoaLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchsoaLoad();
    })
    $(document).on("click", "#btnExport", function () {
        exportsoaLoad();
    }) 
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })
});

function loadtable(URL, page) {
    debugger
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th class="fivepercentage">Sl.No</th><th>LPO#</th><th>LPO Date</th><th>Invoice No</th><th>Invoice Date</th><th>Debit</th><th>Credit</th><th>Balance</th><th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-SoaLoad').empty();
        $('#list-SoaLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';            
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].debit + '</td>';
            divstring += '<td>' + response[i].credit + '</td>';
            divstring += '<td>' + response[i].balance + '</td>';
            divstring += '<td>';
           // divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].invoiceno + '"></i></a><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].lpoid + '"></i></a>'
            divstring += '<a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].lpoid + '"></i></a>';
            if (response[i].isAttached == true) {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            } else {
                divstring += '<a href="#" title="No Attachments"><i class="fa fa-paperclip attached" data-DetailsID="' + response[i].invoiceno + '"></i></a>'
            }

            divstring += '</td></tr>';
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-SoaLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchsoaLoad(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {

                footerstring += '<li class="active"><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                footerstring += '<li><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchsoaLoad(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-SoaLoad-pagination').append(footerstring);

    });
}
function searchsoaLoad(page = 1) {   
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    var serlpofromdate = $('#serlpofromdate').val();
    var serlpotodate = $('#serlpotodate').val();
    var URL = "/GhqNtt/GetSoaLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate+ "&serlpofromdate=" + serlpofromdate + "&serlpotodate=" + serlpotodate + "&page=" + page;
    loadtable(URL, page)
}
function showDetailsDialog(id) {
    window.open('/GhqNtt/InvoiceDetailsVerifiedLoad?Id=' + id, '_blank');
}
function showattachments(invoiceno) {
    debugger
    window.open('/GhqNtt/attachmentcopy?invoiceno=' + invoiceno);
}
function exportsoaLoad(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    window.location.href = "/GhqNtt/exportSoaLoad?sserlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate;

}


