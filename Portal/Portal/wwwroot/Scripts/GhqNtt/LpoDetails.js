﻿$(document).ready(function () {
    if ($('#SuccessData').val() == "Success") {
        alert("Success");
        $("#btnadd").attr("disabled", true);
        $("#btnapprove").attr("disabled", true);
    }
    var id = $('#id').val();
    debugger
    loadtable("/GhqNtt/GetLpodetails?invoiceid=" + id, 1, 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $('#btnDetails').click(function () {
        debugger;
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var ghqinvoicedetailmodal = {
            code: $('#Code').val(),
            bookno: $('#Ticket').val(),
            tic_class: $('#Class').val(),
            refno: $('#Refno').val(),
            pax: $('#Passenger').val(),
            route: $('#Route').val(),
            yq: $('#YQ').val(),
            sc: $('#SC').val(),
            tax: $('#Tax').val(),
            id: $('#DetailsId').val(),
            fare: $('#Fare').val()
        }
        $.ajax({
            url: '/GhqNtt/UpdateInvoice',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                ghqinvoicedetailmodal: ghqinvoicedetailmodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
});
function loadtable(URL, page) {
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        debugger
        console.log(result);
        var response = result.data;
        var divstring = '<thead><tr><th>Sl#</th><th>LpoDate</th><th>Route</th><th>Pax</th><th>Ref No</th><th>Lpo Class</th><th>Pax dob</th></th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        for (var i = 0; i < response.length; i++) {
            var Status = response[i].status == 1 ? "Active" : "Inactive";
            divstring += '<tr><td>' + (i + 1) + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].pax + '</td>';
            divstring += '<td>' + response[i].refno + '</td>';
            divstring += '<td>' + response[i].lpoclass + '</td>';
            divstring += '<td>' + response[i].paxdob + '</td></tr>';
            // divstring += '<td class="actions"><a onclick="showDetailsDialog(' + response[i].id + ')"><i class="fa fa-pencil"></i></a><a onclick="showDeleteClient(' + response[i].id + ')"><i class="fa fa-trash-o"></i></a></td></tr>';
        }
        divstring += '</tbody></table >';
        $('#list-InvoiceDetails').append(divstring);
    });
}

function showDeleteClient(Id) {
    if (confirm("Are you sure want to delete?")) {
        $.ajax({
            url: '/GhqNtt/DetleteInvoice',
            method: 'POST',
            data: { Id: Id },
            dataType: "json",
            async: false
        }).done(function (result) {
            debugger;
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    }
}
function showDetailsDialog(Id) {
    $.magnificPopup.open({
        items: {
            src: "#modalForm"
        },
        type: 'inline',
        preloader: false,
        modal: true,
    });
    $.ajax({
        url: '/GhqNtt/GhqInvoicedetails',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        debugger;
        $('#DetailsId').val(result.data.id);
        $('#Code').val(result.data.code);
        $('#Ticket').val(result.data.bookno);
        $('#Class').val(result.data.tic_class);
        $('#Refno').val(result.data.refno);
        $('#Passenger').val(result.data.pax);
        $('#Route').val(result.data.route);
        $('#Fare').val(result.data.fare);
        $('#YQ').val(result.data.yq);
        $('#SC').val(result.data.sc);
        $('#Tax').val(result.data.tax);
    });

}
