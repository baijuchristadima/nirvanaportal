﻿$(document).ready(function () {
    if ($('#SuccessData').val() == "Success") {
        alert("Success");
        $("#btnadd").attr("disabled", true);
        $("#btnapprove").attr("disabled", true);
    }
    debugger
    var id = $('#id').val();
    debugger
    loadtable("/GhqNtt/GetInvoicedetails?invoiceid=" + id, 1, 1);
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });

    $(document).on("click", "#btnadd", function () {

        debugger
        $.magnificPopup.open({
            items: {
                src: "#modalFormAdd"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    })

    $(document).on("click", "#addinvoicedetails", function () {
        debugger
        AddInvoiceDetails();
    })

    function AddInvoiceDetails() {
        debugger
    var form = $('#Details-form');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    var ghqinvoicedetailmodal = {
        code: $('#add_Code').val(),
        bookno: $('#add_Ticket').val(),
        tic_class: $('#add_Class').val(),
        refno: $('#add_Refno').val(),
        pax: $('#add_Passenger').val(),
        route: $('#add_Route').val(),
        yq: $('#add_YQ').val(),
        sc: $('#add_SC').val(),
        tax: $('#add_Tax').val(),
        id: $('#add_DetailsId').val(),
        fare: $('#add_Fare').val(),
        lpoid: id
    }

    $.ajax({
        url: '/GhqNtt/AddInvoiceDetail',
        method: 'POST',
        data: {
            __RequestVerificationToken: token,
            ghqinvoicedetailmodal: ghqinvoicedetailmodal
        },
        dataType: "json",
        async: false
    }).done(function (result) {
        window.location.reload();
        //toastr.success(result);
        //alert("Deleted");
    });
}


 

    $(document).on("click", "#btnreject", function () {
        debugger
        var invno = document.getElementById("invno").value;
        rejectInvoice(invno)

    })

    function rejectInvoice(invno) {
        debugger
        $.ajax({
            url: '/GhqNtt/GhqRejectInvoice',
            method: 'POST',
            data: {
                invno: invno
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            window.location.href = "/GhqNtt/StaffveriLoad";
        });

    }


    $('#btnDetails').click(function () {
        debugger
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var ghqinvoicedetailmodal = {
            code: $('#Code').val(),
            bookno: $('#Ticket').val(),
            tic_class: $('#Class').val(),
            refno: $('#Refno').val(),
            pax: $('#Passenger').val(),
            route: $('#Route').val(),
            yq: $('#YQ').val(),
            sc: $('#SC').val(),
            tax: $('#Tax').val(),
            id: $('#DetailsId').val(),
            fare: $('#Fare').val()
        }
        $.ajax({
            url: '/GhqNtt/UpdateInvoice',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                ghqinvoicedetailmodal: ghqinvoicedetailmodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
});
function loadtable(URL, page) {
    debugger
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        debugger
        var response = result.data;

        var divstring = '<thead><tr><th>Sl#</th><th>Code</th><th>Ticket</th><th>Class</th><th>Ref No</th><th>Passenger</th><th>Route</th></th><th>Fare</th></th><th>YQ</th><th>Tax</th><th>SC</th><th>Total</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        var divstringexcess = '<thead><tr><th>Sl#</th><th>Code</th><th>Ticket</th><th>Class</th><th>Ref No</th><th>Passenger</th><th>Route</th></th><th>Fare</th></th><th>YQ</th><th>Tax</th><th>SC</th><th>Total</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        $('#list-Excess-InvoiceDetails').empty();

        for (var i = 0; i < response.length; i++) {
            if (response[i].ticket_excess_baggage_pices == null || response[i].ticket_excess_baggage_pices == 0 || response[i].ticket_excess_baggage == null || response[i].ticket_excess_baggage == 0) {
              debugger
                var Status = response[i].status == 1 ? "Active" : "Inactive";
                divstring += '<tr><td>' + (i + 1) + '</td>';
                divstring += '<td>' + response[i].code + '</td>';
                divstring += '<td>' + response[i].bookno + '</td>';
                divstring += '<td>' + response[i].tic_class + '</td>';
                divstring += '<td>' + response[i].refno + '</td>';
                divstring += '<td>' + response[i].pax + '</td>';
                divstring += '<td>' + response[i].route + '</td>';
                divstring += '<td>' + response[i].fare + '</td>';
                divstring += '<td>' + response[i].yq + '</td>';
                divstring += '<td>' + response[i].tax + '</td>';
                divstring += '<td>' + response[i].sc + '</td>';
                divstring += '<td>' + response[i].total + '</td>';
                divstring += '<td>' + Status + '</td>';
                divstring += '<td class="actions"><a onclick="showDetailsDialog(' + response[i].id + ')"><i class="fa fa-pencil"></i></a><a onclick="showDeleteClient(' + response[i].id + ')"><i class="fa fa-trash-o"></i></a></td></tr>';                            
            } else {
               debugger
                var Status = response[i].status == 1 ? "Active" : "Inactive";
                divstringexcess += '<tr><td>' + (i + 1) + '</td>';
                divstringexcess += '<td>' + response[i].code + '</td>';
                divstringexcess += '<td>' + response[i].bookno + '</td>';
                divstringexcess += '<td>' + response[i].tic_class + '</td>';
                divstringexcess += '<td>' + response[i].refno + '</td>';
                divstringexcess += '<td>' + response[i].pax + '</td>';
                divstringexcess += '<td>' + response[i].route + '</td>';
                divstringexcess += '<td>' + response[i].fare + '</td>';
                divstringexcess += '<td>' + response[i].yq + '</td>';
                divstringexcess += '<td>' + response[i].tax + '</td>';
                divstringexcess += '<td>' + response[i].sc + '</td>';
                divstringexcess += '<td>' + response[i].total + '</td>';
                divstringexcess += '<td>' + Status + '</td>';
                divstringexcess += '<td class="actions"><a onclick="showDetailsDialog(' + response[i].id + ')"><i class="fa fa-pencil"></i></a><a onclick="showDeleteClient(' + response[i].id + ')"><i class="fa fa-trash-o"></i></a></td></tr>';
              
            }
        }

        divstring += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;" class="total">Total</th><td class="total"><b>' + result.totalFare + '</b></td><td class="total"><b>' + result.totalYq + '</b></td><td class="total"><b>' + result.totalTax + '</b></td><td class="total"><b>' + result.totalSc + '</b></td><td class="total"><b>' + result.totalInvoice + '</b></td></tr></tfoot></table >';
        divstringexcess += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;" class="total">Total</th><td class="total"><b>' + result.excessTotalFare + '</b></td><td class="total"><b>' + result.excessTotalYq + '</b></td><td class="total"><b>' + result.excessTotalTax + '</b></td><td class="total"><b>' + result.excessTotalSc + '</b></td><td class="total"><b>' + result.excessTotalInvoice + '</b></td></tr></tfoot></table >';
        // divstring += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;">Total</th><td>' + result.totalFare + '</td><td>' + result.totalYq + '</td><td>' + result.totalTax + '</td><td>' + result.totalSc + '</td><td>' + result.totalInvoice + '</td></tr></tfoot></table >';
        $('#list-Excess-InvoiceDetails').append(divstringexcess);
        $('#list-InvoiceDetails').append(divstring);
       
    });
}

function showDeleteClient(Id) {

    if (confirm("Are you sure want to delete?")) {
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        $.ajax({
            url: '/GhqNtt/DetleteInvoice',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                Id: Id
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    }
}
function showDetailsDialog(Id) {
    $.magnificPopup.open({
        items: {
            src: "#modalForm"
        },
        type: 'inline',
        preloader: false,
        modal: true,
    });



    $.ajax({
        url: '/GhqNtt/GhqInvoicedetails',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        $('#DetailsId').val(result.data.id);
        $('#Code').val(result.data.code);
        $('#Ticket').val(result.data.bookno);
        $('#Class').val(result.data.tic_class);
        $('#Refno').val(result.data.refno);
        $('#Passenger').val(result.data.pax);
        $('#Route').val(result.data.route);
        $('#Fare').val(result.data.fare);
        $('#YQ').val(result.data.yq);
        $('#SC').val(result.data.sc);
        $('#Tax').val(result.data.tax);
    });

}
