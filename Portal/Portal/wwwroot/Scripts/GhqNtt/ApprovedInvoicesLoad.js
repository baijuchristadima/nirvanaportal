﻿$(document).ready(function () {
    loadtable("/GhqNtt/GetApprovedInvoicesLoad", 1);
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/ApprovedListLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoicesload();
    })  
    $(document).on("click", "#btnExport", function () {
        debugger
        var ids = [];
        $('#list-InvoicesLoad input:checked').each(function () {
            debugger
            var id = $(this).attr("id");         
            ids.push(this.id);
        });        
        exportinvoicesload(ids)
    }) 
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })   
    $('input[type="checkbox"]').click(function () {
        if ($(this).prop("checked") == true) {          
            $(this).addClass('selected')
        }
        else if ($(this).prop("checked") == false) {         
            $(this).removeClass('selected');
        }
    });
});

function loadtable(URL, page) {
    debugger
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th th class="fivepercentage">Select</th><th th class="fivepercentage">Sl.No</th><th class="tenpercentage">LPO#</th><th class="tenpercentage">LPO Date</th><th class="tenpercentage">Invoice No</th><th class="tenpercentage">Invoice Date</th><th class="fifteenpercentage">Verified By</th><th class="tenpercentage">Verified On</th></th><th class="fifteenpercentage">Ghq Verified By</th></th><th>GhqVerified On</th></th><th th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-InvoicesLoad').empty();
        $('#list-InvoicesLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr>';
            divstring += '<td><input type="checkbox" id="'+response[i].lpono+'"/></td >';           
            divstring += '<td>' + (i+1) + '</td>';           
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].verifiedby + '</td>';
            divstring += '<td>' + response[i].verifiedon + '</td>';
            divstring += '<td>' + response[i].ghqverifiedby + '</td>';
            divstring += '<td>' + response[i].ghqverifiedon + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';
            if (response[i].isAttached == true) {
                divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].lpono + '"></i></a>'
            } else {
                divstring += '<a href="#" title="No Attachments"><i class="fa fa-paperclip attached" data-DetailsID="' + response[i].lpono + '"></i></a>'
            }

          
            divstring += '</td></tr>';  
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-InvoicesLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoicesload(1);return false;"><span class="fa fa-step-backward"></span></a></li>';

        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {

                footerstring += '<li class="active"><a href="#" onclick="searchinvoicesload(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {

                footerstring += '<li><a href="#" onclick="searchinvoicesload(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoicesload(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-InvoicesLoad-pagination').append(footerstring);

    });
}
function searchinvoicesload(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serlpofromdate = $('#serlpofromdate').val();
    var serlpotodate = $('#serlpotodate').val();
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var serapprovedfromdate = $('#serapprovedfromdate').val();
    var serapprovedtodate = $('#serapprovedtodate').val();
    var serpurpose = $('#serpurpose').val();

    var URL = "/GhqNtt/GetApprovedInvoicesLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serlpofromdate=" + serlpofromdate + "&serlpotodate=" + serlpotodate + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate + "&serapprovedfromdate=" + serapprovedfromdate + "&serapprovedtodate=" + serapprovedtodate + "&serpurpose=" + serpurpose  + "&page=" + page;
    loadtable(URL, page)
}

function exportinvoicesload(ids) {
    window.location.href = "/GhqNtt/exportinvoicesload?selectedIDs=" + ids;   
}
function showDetailsDialog(id) {
    window.open('/GhqNtt/InvoiceDetailsVerifiedLoad?Id=' + id, '_blank');
}
function showattachments(invoiceno) {
    window.open('/GhqNtt/attachmentcopy?invoiceno=' + invoiceno);
}


