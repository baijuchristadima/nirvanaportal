﻿$(document).ready(function () {
    loadtable("/GhqNtt/GetStaffveriLoad", 1);
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/StaffveriLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoice();
    })  
    $(document).on("click", ".fa-eye", function () {
        debugger
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });
    $(document).on("click", ".fa-paperclip", function () {
        var id = $(this).data('detailsid');
        Getinfordocs(id);
        $.magnificPopup.open({
            items: {
                src: "#modaldocs"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    })
    $(document).on("click", ".fa-print", function () {
        var id = $(this).data('detailsid');
        window.open("https://localhost:44364/Home/ExportCustomers?Id=" + id, '_blank');
    })
    $(document).on("click", ".fa-download", function () {
        var file = $(this).data('detailsid');
        showattachments(file);
    })
    $(document).on("click", ".fa-pencil", function () {
        debugger
        var id = $(this).data('detailsid');
        var lpono = $(this).data('lpono');
        var invoiceno = $(this).data('invoiceno');
        var lpodate = $(this).data('lpodate');
        var invoicedate = $(this).data('invoicedate');
        $.magnificPopup.open({
            items: {
                src: "#modalinv"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
        $('#Id').val(id);
        $('#lpono').val(lpono);
        $('#invno').val(invoiceno);
        $('#lpodate').val(lpodate);
        $('#invdate').val(invoicedate);
    });
    $('#btnDetails').click(function () {
        var form = $('#Details-form');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var ghqinvoicemodal = {

            lpono: $('#lpono').val(),
            lpodate: $('#lpodate').val(),
            invoicedate: $('#invdate').val(),
            invoiceno: $('#invno').val(),
        }
        $.ajax({
            url: '/GhqNtt/UpdateInvoiceMain',
            method: 'POST',
            data: {
                __RequestVerificationToken: token,
                ghqinvoicemodal: ghqinvoicemodal
            },
            dataType: "json",
            async: false
        }).done(function (result) {
            window.location.reload();
            //toastr.success(result);
            //alert("Deleted");
        });
    });
});
function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        //var divstring = '<thead><tr><th>#</th><th>LPO#</th><th>LPO Date</th><th>Invoice No</th><th>Invoice Date</th><th>Action</th></tr></thead><tbody>';
        var divstring = '<thead><tr><th class="threepercentage">Sl#</th><th class="twentypercentage">LPO#</th><th class="forteenpercentage">LPO Date</th><th class="twentyfivepercentage">Invoice No</th><th class="forteenpercentage">Invoice Date</th><th class="sevenpercentage">Action</th></tr></thead><tbody>';
        $('#list-StaffveriLoad').empty();
        $('#list-StaffveriLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i+1) + '</td>';            
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td><a href="#" title="Modify"><i class="fa fa-pencil" data-DetailsID="' + response[i].id + '" data-lpono="' + response[i].lpono + '" data-invoiceno="' + response[i].invoiceno + '" data-lpodate="' + response[i].lpodate + '" data-invoicedate="' + response[i].invoicedate + '"></i></a><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a><a href="#" title="Print"><i class="fa fa-print fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';
            divstring += '<a href="#" title="Attachments"><i class="fa fa-paperclip" data-DetailsID="' + response[i].id + '"></i></a>'
            divstring += '</td></tr>';
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-StaffveriLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoice(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                footerstring += '<li class="active"><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                footerstring += '<li><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoice(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-StaffveriLoad-pagination').append(footerstring);

    });
}
function searchinvoice(page = 1) {    
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    var URL = "/GhqNtt/GetStaffveriLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate
    "&sertodate=" + sertodate + "&page=" + page;
    loadtable(URL, page)
}

function showDetailsDialog(id) {
    debugger
    window.open('/GhqNtt/InvoiceDetails?Id=' + id, '_blank');
}
function Getinfordocs(Id) {
    $.ajax({
        url: '/GhqNtt/Getinforattachments',
        method: 'GET',
        data: { Id: Id },
        dataType: "json",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<tbody>';
        $('#list-infordocs').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + response[i].path + '</td>';
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-download" data-DetailsID="' + response[i].path + '"></i></a></td></tr>';
        }
        divstring += '</tbody>';
        $('#list-infordocs').append(divstring);
    });

}
function showattachments(file) {
    window.open('/GhqNtt/infordocdownload?file=' + file);
}


