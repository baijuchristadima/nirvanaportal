﻿$(document).ready(function () {
    loadtable("/GhqNtt/GetUploadList", 1);
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/LpoLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoice();
    })
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $('.modal-dismiss').click(function () {
        $.magnificPopup.close();
    });

    $(document).on("click", "#btnUpload", function () {
        window.location.href = "/GhqNtt/FileUpload";
    })

    
    $(document).on("click", ".fa-paperclip", function () {
        var id = $(this).data('detailsid');
        Getinfordocs(id);
        $.magnificPopup.open({
            items: {
                src: "#modaldocs"
            },
            type: 'inline',
            preloader: false,
            modal: true,
        });
    })
   
    
});
function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        debugger
        var response = result.data;
        //var divstring = '<thead><tr><th>#</th><th>LPO#</th><th>LPO Date</th><th>Invoice No</th><th>Invoice Date</th><th>Action</th></tr></thead><tbody>';
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="twentypercentage">LPO#</th><th class="forteenpercentage">LPO Date</th><th class="twentyfivepercentage">Uploaded By</th><th class="forteenpercentage">Uploaded On</th><th class="twentyfivepercentage">Comments</th><th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-UploadList').empty();
        $('#list-UploadList-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i + 1) + '</td>';
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].uploaduser + '</td>';
            divstring += '<td>' + response[i].uploadon + '</td>';
            divstring += '<td>' + response[i].uploadcomments + '</td>';            
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';            
            divstring += '</td></tr>';
        }
        debugger
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-UploadList').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoice(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                footerstring += '<li class="active"><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                footerstring += '<li><a href="#" onclick="searchinvoice(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoice(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-UploadList-pagination').append(footerstring);

    });
}

function searchinvoice(page = 1) {
    debugger
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    var lpofromdate = $('#lpofromdate').val();
    var lpotodate = $('#lpotodate').val();
    var URL = "/GhqNtt/GetUploadList?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate
    + "&lpotodate=" + lpotodate + "&lpofromdate=" + lpofromdate + "&page=" + page;
    loadtable(URL, page)
}

function showDetailsDialog(id) {
    window.open('/GhqNtt/LpoDetails?Id=' + id, '_blank');
}

