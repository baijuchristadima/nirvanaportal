﻿$(document).ready(function () {
    loadtable("/GhqNtt/GetOpenInvoicesLoad", 1);   
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/GhqInvoicesLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchinvoicesload();
    })     
    $(document).on("click", ".fa-eye", function () {
        var id = $(this).data('detailsid');
        showDetailsDialog(id);
    })
    $(document).on("click", ".fa-paperclip", function () {
        var invoiceno = $(this).data('detailsid');
        showattachments(invoiceno);
    })
});

function loadtable(URL, page) {
    debugger
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th class="fivepercentage">Sl#</th><th class="forteenpercentage">LPO#</th><th class="forteenpercentage">LPO Date</th><th class="forteenpercentage">Invoice No</th><th class="forteenpercentage">Invoice Date</th><th class="twentypercentage">Verified By</th><th class="forteenpercentage">Verified On</th></th><th class="fivepercentage">Action</th></tr></thead><tbody>';
        $('#list-InvoicesLoad').empty();
        $('#list-InvoicesLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            debugger
            divstring += '<tr><td>' + (i+1) + '</td>';            
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].verifiedby + '</td>';
            divstring += '<td>' + response[i].verifiedon + '</td>';            
            divstring += '<td><a href="#" title="View Details"><i class="fa fa-eye fa-lg" data-DetailsID="' + response[i].id + '"></i></a>';
            debugger
            if (response[i].isAttached == true) {
                debugger
                divstring += '<a href="#" title="Attachment"><i class="fa fa-paperclip" data-DetailsID="' + response[i].lpono + '"></i></a>'
            } else {
                divstring += '<a href="#" title="Invalid Attachment" disabled><i class="fa fa-paperclip attached" data-DetailsID="' + response[i].lpono + '"></i></a>'
            }
          
            divstring += '</td></tr>';  
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-InvoicesLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchinvoicesload(1);return false;"><span class="fa fa-step-backward"></span></a></li>';

        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {

                footerstring += '<li class="active"><a href="#" onclick="searchinvoicesload(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {

                footerstring += '<li><a href="#" onclick="searchinvoicesload(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchinvoicesload(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-InvoicesLoad-pagination').append(footerstring);

    });
}
function searchinvoicesload(page = 1) {    
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serlpofromdate = $('#serlpofromdate').val();
    var serlpotodate = $('#serlpotodate').val();    
    var serinvfromdate = $('#serinvfromdate').val();
    var serinvtodate = $('#serinvtodate').val();
    var serpurpose = $('#serpurpose').val();
    debugger

    var URL = "/GhqNtt/GetOpenInvoicesLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serlpofromdate=" + serlpofromdate + "&serlpotodate=" + serlpotodate + "&serinvfromdate=" + serinvfromdate + "&serinvtodate=" + serinvtodate + "&serpurpose=" + serpurpose + "&page=" + page;
    loadtable(URL, page)
}
function showDetailsDialog(id) {
    debugger
    window.open('/GhqNtt/InvoiceDetailsVerifiedLoad?Id=' + id, '_blank');
}
function showattachments(invoiceno) {
    window.open('/GhqNtt/attachmentcopy?invoiceno=' + invoiceno);
}


