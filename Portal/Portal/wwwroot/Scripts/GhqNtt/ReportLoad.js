﻿$(document).ready(function () {
    loadtable("/GhqNtt/GetReportLoad", 1);
    $(document).on("click", "#btnreset", function () {
        window.location.href = "/GhqNtt/ReportLoad";
    })
    $(document).on("click", "#btnSearch", function () {
        searchreportLoad();
    })
    $(document).on("click", "#btnExport", function () {
        exportreportLoad();
    })   
});
function loadtable(URL, page) {
    var recordcount = 250;
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;

        var divstring = '<thead><tr><th>Sl#</th><th>Invoice#</th><th>Inv Date</th><th>Lpo#</th><th>Lpo Date</th><th>Pax</th><th>Class</th><th>Name</th><th>Reference#</th><th>Book#</th><th>Route</th><th>Destination</th><th>Purpose</th><th>Fare</th><th>Tax</th><th>Yq</th><th>SC</th><th>Total</th><th>Paid</th><th>Status</th><th>Tkt_type</th></tr></thead><tbody>';
        $('#list-ReportLoad').empty();
        $('#list-ReportLoad-pagination').empty();
        for (var i = 0; i < response.length; i++) {
            divstring += '<tr><td>' + (i + 1) + '</td>';
            divstring += '<td>' + response[i].invoiceno + '</td>';
            divstring += '<td>' + response[i].invoicedate + '</td>';
            divstring += '<td>' + response[i].lpono + '</td>';
            divstring += '<td>' + response[i].lpodate + '</td>';
            divstring += '<td>' + response[i].pax + '</td>';
            divstring += '<td>' + response[i].tclass + '</td>';
            divstring += '<td>' + response[i].name + '</td>';
            divstring += '<td>' + response[i].referenceNo + '</td>';
            divstring += '<td>' + response[i].bookingnumber + '</td>';
            divstring += '<td>' + response[i].route + '</td>';
            divstring += '<td>' + response[i].destination + '</td>';
            divstring += '<td>' + response[i].purpose + '</td>';
            divstring += '<td>' + response[i].fare + '</td>';
            divstring += '<td>' + response[i].tax + '</td>';
            divstring += '<td>' + response[i].yq + '</td>';
            divstring += '<td>' + response[i].service_charge + '</td>';
            divstring += '<td>' + response[i].total + '</td>';
            divstring += '<td>' + response[i].payStatus + '</td>';
            divstring += '<td>' + response[i].status + '</td>';
            divstring += '<td>' + response[i].tkt_type + '</td></tr>';           
        }
        var start = page > 2 ? page - 2 : 0;
        var maximum = page * recordcount > result.total ? result.total : page * recordcount;
        divstring += '</tbody></table >';
        $('#list-ReportLoad').append(divstring);
        var footerstring = '<div class="col-sm-12 col-md-4"><div class="dataTables_info" id="datatable-default_info" role="status" aria-live="polite">Showing ' + (((page - 1) * recordcount) + 1) + ' to ' + maximum + ' of ' + result.total + ' entries</div>' +
            '</div> <div class="col-sm-12 col-md-8"><div class="dataTables_paginate paging_bs_normal" id="datatable-default_paginate"><ul class="pagination"><li class="prev"><a href="#" onclick="searchsoaLoad(1);return false;"><span class="fa fa-step-backward"></span></a></li>';
        debugger;
        for (var i = start; i < (start + 4) && i < (result.total / recordcount); i++) {
            if ((i + 1) == page) {
                debugger;
                footerstring += '<li class="active"><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
            else {
                debugger;
                footerstring += '<li><a href="#" onclick="searchsoaLoad(' + (i + 1) + ');return false;">' + (i + 1) + '</a></li>';
            }
        }
        footerstring += '<li class="next"><a href="#" onclick="searchsoaLoad(' + Math.ceil((result.total / recordcount)) + ');return false;"><span class="fa fa-step-forward"></span></a></li></ul></div></div>';
        $('#list-SoaLoad-pagination').append(footerstring);

    });
}
function searchreportLoad(page = 1) {
    //alert();
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    var serpassenger = $('#serpassenger').val();
    var URL = "/GhqNtt/GetReportLoad?serlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate + "&serpassenger=" + serpassenger + "&page=" + page;
    loadtable(URL, page)
}
function showDetailsDialog(id) {
    window.open('/GhqNtt/InvoiceDetailsVerifiedLoad?Id=' + id, '_blank');
}
function showattachments(invoiceno) {
    window.open('/GhqNtt/attachmentcopy?invoiceno=' + invoiceno);
}
function exportreportLoad(page = 1) {
    var serlpono = $('#serlpono').val();
    var serinvoiceno = $('#serinvoiceno').val();
    var serfromdate = $('#serfromdate').val();
    var sertodate = $('#sertodate').val();
    window.location.href = "/GhqNtt/exportReport?sserlpono=" + serlpono + "&serinvoiceno=" + serinvoiceno + "&serfromdate=" + serfromdate + "&sertodate=" + sertodate;

}


