﻿$(document).ready(function () {
    if ($('#SuccessData').val() == "Success") {
        alert("Success");
        $("#btnadd").attr("disabled", true);
        $("#btnapprove").attr("disabled", true);
    }
    var id = $('#id').val();
    loadtable("/GhqNtt/GetInvoicedetailsVerified?invoiceid=" + id, 1, 1);
});

$(document).on("click", "#btnExport", function () {
    debugger
    var ids = document.getElementById("lpono").value;
    exportinvoicesload(ids)
})

$(document).on("click", "#btnreject", function () {
    var invno = document.getElementById("invno").value;
    rejectInvoice(invno)

})

function rejectInvoice(invno) {

  debugger
    $.ajax({
        url: '/GhqNtt/GhqRejectInvoiceVerified',
        method: 'POST',
        data: {
            invno: invno
        },
        dataType: "json",
        async: false
    }).done(function (result) {
       // window.location.reload();
        window.location.href = "/GhqNtt/GhqInvoicesLoad";
    });

}

function exportinvoicesload(ids) {
    window.location.href = "/GhqNtt/exportinvoicesload?selectedIDs=" + ids;
}

function loadtable(URL, page) {
    debugger
    var recordcount = 2
    $.ajax({
        url: URL,
        method: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false
    }).done(function (result) {
        var response = result.data;
        var divstring = '<thead><tr><th>Sl#</th><th>Code</th><th>Ticket</th><th>Class</th><th>Ref No</th><th>Passenger</th><th>Route</th></th><th>Fare</th></th><th>YQ</th><th>Tax</th><th>SC</th><th>Total</th></tr></thead><tbody>';
        var divstringexcess = '<thead><tr><th>Sl#</th><th>Code</th><th>Ticket</th><th>Class</th><th>Ref No</th><th>Passenger</th><th>Route</th></th><th>Fare</th></th><th>YQ</th><th>Tax</th><th>SC</th><th>Total</th></tr></thead><tbody>';
        $('#list-InvoiceDetails').empty();
        $('#list-ExcessInvoiceDetails').empty();
        for (var i = 0; i < response.length; i++) {

            if (response[i].ticket_excess_baggage_pices == null || response[i].ticket_excess_baggage_pices == 0 || response[i].ticket_excess_baggage == null || response[i].ticket_excess_baggage == 0) {
                divstring += '<tr><td>' + (i + 1) + '</td>';
                divstring += '<td>' + response[i].code + '</td>';
                divstring += '<td>' + response[i].bookno + '</td>';
                divstring += '<td>' + response[i].tic_class + '</td>';
                divstring += '<td>' + response[i].refno + '</td>';
                divstring += '<td>' + response[i].pax + '</td>';
                divstring += '<td>' + response[i].route + '</td>';
                divstring += '<td>' + response[i].fare + '</td>';
                divstring += '<td>' + response[i].yq + '</td>';
                divstring += '<td>' + response[i].tax + '</td>';
                divstring += '<td>' + response[i].sc + '</td>';
                divstring += '<td>' + response[i].total + '</td></tr>';
            } else {
                divstringexcess += '<tr><td>' + (i + 1) + '</td>';
                divstringexcess += '<td>' + response[i].code + '</td>';
                divstringexcess += '<td>' + response[i].bookno + '</td>';
                divstringexcess += '<td>' + response[i].tic_class + '</td>';
                divstringexcess += '<td>' + response[i].refno + '</td>';
                divstringexcess += '<td>' + response[i].pax + '</td>';
                divstringexcess += '<td>' + response[i].route + '</td>';
                divstringexcess += '<td>' + response[i].fare + '</td>';
                divstringexcess += '<td>' + response[i].yq + '</td>';
                divstringexcess += '<td>' + response[i].tax + '</td>';
                divstringexcess += '<td>' + response[i].sc + '</td>';
                divstringexcess += '<td>' + response[i].total + '</td></tr>';

            }

           
        }
        //divstring += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;font-size: 15px;">Total</th><td style="font-size: 15px;"><b>' + result.totalFare + '</b></td><td style="font-size: 15px;"><b>' + result.totalYq + '</b></td><td style="font-size: 15px;"><b>' + result.totalTax + '</b></td><td style="font-size: 15px;"><b>' + result.totalSc + '</b></td><td style="font-size: 15px;"><b>' + result.totalInvoice + '</b></td></tr></tfoot></table >';
        divstring += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;" class="total">Total</th><td class="total"><b>' + result.totalFare + '</b></td><td class="total"><b>' + result.totalYq + '</b></td><td class="total"><b>' + result.totalTax + '</b></td><td class="total"><b>' + result.totalSc + '</b></td><td class="total"><b>' + result.totalInvoice + '</b></td></tr></tfoot></table >';
        divstringexcess += '</tbody><tfoot><tr><th id="total" colspan="7" style="text-align: right;" class="total">Total</th><td class="total"><b>' + result.excessTotalFare + '</b></td><td class="total"><b>' + result.excessTotalYq + '</b></td><td class="total"><b>' + result.excessTotalTax + '</b></td><td class="total"><b>' + result.excessTotalSc + '</b></td><td class="total"><b>' + result.excessTotalInvoice + '</b></td></tr></tfoot></table >';
        $('#list-InvoiceDetails').append(divstring);       
        $('#list-ExcessInvoiceDetails').append(divstringexcess);
    });
}