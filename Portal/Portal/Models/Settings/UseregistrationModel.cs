﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Settings
{
    public class RegistrationModel
    {
        [Required]
      
        //[StringLength(15, ErrorMessage = "Name length can't be more than 15.")]
        public string Name { get; set; }
        [Required]
      
        public string Mobile { get; set; }
        [Required]
        
        public string Email { get; set; }

        [Required]
       
        public string Password { get; set; }
   
       
    }
}
