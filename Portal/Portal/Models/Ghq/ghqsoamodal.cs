﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqsoamodal
    {
        public Int64 id { get; set; }
        public string lpono { get; set; }
        public string lpodate { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string balance { get; set; }
        public long lpoid { get; set; }
        public bool IsAttached { get; set; }
    }
}
