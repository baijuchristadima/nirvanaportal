﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqreport
    {
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string lpono { get; set; }
        public string lpodate { get; set; }
        public string tclass { get; set; }
        public string name { get; set; }
        public string referenceNo { get; set; }
        public string bookingnumber { get; set; }
        public string route { get; set; }
        public string destination { get; set; }
        public string purpose { get; set; }
        public string fare { get; set; }
        public string tax { get; set; }
        public string yq { get; set; }
        public string service_charge { get; set; }
        public string total { get; set; }
        public string payStatus { get; set; }
        public string status { get; set; }
        public string tkt_type { get; set; }
        public string pax { get; set; }

    }
}
