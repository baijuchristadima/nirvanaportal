﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqinvoicedetailmodal
    {
        public long id { get; set; }
        public string code { get; set; }
        public string bookno { get; set; }
        public string tic_class { get; set; }
        public string refno { get; set; }
        public string pax { get; set; }
        public string route { get; set; }
        public string fare { get; set; }
        public string yq { get; set; }
        public string tax { get; set; }
        public string sc { get; set; }
        public string total { get; set; }
        public int status { get; set; }
        public string ticket_excess_baggage_pices { get; set; }
        public string ticket_excess_baggage { get; set; }
        public long lpoid { get; set; }
    }
}
