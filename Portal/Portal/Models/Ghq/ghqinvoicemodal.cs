﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqinvoicemodal
    {
        public Int64 id { get; set; }
        public string companyname { get; set; }
        public string lpono { get; set; }
        public string lpodate { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string verifiedby { get; set; }
        public string verifiedon { get; set; }

        public string ghqverifiedby { get; set; }
        public string ghqverifiedon { get; set; }
        public int paystatus { get; set; }

        public string uploaduser { get; set; }
        public string uploadon { get; set; }

        public string uploadcomments { get; set; }
        public string lpopurpose { get; set; }
        public bool IsAttached { get; set; }
    }
}
