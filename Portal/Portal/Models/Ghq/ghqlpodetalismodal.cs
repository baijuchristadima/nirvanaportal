﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqlpodetalismodal
    {
        public long id { get; set; }
        public string lpodate { get; set; }
        public string route { get; set; }
        public string pax { get; set; }
        public string refno { get; set; }
        public string lpoclass { get; set; }
        public string paxdob { get; set; }
        public string name { get; set; }
    }
}
