﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Ghq
{
    public class ghqinvoicedetailsexportmodal
    {
        public string  TRAVEL_AGENCY { get; set; }
        public string LPO_ID { get; set; }
        public string LPO_DATE { get; set; }
        public string REF_NO { get; set; }
        public string INV_NO { get; set; }
        public string INV_DATE { get; set; }
        public string INV_AMOUNT { get; set; }
        public string TIC_NO { get; set; }
        public string TIC_DATE { get; set; }
        public string TIC_AMOUNT { get; set; }
        public string TIC_WEIGHT { get; set; }
        public string TIC_ROUTE { get; set; }
        public string TICKET_CLASS { get; set; }
        public string TICKET_AIR_LINE { get; set; }
        public string CITY_CODE { get; set; }
        public string ACCEPTED { get; set; }
        public string REMARKS { get; set; }
        public string UPDATE_STATUS { get; set; }
        public string TKT_TYPE { get; set; }
        public string NAME_ON_TICKET { get; set; }
        public string TICKET_FARE { get; set; }
        public string FUEL_CHARGE { get; set; }
        public string TAX { get; set; }
        public string SERVICE_CHARGE { get; set; }
        public string DISCOUNT { get; set; }
        public string TIC_WEIGHT_UNIT { get; set; }
        public string TAX_REGISTRATION_NO { get; set; }
        public string TAX_TOTAL_AMOUNT { get; set; }
        public string TAX_TYPE { get; set; }
        public string TAX_RATE { get; set; }
        public string TAX_AMOUNT { get; set; }
    }
}
