﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Nyu
{
    public class nyuinvoicedetailmodal
    {
        public long id { get;set; }
        public string servicetype { get; set; }
        public string bookno { get; set; }
        public string pax { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
        public string provider { get; set; }
        public string route { get; set; }
        public string fare { get; set; }
        public string tax { get; set; }
        public string vat { get; set; }
        public string total { get; set; }
        public string authcode { get; set; }
        public string account { get; set; }
        public string fund { get; set; }
        public string org { get; set; }
        public string project { get; set; }
        public string program { get; set; }
        public int status { get; set; }
        public string serviceFee { get; set; }


        public long invoiceid { get; set; }
    }
}
