﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Nyu
{
    public class nyuinvoicedetailexportmodal
    {

        public string ReferenceNo { get; set; }
        public string RequestorId { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string NAME { get; set; }
        public string AUTHORISEDBY { get; set; }
        public string TRAVELCODE { get; set; }
        public string AirTicket { get; set; }
        public string Hotel { get; set; }
        public string GroundTransport { get; set; }
        public string OtherServices { get; set; }
        public string VAT { get; set; }
        public string Exceptio { get; set; }
        public string Total { get; set; }
        public string Fund { get; set; }
        public string Account { get; set; }
        public string ORG { get; set; }
        public string Project { get; set; }
        public string Progra { get; set; }
        public string TravelDate { get; set; }
        public string ReturnDate { get; set; }
        public string OriginCity { get; set; }
        public string SEC2 { get; set; }
        public string SEC3 { get; set; }
        public string SEC4 { get; set; }
        public string SEC5 { get; set; }
        public string SEC6 { get; set; }
        public string FinalCity { get; set; }
        public string CHECK_IN { get; set; }
        public string CHECK_OUT { get; set; }
    }
}
