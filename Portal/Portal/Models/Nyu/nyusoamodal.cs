﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Nyu
{
    public class nyusoamodal
    {
        public Int64 id { get; set; }
        public string lpono { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string balance { get; set; }

        public Int64 invoiceid { get; set; }
        public bool IsAttached { get; set; }
    }
}
