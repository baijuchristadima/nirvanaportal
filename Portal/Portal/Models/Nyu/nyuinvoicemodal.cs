﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Nyu
{
    public class nyuinvoicemodal
    {
        public Int64 id { get; set; }
        public string lpono { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string verifiedby { get; set; }
        public string verifiedon { get; set; }
        public int paystatus { get; set; }
        public string travelcode { get; set; }
        public string authcode { get; set; }
        public int status { get; set; } 
        public string company { get; set; }
        public List<nyuinvoicedetailmodal> nyuinvoicedetailmodal { get; set; }
        public bool IsAttached { get; set; }
    }
}
