﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Corporate
{
    public class corporateinvoicedetailsmodal
    {
       public long id { get; set; }
        public string servicetype { get; set; }
    
        public string bookno { get; set; }
        public string guest { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string provider { get; set; }
        public string route { get; set; }
        public string fare { get; set; }
        public string tax { get; set; }
        public string vat { get; set; }
        public string sc { get; set; }
        public int status { get; set; }
        public string total { get; set; }
        public string issuedate { get; set; }
        public int billinginvoiceLine { get; set; }
        public long invoiceid { get; set; }

        public string type { get; set; }
    }
}
