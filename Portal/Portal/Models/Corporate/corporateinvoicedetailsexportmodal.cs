﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Corporate
{
    public class corporateinvoicedetailsexportmodal
    {
        public string companyname { get; set; }
        public string lpono { get; set; }
        public string lpodate { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string servicetype { get; set; }
        public string bookno { get; set; }
        public string guest { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string provider { get; set; }
        public string route { get; set; }
        public double fare { get; set; }
        public double tax { get; set; }
        public double vat { get; set; }
        public double sc { get; set; }

        public double total { get; set; }

        public string issuedate { get; set; }

        public string type { get; set; }
    }
}
