﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Corporate
{
    public class corporateinvoicemodal
    {
        public Int64 id { get; set; }
        public string companyname { get; set; }
        public string lpono { get; set; }
        public string lpodate { get; set; }
        public string invoiceno { get; set; }
        public string invoicedate { get; set; }
        public string verifiedby { get; set; }
        public string verifiedon { get; set; }
        public string attachment { get; set; }
        public int paystatus { get; set; }      
    }
}
