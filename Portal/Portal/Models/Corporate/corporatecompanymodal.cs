﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.UI.Models.Corporate
{
    public class corporatecompanymodal
    {
        
            public Int64 id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public string address { get; set; }
            public string secaddress { get; set; }

            public string thirdaddress { get; set; }

            public string forthaddress { get; set; }
            public string phone { get; set; }
            public string mail { get; set; }
            public string invformat { get; set; }
            public string currency { get; set; }
            public int status { get; set; }
            public string vat { get; set; }
            public string createdon { get; set; }

            public string createdby { get; set; }
        }
    
}
