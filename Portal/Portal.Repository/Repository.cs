﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Nirvana_Portal.Data;
using Portal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
namespace Portal.Repository
{
    public class Repository<T> : IRepository<T> where T : auditdetail
    {
        private readonly ApplicationDbContext context;
        private DbSet<T> entities;
        string errorMessage = string.Empty;
        private IHttpContextAccessor _contextAccessor;

        public Repository(ApplicationDbContext context, IHttpContextAccessor contextAccessor)
        {
            this.context = context;
            entities = context.Set<T>();
            _contextAccessor = contextAccessor;
        }
        public IQueryable<T> GetAll()
        {
            return entities.AsQueryable();
        }

        public T Get(long id)
        {
            return entities.Find(id);
        }
        public void Insert(T entity)
        {
            IPHostEntry heserver = Dns.GetHostEntry(Dns.GetHostName());
            var ip = heserver.AddressList[1].ToString();
            string UserId = "6d528d47-0943-4054-9384-64cff1b0dd83";
            //if (_contextAccessor.HttpContext.Session.GetString("UserId") != null)
            //{
            //    UserId = int.Parse(_contextAccessor.HttpContext.Session.GetString("UserId"));
            //}
            //if (entity == null)
            //{
            //    throw new ArgumentNullException("entity");
            //}
            entity.createdby = UserId;
            entity.createddate = DateTime.Now;
            entity.modifieddate = DateTime.Now;
            entity.modifiedby = UserId;
            entity.ipaddress = ip;
            entity.status = 1;
            entities.Add(entity);
            SaveChange();
        }

        public void Update(T entity)
        {
            IPHostEntry heserver = Dns.GetHostEntry(Dns.GetHostName());
            var ip = heserver.AddressList[1].ToString();
            long UserId = 0;
            //if (_contextAccessor.HttpContext.Session.GetString("UserId") != null)
            //{
            //    UserId = int.Parse(_contextAccessor.HttpContext.Session.GetString("UserId"));
            //}
            //if (entity == null)
            //{
            //    throw new ArgumentNullException("entity");
            //}
           // entity.modifiedby = UserId;
            entity.ipaddress = ip;
            entity.modifieddate = DateTime.Now;
            context.Entry(entity).State = EntityState.Modified;
            context.Entry(entity).Property(nameof(entity.createdby)).IsModified = false;
            context.Entry(entity).Property(nameof(entity.createddate)).IsModified = false;
            SaveChange();
        }




        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            SaveChange();
        }
        private void SaveChange()
        {
            context.SaveChanges();
        }

        public IQueryable<T> GetAllDeleted()
        {
            return entities.AsQueryable();
        }
    }
}
