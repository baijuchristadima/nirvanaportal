﻿using Portal.Data;
using System.Linq;

namespace Portal.Repository
{
    public interface IRepository<T> where T : auditdetail
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAllDeleted();
        T Get(long id);       
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
