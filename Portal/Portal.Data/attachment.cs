﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Data
{
   public class attachment : auditdetail 
    {
        public long invoiceid { get; set; }
        public string path { get; set; }
        public string type { get; set; }
    }
}
