﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Corporate
{
    public class corporatecompany :auditdetail
    {
        public string code { get; set; }
        public string name { get; set; }
        public string firstaddress { get; set; }
        public string secondaddress { get; set; }
        public string thirdaddress { get; set; }
        public string forthaddress { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public string invformat { get; set; }        
        public string currency { get; set; }
        public string regno { get; set; }
        public string address { get; set; }
        [ForeignKey("corporatecurrencies")]
        public Int64 currid { get; set; }
        public virtual corporatecurrencies Corporatecurrencies { get; set; }

        public int format { get; set; }
        public int secinv { get; set; }
        public int batch { get; set; }
        public string TRNNo { get; set; }
        public string minorunit { get; set; }
    }
}
