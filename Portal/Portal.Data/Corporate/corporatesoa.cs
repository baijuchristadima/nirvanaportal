﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Corporate
{
    public class corporatesoa : auditdetail
    {
        [ForeignKey("corporateinvoice")]
        public Int64 invoiceid { get; set; }
        public virtual corporateinvoice corporateinvoice { get; set; }
        public double debit { get; set; }

        public double credit { get; set; }
        public string details { get; set; }
        public int paystatus { get; set; }
        [ForeignKey("corporatecompany")]
        public Int64 companyid { get; set; }
        public virtual corporatecompany Corporatecompany { get; set; }
        public string invoiceno { get; set; }
        public DateTime invoicedate { get; set; }
        public string ipo { get; set; }
        public DateTime ipodate { get; set; }
        public string narration { get; set; }
        public double balance { get; set; }
    }
}
