﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Corporate
{
    public class corporateuseassigns : auditdetail
    {
        [ForeignKey("corporateinvoice")]
        public Int64 userid { get; set; }
        public virtual corporateinvoice Corporateinvoice{get;set;}
        [ForeignKey("corporatecompany")]
        public Int64 companyid { get; set; }
        public virtual corporatecompany Corporatecompany { get; set; }

    }
}
