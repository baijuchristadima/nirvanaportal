﻿using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Corporate
{
    public class corporateinvoice :auditdetail
    {
        [ForeignKey("corporatecompany")]
        public Int64 companyid { get; set; }
        public virtual corporatecompany corporatecompany { get; set; }
        public string lpono { get; set; }
        public DateTime? lpodate { get; set; }
        public string invoiceno { get; set; }
        public DateTime? invoicedate { get; set; }
        [ForeignKey("verifieduser")]
        public string verifiedby { get; set; }
        public virtual ApplicationUser verifieduser { get; set; }
        public DateTime? verifiedon { get; set; }       
        public int paystatus { get; set; }
        public string billinginvoice { get; set; }
        public string crmno { get; set; }
        public bool attach { get; set; }
        public int ispaid { get; set; }
        public string attachpath { get; set; }
        public int batch { get; set; }

    }

    public class corporateInvoicedetail :auditdetail
    {
        [ForeignKey("corporateinvoice")]
        public Int64 invoiceid { get; set; }
        public virtual corporateinvoice corporateinvoice { get; set; }
        public string servicetype { get; set; }

        public DateTime? issuedate { get; set; }
        public string bookno { get; set; }
        public string guest { get; set; }
        public DateTime? from { get; set; }
        public DateTime? to { get; set; }
        public string provider { get; set; }
        public string route { get; set; }
        public double fare { get; set; }        
        public double tax { get; set; }
        public double vat { get; set; }
        public double sc { get; set; }
        public string type { get; set; }
        public int  billinginvoiceLine { get; set; }
        public string suppref { get; set; }
        public string tclass { get; set; }
        public string aircode { get; set; }
        public DateTime checkintime { get; set; }
        public DateTime checkoutdate { get; set; }
        public string serviceprovider { get; set; }
        public string suppliername { get; set; }
        public string passenger { get; set; }
        public int unit { get; set; }
        public string city { get; set; }
        public string roomtype { get; set; }
        public double yq { get; set; }
        public double servicefree { get; set; }
        public double total { get; set; }
        public string providercode { get; set; }
        public string pnr { get; set; }
        public DateTime ticketdate { get; set; }
        public string costcenter { get; set; }
        public string prjcode { get; set; }
        public string approver { get; set; }
        public string flightno { get; set; }
        public int odr { get; set; }
        public string alcode { get; set; }
        public string tikclass { get; set; }            
    }

  

    public class rmdatavalue : auditdetail
    {
        [ForeignKey("corporateInvoicedetail")]
        public Int64 detailid { get; set; }
        public virtual corporateInvoicedetail corporateInvoicedetail { get; set; }      
        public string rmcode { get; set; }
        public string rmname { get; set; }
        public string rmvalue { get; set; }
    }
}
