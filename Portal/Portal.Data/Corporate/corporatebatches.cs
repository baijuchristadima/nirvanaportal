﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Corporate
{
    public class corporatebatches : auditdetail
    {
            [ForeignKey("corporatecompany")]
            public Int64 companyid { get; set; }
            public virtual corporatecompany corporatecompany { get; set; }
            public DateTime? fromdate { get; set; }
            public DateTime? todate { get; set; }
            public string closedby { get; set; }
            public DateTime? closedon { get; set; }      
    }
}
