﻿using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data
{
   public class auditdetail
    {
        [Key]
        public Int64 id { get; set; }
        [ForeignKey("createduser")]
        public string createdby { get; set; }
        public virtual ApplicationUser createduser { get; set; }
        [ForeignKey("modifieduser")]
        public string modifiedby { get; set; }
        public virtual ApplicationUser modifieduser { get; set; }
        public DateTime createddate { get; set; }
        public DateTime modifieddate { get; set; }
        public string ipaddress { get; set; }
        public int status { get; set; }
    }
}
