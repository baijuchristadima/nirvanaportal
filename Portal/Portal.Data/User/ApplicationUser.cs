﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Data
{
   public class ApplicationUser : IdentityUser
    {
        public string fullname { get; set; }
    }
}
