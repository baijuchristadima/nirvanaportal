﻿using System;
using System.Collections.Generic;
using System.Text;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Nirvana_Portal.Data.Ghq;
using Nirvana_Portal.Data.Nyu;
using Portal.Data;
using Portal.Data.Corporate;
using Portal.Data.Ghq;

namespace Portal.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
      
        public DbSet<corporatecompany> corporatecompany { get; set; }
        public DbSet<corporateinvoice> corporateinvoice { get; set; }
        public DbSet<corporateInvoicedetail> corporateInvoicedetail { get; set; }
        public DbSet<corporatesoa> corporatesoa { get; set; }
        public DbSet<nyuinvoice> nyuinvoice { get; set; }
        public DbSet<nyuInvoicedetail> nyuInvoicedetail { get; set; }
        public DbSet<nyusoa> nyusoa { get; set; }
        public DbSet<ghqinvoice> ghqinvoice { get; set; }
        public DbSet<ghqinvoicedetail> ghqinvoicedetail { get; set; }
        public DbSet<ghqsoa> ghqsoa { get; set; }
        public DbSet<rmdatavalue> rmdatavalue { get; set; }
        public DbSet<attachment> attachment { get; set; }
        public DbSet<corporatebatches> corporatebatches { get; set; }
        public DbSet<corporatecurrencies> corporatecurrencies { get; set; }
        public DbSet<corporateuseassigns> corporateuseassigns { get; set; }
        public DbSet<ghqhotelinvoicedetails> ghqhotelinvoicedetails { get; set; }
        public DbSet<ghqhotelinvoicemains> ghqhotelinvoicemains { get; set; }
        public DbSet<ghqhotellpodetails> ghqhotellpodetails{ get; set; }
        public DbSet<ghqhotelsoas> ghqhotelsoas { get; set; }
        public DbSet<ghqticketlpodetails> ghqticketlpodetails { get; set; }

        public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {

        }
    }
}
