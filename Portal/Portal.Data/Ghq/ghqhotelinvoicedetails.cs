﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Ghq
{
   public class ghqhotelinvoicedetails : auditdetail
    {
        [ForeignKey("ghqhotelinvoicemains")]
        public Int64 lpoid { get; set; }
        public virtual ghqhotelinvoicemains Ghqhotelinvoice { get; set; }
        public string supportdoc { get; set; }
        public DateTime checkintime { get; set; }
        public DateTime checkouttime { get; set; }
        public string serviceprovider { get; set; }
        public string suppliername { get; set; }
        public string passenger { get; set; }
        public int unit { get; set; }
        public string city { get; set; }
        public string roomtype { get; set; }
        public double fare { get; set; }
        public double servicefee { get; set; }
        public double taxone { get; set; }
        public double taxtwo { get; set; }
        public double total { get; set; }
        public int lpoorder { get; set; }
        public string xtraservice { get; set; }
        public string xtracode { get; set; }
        public string roomtypeid { get; set; }
        public int days { get; set; }
        public int invorder { get; set; }

    }
}
