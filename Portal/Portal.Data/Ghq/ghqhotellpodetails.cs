﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Ghq
{
    public class ghqhotellpodetails : auditdetail
    {

        [ForeignKey("ghqhotelinvoicemains")]
        public Int64 lpoid { get; set; }
        public virtual ghqhotelinvoicemains Ghqhotelinvoicemains { get; set; }
        public string reservation_number { get; set; }
        public DateTime reservation_date_from { get; set; }
        public DateTime reservation_date_to { get; set; }
        public DateTime reservation_hour_from { get; set; }
        public DateTime reservation_hour_to { get; set; }
        public string previouse_trx_no { get; set; }
        public string reservation_type { get; set; }
        public string vendor_number { get; set; }
        public string vendor_site { get; set; }
        public string to_who_concern { get; set; }
        public string according_name { get; set; }
        public string according_tel { get; set; }
        public string creator_id { get; set; }
        public string creation_date { get; set; }
        public string approve2_id { get; set; }
        public string approve2_date { get; set; }
        public string po_allowance_type { get; set; }
        public string po_allowance_id { get; set; }
        public string note { get; set; }
        public string note3 { get; set; }
        public string note4 { get; set; }
        public string attribute1 { get; set; }
        public string attribute3 { get; set; }
        public string attribute4 { get; set; }
        public string attribute5 { get; set; }
        public string seq { get; set; }
        public string service_code { get; set; }
        public string service_name { get; set; }
        public string unit_no { get; set; }
        public string night_no { get; set; }
        public double unit_price { get; set; }
        public double tax_price { get; set; }
        public double added_tax { get; set; }
        public double amount { get; set; }
        public string note_1 { get; set; }
        public DateTime from_date { get; set; }
        public DateTime to_date { get; set; }
        public string attribute1_1 { get; set; }
        public string attribute2_1 { get; set; }
        public string attribute3_1 { get; set; }
        public string attribute4_1 { get; set; }
        public string attribute5_1 { get; set; }
        public string extra_service_code { get; set; }
        public string extra_service_name { get; set; }
        public string enclude_flag { get; set; }
        public string attribute3_2 { get; set; }
        public string attribute4_2 { get; set; }
        public string attribute5_2 { get; set; }
        public string seq_1 { get; set; }
        public string name { get; set; }
        public string night_no_1 { get; set; }
        public string from_date_1 { get; set; }
        public string to_date_1 { get; set; }
        public string attribute1_3 { get; set; }
        public string attribute2_3 { get; set; }
        public string attribute3_3 { get; set; }
        public string attribute4_3 { get; set; }
        public string attribute5_3 { get; set; }
    }
}
