﻿using Microsoft.AspNetCore.SignalR;
using Nirvana_Portal.Data;
using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Nirvana_Portal.Data.Ghq
{
    public class ghqinvoice :auditdetail
    {
        public string lpono { get; set; }
        public DateTime? lpodate { get; set; }
        public string invoiceno { get; set; }
        public DateTime? invoicedate { get; set; } 
        [ForeignKey("accveriuser")]
        public string nttverifiedby { get; set; }
        public virtual ApplicationUser accveriuser { get; set; }
        public DateTime? nttverifiedon { get; set; }
        [ForeignKey("ghqveriuser")]
        public string ghqverifiedby { get; set; }
        public virtual ApplicationUser ghqveriuser { get; set; }
        public DateTime? ghqverifiedon { get; set; }

        [ForeignKey("uploaduser")]
        public string uploadedby { get; set; }
        public virtual ApplicationUser uploaduser { get; set; }

        public DateTime? uploadedon { get; set; }

        public string uploadedcomments { get; set; }

        public int paystatus { get; set; }

        public string purpose { get; set; }
        public string rejuser { get; set; }
        public DateTime rejdate { get; set; }
        public string rejreason { get; set; }
        public int disputed { get; set; }
    }
    public class ghqinvoicedetail : auditdetail
    {
        [ForeignKey("ghqinvoice")]
        public Int64 invoiceid { get; set; }
        public virtual ghqinvoice ghqinvoice { get; set; }
        public string code { get; set; }
        public string bookno { get; set; }
        public string tic_class { get; set; }
        public string refno { get; set; }
        public string pax { get; set; }
        public string route { get; set; }
        public double fare {get;set;}
        public double yq { get; set; }
        public double tax { get; set; }
        public double sc { get; set; }
        public double discount { get; set; }
        public double total { get; set; }
        public string narration { get; set; }
        public string sec1 { get; set; }
        public string sec2 { get; set; }
        public string sec3 { get; set; }
        public string sec4 { get; set; }
        public string sec5 { get; set; }
        public string sec6 { get; set; }
        public string sec7 { get; set; }
        public string sec8 { get; set; }
        public DateTime? traveldate { get; set; }
        public DateTime? returndate { get; set; }
        public string airlinenumcode { get; set; }
        public int orderno { get; set; }
        public string ticweight { get; set; }
        public string dest { get; set; }
        public string weightunit { get; set; }
        public DateTime? issuedate { get; set; }
        public string piece { get; set; }
        public string weight { get; set; }
        public string name { get; set; }
        public int ticket_order_lpo { get; set; }
        public double service_charge { get; set; }
        public string city_code { get; set; }
        public string acceptedstatus { get; set; }
        public string update_status { get; set; }
        public string tkt_type { get; set; }
        public DateTime tic_date { get; set; }
        public string ticket_excess_baggage_pices { get; set; }
        public string ticket_excess_baggage { get; set; }
        public double zdisc { get; set; }
    }
}
