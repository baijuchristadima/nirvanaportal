﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Ghq
{
    public class ghqhotelsoas : auditdetail
    {
        [ForeignKey("ghqhotelinvoicemains")]
        public Int64 lpoid { get; set; } 
        public string lpono { get; set; }
        public DateTime lpodate { get; set; }
        public string invoiceno { get; set; }
        public DateTime invoicedate { get; set; }
        public string pax { get; set; }
        public double total { get; set; }
        public double balance { get; set; }
    }
}
