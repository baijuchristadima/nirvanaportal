﻿using Nirvana_Portal.Data.Ghq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Ghq
{
    public class ghqticketlpodetails : auditdetail
    {
        [ForeignKey("ghqinvoice")]
        public Int64 lpoid { get; set; }
        public virtual ghqinvoice ghqinvoice { get; set; }
        public DateTime lpodate { get; set; }
        public string route { get; set; }
        public string pax { get; set; }
        public string refno { get; set; }
        public string lpoclass { get; set; }
        public string paxdob { get; set; }
    }
}
