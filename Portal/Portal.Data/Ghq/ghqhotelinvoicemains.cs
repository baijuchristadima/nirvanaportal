﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Data.Ghq
{
   public class ghqhotelinvoicemains : auditdetail
    {
        public string lpono { get; set; }
        public DateTime lpodate { get; set; }
        public string invoiceno { get; set; }
        public DateTime invoicedate { get; set; }
        public Int64 accveriuser { get; set; }
        public DateTime accveridate { get; set; }
        public Int64 ghqveriuser { get; set; }
        public DateTime ghqveridate { get; set; }
        public Int64 rejby { get; set; }
        public DateTime rejdate { get; set; }
        public string rejreason { get; set; }
        public string uploadnotes { get; set; }
        public int uploaduser { get; set; }
        public DateTime uploaddate { get; set; }
        public int batch { get; set; }
        public int paystatus { get; set; }

    }
}
