﻿using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Transactions;

namespace Nirvana_Portal.Data.Ghq
{
  public  class ghqsoa :auditdetail
{
        public string lpono { get; set; }
        public DateTime? lpodate { get; set; }
        public string invoiceno { get; set; }
        public DateTime? invoicedate { get; set; }
        public double debit { get; set; }
        public double credit { get; set; }
        public int paystatus { get; set; }
        [ForeignKey("ghqinvoice")]
        public Int64 lpoid { get; set; }
        public virtual ghqinvoice ghqinvoice { get; set; }
        public string pax { get; set; }
        public double total { get; set; }
        public double balance { get; set; }
        public int purpose { get; set; }

    }
}
