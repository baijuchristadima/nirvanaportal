﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt21092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "uploadedby",
                table: "ghqinvoice",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoice_uploadedby",
                table: "ghqinvoice",
                column: "uploadedby");

            migrationBuilder.AddForeignKey(
                name: "FK_ghqinvoice_AspNetUsers_uploadedby",
                table: "ghqinvoice",
                column: "uploadedby",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ghqinvoice_AspNetUsers_uploadedby",
                table: "ghqinvoice");

            migrationBuilder.DropIndex(
                name: "IX_ghqinvoice_uploadedby",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "uploadedby",
                table: "ghqinvoice");
        }
    }
}
