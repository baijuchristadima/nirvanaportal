﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "rmdatavalue",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false),
                    rmcode = table.Column<string>(nullable: true),
                    rmname = table.Column<string>(nullable: true),
                    rmvalue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rmdatavalue", x => x.id);
                    table.ForeignKey(
                        name: "FK_rmdatavalue_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_rmdatavalue_corporateinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "corporateinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_rmdatavalue_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_rmdatavalue_createdby",
                table: "rmdatavalue",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_rmdatavalue_invoiceid",
                table: "rmdatavalue",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_rmdatavalue_modifiedby",
                table: "rmdatavalue",
                column: "modifiedby");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "rmdatavalue");
        }
    }
}
