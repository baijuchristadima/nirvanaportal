﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal16092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "attachment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false),
                    path = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_attachment", x => x.id);
                    table.ForeignKey(
                        name: "FK_attachment_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_attachment_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_attachment_createdby",
                table: "attachment",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_attachment_modifiedby",
                table: "attachment",
                column: "modifiedby");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "attachment");
        }
    }
}
