﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt210920212 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "dest",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "paystatus",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dest",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "paystatus",
                table: "nyuInvoicedetail");
        }
    }
}
