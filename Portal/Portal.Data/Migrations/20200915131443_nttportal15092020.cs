﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal15092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IDMUniqueID",
                table: "corporateinvoice");

            migrationBuilder.AddColumn<bool>(
                name: "attach",
                table: "corporateinvoice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "crmno",
                table: "corporateinvoice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "attach",
                table: "corporateinvoice");

            migrationBuilder.DropColumn(
                name: "crmno",
                table: "corporateinvoice");

            migrationBuilder.AddColumn<string>(
                name: "IDMUniqueID",
                table: "corporateinvoice",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
