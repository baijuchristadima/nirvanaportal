﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal2020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "balance",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "balance",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "balance",
                table: "corporatesoa");

            migrationBuilder.AddColumn<double>(
                name: "credit",
                table: "nyusoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "debit",
                table: "nyusoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "credit",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "debit",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "credit",
                table: "corporatesoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "debit",
                table: "corporatesoa",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "credit",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "debit",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "credit",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "debit",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "credit",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "debit",
                table: "corporatesoa");

            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "nyusoa",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "ghqsoa",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "corporatesoa",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
