﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt07152020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "unit",
                table: "nyuInvoicedetail");

            migrationBuilder.AddColumn<double>(
                name: "fare",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "servicetype",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "tax",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "vat",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "fare",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "servicetype",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "tax",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "vat",
                table: "nyuInvoicedetail");

            migrationBuilder.AddColumn<int>(
                name: "unit",
                table: "nyuInvoicedetail",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
