﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal202009 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "attachment",
                table: "corporateinvoice");

            migrationBuilder.AddColumn<int>(
                name: "billinginvoiceLine",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "issuedate",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "corporateInvoicedetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "billinginvoiceLine",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "issuedate",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "type",
                table: "corporateInvoicedetail");

            migrationBuilder.AddColumn<string>(
                name: "attachment",
                table: "corporateinvoice",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
