﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt250620 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "createdby",
                table: "corporatecurrency",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "createddate",
                table: "corporatecurrency",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ipaddress",
                table: "corporatecurrency",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedby",
                table: "corporatecurrency",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "modifieddate",
                table: "corporatecurrency",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "status",
                table: "corporatecurrency",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrency_createdby",
                table: "corporatecurrency",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrency_modifiedby",
                table: "corporatecurrency",
                column: "modifiedby");

            migrationBuilder.AddForeignKey(
                name: "FK_corporatecurrency_AspNetUsers_createdby",
                table: "corporatecurrency",
                column: "createdby",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_corporatecurrency_AspNetUsers_modifiedby",
                table: "corporatecurrency",
                column: "modifiedby",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_corporatecurrency_AspNetUsers_createdby",
                table: "corporatecurrency");

            migrationBuilder.DropForeignKey(
                name: "FK_corporatecurrency_AspNetUsers_modifiedby",
                table: "corporatecurrency");

            migrationBuilder.DropIndex(
                name: "IX_corporatecurrency_createdby",
                table: "corporatecurrency");

            migrationBuilder.DropIndex(
                name: "IX_corporatecurrency_modifiedby",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "createdby",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "createddate",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "ipaddress",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "modifiedby",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "modifieddate",
                table: "corporatecurrency");

            migrationBuilder.DropColumn(
                name: "status",
                table: "corporatecurrency");
        }
    }
}
