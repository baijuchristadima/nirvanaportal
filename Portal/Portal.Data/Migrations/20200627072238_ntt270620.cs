﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt270620 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "corporatecompany",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    mail = table.Column<string>(nullable: true),
                    invformat = table.Column<string>(nullable: true),
                    currencyId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporatecompany", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporatecompany_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporatecompany_corporatecurrency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "corporatecurrency",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_corporatecompany_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "corporateinvoice",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    companyid = table.Column<long>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    lpodate = table.Column<DateTime>(nullable: true),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: true),
                    verifiedby = table.Column<string>(nullable: true),
                    verifiedon = table.Column<DateTime>(nullable: true),
                    attachment = table.Column<string>(nullable: true),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporateinvoice", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporateinvoice_corporatecompany_companyid",
                        column: x => x.companyid,
                        principalTable: "corporatecompany",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_corporateinvoice_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateinvoice_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateinvoice_AspNetUsers_verifiedby",
                        column: x => x.verifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "corporateInvoicedetail",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false),
                    bookno = table.Column<string>(nullable: true),
                    from = table.Column<DateTime>(nullable: true),
                    to = table.Column<DateTime>(nullable: true),
                    provider = table.Column<string>(nullable: true),
                    supplier = table.Column<string>(nullable: true),
                    guest = table.Column<string>(nullable: true),
                    servicetype = table.Column<string>(nullable: true),
                    unit = table.Column<int>(nullable: false),
                    fare = table.Column<double>(nullable: false),
                    yq = table.Column<double>(nullable: false),
                    tax = table.Column<double>(nullable: false),
                    vat = table.Column<double>(nullable: false),
                    sc = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporateInvoicedetail", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporateInvoicedetail_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateInvoicedetail_corporateinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "corporateinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_corporateInvoicedetail_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "corporatesoa",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false),
                    balance = table.Column<double>(nullable: false),
                    details = table.Column<string>(nullable: true),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporatesoa", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporatesoa_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporatesoa_corporateinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "corporateinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_corporatesoa_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_corporatecompany_createdby",
                table: "corporatecompany",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecompany_currencyId",
                table: "corporatecompany",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecompany_modifiedby",
                table: "corporatecompany",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateinvoice_companyid",
                table: "corporateinvoice",
                column: "companyid");

            migrationBuilder.CreateIndex(
                name: "IX_corporateinvoice_createdby",
                table: "corporateinvoice",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateinvoice_modifiedby",
                table: "corporateinvoice",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateinvoice_verifiedby",
                table: "corporateinvoice",
                column: "verifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateInvoicedetail_createdby",
                table: "corporateInvoicedetail",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateInvoicedetail_invoiceid",
                table: "corporateInvoicedetail",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_corporateInvoicedetail_modifiedby",
                table: "corporateInvoicedetail",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatesoa_createdby",
                table: "corporatesoa",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatesoa_invoiceid",
                table: "corporatesoa",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_corporatesoa_modifiedby",
                table: "corporatesoa",
                column: "modifiedby");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "corporateInvoicedetail");

            migrationBuilder.DropTable(
                name: "corporatesoa");

            migrationBuilder.DropTable(
                name: "corporateinvoice");

            migrationBuilder.DropTable(
                name: "corporatecompany");
        }
    }
}
