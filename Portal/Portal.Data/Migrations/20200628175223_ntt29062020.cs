﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt29062020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "account",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "authcode",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "bookno",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "fromdate",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "fund",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "invoiceorder",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "org",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pax",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "program",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "project",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "provider",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "route",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "todate",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "total",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "unit",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "account",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "authcode",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "bookno",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "fromdate",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "fund",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "invoiceorder",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "org",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "pax",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "program",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "project",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "provider",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "route",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "todate",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "total",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "unit",
                table: "nyuInvoicedetail");
        }
    }
}
