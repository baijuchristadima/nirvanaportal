﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal12092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "aircode",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "suppref",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tclass",
                table: "corporateInvoicedetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "aircode",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "suppref",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "tclass",
                table: "corporateInvoicedetail");
        }
    }
}
