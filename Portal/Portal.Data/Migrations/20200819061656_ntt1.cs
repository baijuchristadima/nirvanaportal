﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "corporatecompany");

            migrationBuilder.AddColumn<string>(
                name: "firstaddress",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "forthaddress",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "regno",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "secondaddress",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thirdaddress",
                table: "corporatecompany",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "firstaddress",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "forthaddress",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "regno",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "secondaddress",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "thirdaddress",
                table: "corporatecompany");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "corporatecompany",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
