﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "supplier",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "unit",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "yq",
                table: "corporateInvoicedetail");

            migrationBuilder.AddColumn<string>(
                name: "route",
                table: "corporateInvoicedetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "route",
                table: "corporateInvoicedetail");

            migrationBuilder.AddColumn<string>(
                name: "supplier",
                table: "corporateInvoicedetail",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "unit",
                table: "corporateInvoicedetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "yq",
                table: "corporateInvoicedetail",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
