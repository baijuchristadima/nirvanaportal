﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal200902 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IDMUniqueID",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "billinginvoice",
                table: "corporateInvoicedetail");

            migrationBuilder.AddColumn<string>(
                name: "IDMUniqueID",
                table: "corporateinvoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "billinginvoice",
                table: "corporateinvoice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IDMUniqueID",
                table: "corporateinvoice");

            migrationBuilder.DropColumn(
                name: "billinginvoice",
                table: "corporateinvoice");

            migrationBuilder.AddColumn<string>(
                name: "IDMUniqueID",
                table: "corporateInvoicedetail",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "billinginvoice",
                table: "corporateInvoicedetail",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
