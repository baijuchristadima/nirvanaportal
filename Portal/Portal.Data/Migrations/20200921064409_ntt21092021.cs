﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt21092021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "uploadedcomments",
                table: "ghqinvoice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "uploadedon",
                table: "ghqinvoice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "uploadedcomments",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "uploadedon",
                table: "ghqinvoice");
        }
    }
}
