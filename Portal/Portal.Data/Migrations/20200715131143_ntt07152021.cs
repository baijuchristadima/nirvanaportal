﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt07152021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "todate",
                table: "nyuInvoicedetail",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "fromdate",
                table: "nyuInvoicedetail",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "bookno",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "code",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "fare",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "pax",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "refno",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "route",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "sc",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "tax",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "tic_class",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "yq",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "bookno",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "code",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "fare",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "pax",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "refno",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "route",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sc",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "tax",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "tic_class",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "yq",
                table: "ghqinvoicedetail");

            migrationBuilder.AlterColumn<string>(
                name: "todate",
                table: "nyuInvoicedetail",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "fromdate",
                table: "nyuInvoicedetail",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
