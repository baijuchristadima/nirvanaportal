﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt210920213 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "airlinenumcode",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dest",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "discount",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "issuedate",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "narration",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "orderno",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "piece",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "returndate",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec1",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec2",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec3",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec4",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec5",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec6",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec7",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sec8",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ticweight",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "total",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "traveldate",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "weight",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "weightunit",
                table: "ghqinvoicedetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "airlinenumcode",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "dest",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "discount",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "issuedate",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "narration",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "orderno",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "piece",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "returndate",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec1",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec2",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec3",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec4",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec5",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec6",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec7",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "sec8",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "ticweight",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "total",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "traveldate",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "weight",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "weightunit",
                table: "ghqinvoicedetail");
        }
    }
}
