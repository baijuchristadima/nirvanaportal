﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_corporatecompany_corporatecurrency_currencyId",
                table: "corporatecompany");

            migrationBuilder.DropTable(
                name: "corporatecurrency");

            migrationBuilder.DropIndex(
                name: "IX_corporatecompany_currencyId",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "currencyId",
                table: "corporatecompany");

            migrationBuilder.AddColumn<string>(
                name: "currency",
                table: "corporatecompany",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "currency",
                table: "corporatecompany");

            migrationBuilder.AddColumn<long>(
                name: "currencyId",
                table: "corporatecompany",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "corporatecurrency",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    createddate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ipaddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    modifiedby = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    modifieddate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<int>(type: "int", nullable: false),
                    unit = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporatecurrency", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporatecurrency_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporatecurrency_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_corporatecompany_currencyId",
                table: "corporatecompany",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrency_createdby",
                table: "corporatecurrency",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrency_modifiedby",
                table: "corporatecurrency",
                column: "modifiedby");

            migrationBuilder.AddForeignKey(
                name: "FK_corporatecompany_corporatecurrency_currencyId",
                table: "corporatecompany",
                column: "currencyId",
                principalTable: "corporatecurrency",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
