﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt210920215 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "lpoid",
                table: "ghqinvoicedetail");

            migrationBuilder.AddColumn<long>(
                name: "Corporatecompanyid",
                table: "corporatesoa",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Corporatecurrenciesid",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "corporatebatches",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    companyid = table.Column<long>(nullable: false),
                    fromdate = table.Column<DateTime>(nullable: true),
                    todate = table.Column<DateTime>(nullable: true),
                    closedby = table.Column<string>(nullable: true),
                    closedon = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporatebatches", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporatebatches_corporatecompany_companyid",
                        column: x => x.companyid,
                        principalTable: "corporatecompany",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_corporatebatches_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporatebatches_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "corporatecurrencies",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    currencyname = table.Column<string>(nullable: true),
                    minorunit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporatecurrencies", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporatecurrencies_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporatecurrencies_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "corporateuseassigns",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    userid = table.Column<long>(nullable: false),
                    Corporateinvoiceid = table.Column<long>(nullable: true),
                    companyid = table.Column<long>(nullable: false),
                    Corporatecompanyid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_corporateuseassigns", x => x.id);
                    table.ForeignKey(
                        name: "FK_corporateuseassigns_corporatecompany_Corporatecompanyid",
                        column: x => x.Corporatecompanyid,
                        principalTable: "corporatecompany",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateuseassigns_corporateinvoice_Corporateinvoiceid",
                        column: x => x.Corporateinvoiceid,
                        principalTable: "corporateinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateuseassigns_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_corporateuseassigns_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqhotelinvoicemains",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    lpodate = table.Column<DateTime>(nullable: false),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: false),
                    accveriuser = table.Column<long>(nullable: false),
                    accveridate = table.Column<DateTime>(nullable: false),
                    ghqveriuser = table.Column<long>(nullable: false),
                    ghqveridate = table.Column<DateTime>(nullable: false),
                    rejby = table.Column<long>(nullable: false),
                    rejdate = table.Column<DateTime>(nullable: false),
                    rejreason = table.Column<string>(nullable: true),
                    uploadnotes = table.Column<string>(nullable: true),
                    uploaduser = table.Column<int>(nullable: false),
                    uploaddate = table.Column<DateTime>(nullable: false),
                    batch = table.Column<int>(nullable: false),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqhotelinvoicemains", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqhotelinvoicemains_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotelinvoicemains_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqhotelsoas",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpoid = table.Column<long>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    lpodate = table.Column<DateTime>(nullable: false),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: false),
                    pax = table.Column<string>(nullable: true),
                    total = table.Column<double>(nullable: false),
                    balance = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqhotelsoas", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqhotelsoas_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotelsoas_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqticketlpodetails",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpoid = table.Column<long>(nullable: false),
                    lpodate = table.Column<DateTime>(nullable: false),
                    route = table.Column<string>(nullable: true),
                    pax = table.Column<string>(nullable: true),
                    refno = table.Column<string>(nullable: true),
                    lpoclass = table.Column<string>(nullable: true),
                    paxdob = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqticketlpodetails", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqticketlpodetails_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqticketlpodetails_ghqinvoice_lpoid",
                        column: x => x.lpoid,
                        principalTable: "ghqinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ghqticketlpodetails_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqhotelinvoicedetails",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpoid = table.Column<long>(nullable: false),
                    Ghqhotelinvoiceid = table.Column<long>(nullable: true),
                    supportdoc = table.Column<string>(nullable: true),
                    checkintime = table.Column<DateTime>(nullable: false),
                    checkouttime = table.Column<DateTime>(nullable: false),
                    serviceprovider = table.Column<string>(nullable: true),
                    suppliername = table.Column<string>(nullable: true),
                    passenger = table.Column<string>(nullable: true),
                    unit = table.Column<int>(nullable: false),
                    city = table.Column<string>(nullable: true),
                    roomtype = table.Column<string>(nullable: true),
                    fare = table.Column<double>(nullable: false),
                    servicefee = table.Column<double>(nullable: false),
                    taxone = table.Column<double>(nullable: false),
                    taxtwo = table.Column<double>(nullable: false),
                    total = table.Column<double>(nullable: false),
                    lpoorder = table.Column<int>(nullable: false),
                    xtraservice = table.Column<string>(nullable: true),
                    xtracode = table.Column<string>(nullable: true),
                    roomtypeid = table.Column<string>(nullable: true),
                    days = table.Column<int>(nullable: false),
                    invorder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqhotelinvoicedetails", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqhotelinvoicedetails_ghqhotelinvoicemains_Ghqhotelinvoiceid",
                        column: x => x.Ghqhotelinvoiceid,
                        principalTable: "ghqhotelinvoicemains",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotelinvoicedetails_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotelinvoicedetails_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqhotellpodetails",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpoid = table.Column<long>(nullable: false),
                    Ghqhotelinvoicemainsid = table.Column<long>(nullable: true),
                    reservation_number = table.Column<string>(nullable: true),
                    reservation_date_from = table.Column<DateTime>(nullable: false),
                    reservation_date_to = table.Column<DateTime>(nullable: false),
                    reservation_hour_from = table.Column<DateTime>(nullable: false),
                    reservation_hour_to = table.Column<DateTime>(nullable: false),
                    previouse_trx_no = table.Column<string>(nullable: true),
                    reservation_type = table.Column<string>(nullable: true),
                    vendor_number = table.Column<string>(nullable: true),
                    vendor_site = table.Column<string>(nullable: true),
                    to_who_concern = table.Column<string>(nullable: true),
                    according_name = table.Column<string>(nullable: true),
                    according_tel = table.Column<string>(nullable: true),
                    creator_id = table.Column<string>(nullable: true),
                    creation_date = table.Column<string>(nullable: true),
                    approve2_id = table.Column<string>(nullable: true),
                    approve2_date = table.Column<string>(nullable: true),
                    po_allowance_type = table.Column<string>(nullable: true),
                    po_allowance_id = table.Column<string>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    note3 = table.Column<string>(nullable: true),
                    note4 = table.Column<string>(nullable: true),
                    attribute1 = table.Column<string>(nullable: true),
                    attribute3 = table.Column<string>(nullable: true),
                    attribute4 = table.Column<string>(nullable: true),
                    attribute5 = table.Column<string>(nullable: true),
                    seq = table.Column<string>(nullable: true),
                    service_code = table.Column<string>(nullable: true),
                    service_name = table.Column<string>(nullable: true),
                    unit_no = table.Column<string>(nullable: true),
                    night_no = table.Column<string>(nullable: true),
                    unit_price = table.Column<double>(nullable: false),
                    tax_price = table.Column<double>(nullable: false),
                    added_tax = table.Column<double>(nullable: false),
                    amount = table.Column<double>(nullable: false),
                    note_1 = table.Column<string>(nullable: true),
                    from_date = table.Column<DateTime>(nullable: false),
                    to_date = table.Column<DateTime>(nullable: false),
                    attribute1_1 = table.Column<string>(nullable: true),
                    attribute2_1 = table.Column<string>(nullable: true),
                    attribute3_1 = table.Column<string>(nullable: true),
                    attribute4_1 = table.Column<string>(nullable: true),
                    attribute5_1 = table.Column<string>(nullable: true),
                    extra_service_code = table.Column<string>(nullable: true),
                    extra_service_name = table.Column<string>(nullable: true),
                    enclude_flag = table.Column<string>(nullable: true),
                    attribute3_2 = table.Column<string>(nullable: true),
                    attribute4_2 = table.Column<string>(nullable: true),
                    attribute5_2 = table.Column<string>(nullable: true),
                    seq_1 = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    night_no_1 = table.Column<string>(nullable: true),
                    from_date_1 = table.Column<string>(nullable: true),
                    to_date_1 = table.Column<string>(nullable: true),
                    attribute1_3 = table.Column<string>(nullable: true),
                    attribute2_3 = table.Column<string>(nullable: true),
                    attribute3_3 = table.Column<string>(nullable: true),
                    attribute4_3 = table.Column<string>(nullable: true),
                    attribute5_3 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqhotellpodetails", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqhotellpodetails_ghqhotelinvoicemains_Ghqhotelinvoicemainsid",
                        column: x => x.Ghqhotelinvoicemainsid,
                        principalTable: "ghqhotelinvoicemains",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotellpodetails_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqhotellpodetails_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ghqsoa_lpoid",
                table: "ghqsoa",
                column: "lpoid");

            migrationBuilder.CreateIndex(
                name: "IX_corporatesoa_Corporatecompanyid",
                table: "corporatesoa",
                column: "Corporatecompanyid");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecompany_Corporatecurrenciesid",
                table: "corporatecompany",
                column: "Corporatecurrenciesid");

            migrationBuilder.CreateIndex(
                name: "IX_corporatebatches_companyid",
                table: "corporatebatches",
                column: "companyid");

            migrationBuilder.CreateIndex(
                name: "IX_corporatebatches_createdby",
                table: "corporatebatches",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatebatches_modifiedby",
                table: "corporatebatches",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrencies_createdby",
                table: "corporatecurrencies",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporatecurrencies_modifiedby",
                table: "corporatecurrencies",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateuseassigns_Corporatecompanyid",
                table: "corporateuseassigns",
                column: "Corporatecompanyid");

            migrationBuilder.CreateIndex(
                name: "IX_corporateuseassigns_Corporateinvoiceid",
                table: "corporateuseassigns",
                column: "Corporateinvoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_corporateuseassigns_createdby",
                table: "corporateuseassigns",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_corporateuseassigns_modifiedby",
                table: "corporateuseassigns",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelinvoicedetails_Ghqhotelinvoiceid",
                table: "ghqhotelinvoicedetails",
                column: "Ghqhotelinvoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelinvoicedetails_createdby",
                table: "ghqhotelinvoicedetails",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelinvoicedetails_modifiedby",
                table: "ghqhotelinvoicedetails",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelinvoicemains_createdby",
                table: "ghqhotelinvoicemains",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelinvoicemains_modifiedby",
                table: "ghqhotelinvoicemains",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotellpodetails_Ghqhotelinvoicemainsid",
                table: "ghqhotellpodetails",
                column: "Ghqhotelinvoicemainsid");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotellpodetails_createdby",
                table: "ghqhotellpodetails",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotellpodetails_modifiedby",
                table: "ghqhotellpodetails",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelsoas_createdby",
                table: "ghqhotelsoas",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqhotelsoas_modifiedby",
                table: "ghqhotelsoas",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqticketlpodetails_createdby",
                table: "ghqticketlpodetails",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqticketlpodetails_lpoid",
                table: "ghqticketlpodetails",
                column: "lpoid");

            migrationBuilder.CreateIndex(
                name: "IX_ghqticketlpodetails_modifiedby",
                table: "ghqticketlpodetails",
                column: "modifiedby");

            migrationBuilder.AddForeignKey(
                name: "FK_corporatecompany_corporatecurrencies_Corporatecurrenciesid",
                table: "corporatecompany",
                column: "Corporatecurrenciesid",
                principalTable: "corporatecurrencies",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_corporatesoa_corporatecompany_Corporatecompanyid",
                table: "corporatesoa",
                column: "Corporatecompanyid",
                principalTable: "corporatecompany",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ghqsoa_ghqinvoice_lpoid",
                table: "ghqsoa",
                column: "lpoid",
                principalTable: "ghqinvoice",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_corporatecompany_corporatecurrencies_Corporatecurrenciesid",
                table: "corporatecompany");

            migrationBuilder.DropForeignKey(
                name: "FK_corporatesoa_corporatecompany_Corporatecompanyid",
                table: "corporatesoa");

            migrationBuilder.DropForeignKey(
                name: "FK_ghqsoa_ghqinvoice_lpoid",
                table: "ghqsoa");

            migrationBuilder.DropTable(
                name: "corporatebatches");

            migrationBuilder.DropTable(
                name: "corporatecurrencies");

            migrationBuilder.DropTable(
                name: "corporateuseassigns");

            migrationBuilder.DropTable(
                name: "ghqhotelinvoicedetails");

            migrationBuilder.DropTable(
                name: "ghqhotellpodetails");

            migrationBuilder.DropTable(
                name: "ghqhotelsoas");

            migrationBuilder.DropTable(
                name: "ghqticketlpodetails");

            migrationBuilder.DropTable(
                name: "ghqhotelinvoicemains");

            migrationBuilder.DropIndex(
                name: "IX_ghqsoa_lpoid",
                table: "ghqsoa");

            migrationBuilder.DropIndex(
                name: "IX_corporatesoa_Corporatecompanyid",
                table: "corporatesoa");

            migrationBuilder.DropIndex(
                name: "IX_corporatecompany_Corporatecurrenciesid",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "Corporatecompanyid",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "Corporatecurrenciesid",
                table: "corporatecompany");

            migrationBuilder.AddColumn<long>(
                name: "lpoid",
                table: "ghqinvoicedetail",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
