﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class _20200921100720_ntt210920214 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "nyusoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "datetran",
                table: "nyusoa",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "invoicedate",
                table: "nyusoa",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "invoiceno",
                table: "nyusoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ipono",
                table: "nyusoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "narration",
                table: "nyusoa",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "servicefee",
                table: "nyuInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "unit",
                table: "nyuInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "batchid",
                table: "nyuinvoice",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "ispaid",
                table: "nyuinvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "lpoid",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "pax",
                table: "ghqsoa",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "purpose",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "total",
                table: "ghqsoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "acceptedstatus",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "city_code",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "lpoid",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "service_charge",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "tic_date",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ticket_excess_baggage",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ticket_excess_baggage_pices",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ticket_order_lpo",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "tkt_type",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "update_status",
                table: "ghqinvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "zdisc",
                table: "ghqinvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "disputed",
                table: "ghqinvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "rejdate",
                table: "ghqinvoice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "rejreason",
                table: "ghqinvoice",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "rejuser",
                table: "ghqinvoice",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<double>(
                name: "balance",
                table: "corporatesoa",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "companyid",
                table: "corporatesoa",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "invoicedate",
                table: "corporatesoa",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "invoiceno",
                table: "corporatesoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ipo",
                table: "corporatesoa",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ipodate",
                table: "corporatesoa",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "narration",
                table: "corporatesoa",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "alcode",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "approver",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "checkintime",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "checkoutdate",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "city",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "costcenter",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "flightno",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "odr",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "passenger",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pnr",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "prjcode",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "providercode",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "roomtype",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "servicefree",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "serviceprovider",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "suppliername",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ticketdate",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "tikclass",
                table: "corporateInvoicedetail",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "total",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "unit",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "yq",
                table: "corporateInvoicedetail",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "attachpath",
                table: "corporateinvoice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "batch",
                table: "corporateinvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ispaid",
                table: "corporateinvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TRNNo",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "batch",
                table: "corporatecompany",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "currid",
                table: "corporatecompany",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "format",
                table: "corporatecompany",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "minorunit",
                table: "corporatecompany",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "secinv",
                table: "corporatecompany",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "balance",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "datetran",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "invoicedate",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "invoiceno",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "ipono",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "narration",
                table: "nyusoa");

            migrationBuilder.DropColumn(
                name: "servicefee",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "unit",
                table: "nyuInvoicedetail");

            migrationBuilder.DropColumn(
                name: "batchid",
                table: "nyuinvoice");

            migrationBuilder.DropColumn(
                name: "ispaid",
                table: "nyuinvoice");

            migrationBuilder.DropColumn(
                name: "balance",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "lpoid",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "pax",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "purpose",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "total",
                table: "ghqsoa");

            migrationBuilder.DropColumn(
                name: "acceptedstatus",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "city_code",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "lpoid",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "name",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "service_charge",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "tic_date",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "ticket_excess_baggage",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "ticket_excess_baggage_pices",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "ticket_order_lpo",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "tkt_type",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "update_status",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "zdisc",
                table: "ghqinvoicedetail");

            migrationBuilder.DropColumn(
                name: "disputed",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "rejdate",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "rejreason",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "rejuser",
                table: "ghqinvoice");

            migrationBuilder.DropColumn(
                name: "balance",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "companyid",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "invoicedate",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "invoiceno",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "ipo",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "ipodate",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "narration",
                table: "corporatesoa");

            migrationBuilder.DropColumn(
                name: "alcode",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "approver",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "checkintime",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "checkoutdate",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "city",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "costcenter",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "flightno",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "odr",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "passenger",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "pnr",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "prjcode",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "providercode",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "roomtype",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "servicefree",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "serviceprovider",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "suppliername",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "ticketdate",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "tikclass",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "total",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "unit",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "yq",
                table: "corporateInvoicedetail");

            migrationBuilder.DropColumn(
                name: "attachpath",
                table: "corporateinvoice");

            migrationBuilder.DropColumn(
                name: "batch",
                table: "corporateinvoice");

            migrationBuilder.DropColumn(
                name: "ispaid",
                table: "corporateinvoice");

            migrationBuilder.DropColumn(
                name: "TRNNo",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "address",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "batch",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "currid",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "format",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "minorunit",
                table: "corporatecompany");

            migrationBuilder.DropColumn(
                name: "secinv",
                table: "corporatecompany");
        }
    }
}
