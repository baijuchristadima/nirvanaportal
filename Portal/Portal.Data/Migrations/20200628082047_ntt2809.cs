﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class ntt2809 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ghqinvoice",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    lpodate = table.Column<DateTime>(nullable: true),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: true),
                    nttverifiedby = table.Column<string>(nullable: true),
                    nttverifiedon = table.Column<DateTime>(nullable: true),
                    ghqverifiedby = table.Column<string>(nullable: true),
                    ghqverifiedon = table.Column<DateTime>(nullable: true),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqinvoice", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqinvoice_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqinvoice_AspNetUsers_ghqverifiedby",
                        column: x => x.ghqverifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqinvoice_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqinvoice_AspNetUsers_nttverifiedby",
                        column: x => x.nttverifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqsoa",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    lpodate = table.Column<DateTime>(nullable: true),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: true),
                    balance = table.Column<double>(nullable: false),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqsoa", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqsoa_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqsoa_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "nyuinvoice",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    lpono = table.Column<string>(nullable: true),
                    invoiceno = table.Column<string>(nullable: true),
                    invoicedate = table.Column<DateTime>(nullable: true),
                    travelcode = table.Column<string>(nullable: true),
                    authcode = table.Column<string>(nullable: true),
                    verifiedby = table.Column<string>(nullable: true),
                    verifiedon = table.Column<DateTime>(nullable: true),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nyuinvoice", x => x.id);
                    table.ForeignKey(
                        name: "FK_nyuinvoice_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_nyuinvoice_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_nyuinvoice_AspNetUsers_verifiedby",
                        column: x => x.verifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ghqinvoicedetail",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ghqinvoicedetail", x => x.id);
                    table.ForeignKey(
                        name: "FK_ghqinvoicedetail_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ghqinvoicedetail_ghqinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "ghqinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ghqinvoicedetail_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "nyuInvoicedetail",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nyuInvoicedetail", x => x.id);
                    table.ForeignKey(
                        name: "FK_nyuInvoicedetail_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_nyuInvoicedetail_nyuinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "nyuinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_nyuInvoicedetail_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "nyusoa",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createdby = table.Column<string>(nullable: true),
                    modifiedby = table.Column<string>(nullable: true),
                    createddate = table.Column<DateTime>(nullable: false),
                    modifieddate = table.Column<DateTime>(nullable: false),
                    ipaddress = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    invoiceid = table.Column<long>(nullable: false),
                    balance = table.Column<double>(nullable: false),
                    paystatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nyusoa", x => x.id);
                    table.ForeignKey(
                        name: "FK_nyusoa_AspNetUsers_createdby",
                        column: x => x.createdby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_nyusoa_nyuinvoice_invoiceid",
                        column: x => x.invoiceid,
                        principalTable: "nyuinvoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_nyusoa_AspNetUsers_modifiedby",
                        column: x => x.modifiedby,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoice_createdby",
                table: "ghqinvoice",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoice_ghqverifiedby",
                table: "ghqinvoice",
                column: "ghqverifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoice_modifiedby",
                table: "ghqinvoice",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoice_nttverifiedby",
                table: "ghqinvoice",
                column: "nttverifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoicedetail_createdby",
                table: "ghqinvoicedetail",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoicedetail_invoiceid",
                table: "ghqinvoicedetail",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_ghqinvoicedetail_modifiedby",
                table: "ghqinvoicedetail",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqsoa_createdby",
                table: "ghqsoa",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_ghqsoa_modifiedby",
                table: "ghqsoa",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_nyuinvoice_createdby",
                table: "nyuinvoice",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_nyuinvoice_modifiedby",
                table: "nyuinvoice",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_nyuinvoice_verifiedby",
                table: "nyuinvoice",
                column: "verifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_nyuInvoicedetail_createdby",
                table: "nyuInvoicedetail",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_nyuInvoicedetail_invoiceid",
                table: "nyuInvoicedetail",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_nyuInvoicedetail_modifiedby",
                table: "nyuInvoicedetail",
                column: "modifiedby");

            migrationBuilder.CreateIndex(
                name: "IX_nyusoa_createdby",
                table: "nyusoa",
                column: "createdby");

            migrationBuilder.CreateIndex(
                name: "IX_nyusoa_invoiceid",
                table: "nyusoa",
                column: "invoiceid");

            migrationBuilder.CreateIndex(
                name: "IX_nyusoa_modifiedby",
                table: "nyusoa",
                column: "modifiedby");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ghqinvoicedetail");

            migrationBuilder.DropTable(
                name: "ghqsoa");

            migrationBuilder.DropTable(
                name: "nyuInvoicedetail");

            migrationBuilder.DropTable(
                name: "nyusoa");

            migrationBuilder.DropTable(
                name: "ghqinvoice");

            migrationBuilder.DropTable(
                name: "nyuinvoice");
        }
    }
}
