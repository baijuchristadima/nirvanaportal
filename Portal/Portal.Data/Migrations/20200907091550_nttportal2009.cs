﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Portal.Data.Migrations
{
    public partial class nttportal2009 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rmdatavalue_corporateinvoice_invoiceid",
                table: "rmdatavalue");

            migrationBuilder.DropIndex(
                name: "IX_rmdatavalue_invoiceid",
                table: "rmdatavalue");

            migrationBuilder.DropColumn(
                name: "invoiceid",
                table: "rmdatavalue");

            migrationBuilder.AddColumn<long>(
                name: "detailid",
                table: "rmdatavalue",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_rmdatavalue_detailid",
                table: "rmdatavalue",
                column: "detailid");

            migrationBuilder.AddForeignKey(
                name: "FK_rmdatavalue_corporateInvoicedetail_detailid",
                table: "rmdatavalue",
                column: "detailid",
                principalTable: "corporateInvoicedetail",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rmdatavalue_corporateInvoicedetail_detailid",
                table: "rmdatavalue");

            migrationBuilder.DropIndex(
                name: "IX_rmdatavalue_detailid",
                table: "rmdatavalue");

            migrationBuilder.DropColumn(
                name: "detailid",
                table: "rmdatavalue");

            migrationBuilder.AddColumn<long>(
                name: "invoiceid",
                table: "rmdatavalue",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_rmdatavalue_invoiceid",
                table: "rmdatavalue",
                column: "invoiceid");

            migrationBuilder.AddForeignKey(
                name: "FK_rmdatavalue_corporateinvoice_invoiceid",
                table: "rmdatavalue",
                column: "invoiceid",
                principalTable: "corporateinvoice",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
