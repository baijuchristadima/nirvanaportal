﻿using Nirvana_Portal.Data;
using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Nirvana_Portal.Data.Nyu
{
   public class nyusoa  :auditdetail
{
        [ForeignKey("nyuinvoice")]
        public Int64 invoiceid { get; set; }
        public virtual nyuinvoice nyuinvoice { get; set; }
        public double debit { get; set; }
        public double credit { get; set; }
        public int paystatus { get; set; }
        public DateTime invoicedate { get; set; }
        public string invoiceno { get; set; }
        public string ipono { get; set; }
        public DateTime datetran { get; set; }
        public double balance { get; set; }
        public string narration { get; set; }
    }
}
