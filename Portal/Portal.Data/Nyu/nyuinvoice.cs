﻿using Portal.Data;
using Portal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Transactions;

namespace Nirvana_Portal.Data.Nyu
{
    public class nyuinvoice :auditdetail
    {
        public string lpono { get; set; }
     
        public string invoiceno { get; set; }
        public DateTime? invoicedate { get; set; }
        public string travelcode { get; set; }
        public string authcode { get; set; }

        [ForeignKey("verifieduser")]
        public string verifiedby { get; set; }
        public virtual ApplicationUser verifieduser { get; set; }
        public DateTime? verifiedon { get; set; }        
        public int paystatus { get; set; }
        public int ispaid { get; set; }
        public Int64 batchid { get; set; }

    }

    public class nyuInvoicedetail :auditdetail
    {
        [ForeignKey("nyuinvoice")]
        public Int64 invoiceid { get; set; }
        public virtual nyuinvoice nyuinvoice { get; set; }
        public string invoiceorder { get; set; }
        public string servicetype { get; set; }
        public string bookno { get; set; }
        public string pax { get; set; }
        public DateTime? fromdate { get; set; }
        public DateTime? todate { get; set; }
        public string provider { get; set; }
        public string route { get; set; }        
        public double fare { get; set; }
        public double tax { get; set; }
        public double vat { get; set; }
        public double total { get; set; }
        public string authcode { get; set; }
        public string account { get; set; }
        public string fund { get; set; }       
        public string org { get; set; }
        public string project { get; set; }
        public string program { get; set; }         
        public string dest { get; set; }
        public int paystatus { get; set; }
        public string unit { get; set; }
        public double servicefee { get; set; }
    }
}
