﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Portal.Data.Nyu
{
   public class nyuperiods : auditdetail
    {
        public DateTime fromate { get; set; }
        public DateTime todate { get; set; }
        public DateTime closedon { get; set; }
        [ForeignKey("closedUser")]
        public string closedBy { get; set; }
        public virtual ApplicationUser closedUser { get; set; }
    }
}
