﻿using Nirvana_Portal.Data.Ghq;
using Portal.Data;
using Portal.Data.Ghq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Service.Interface
{
    public interface IGhqService
    {
        List<ghqinvoice> GetStaffverificationload();
        List<ghqinvoice> GetCustomerInvoicesLoad();
        List<ghqinvoice> GetCustomerApprovedInvoicesLoad();
        List<ghqsoa> GetSOALoad();
        List<ghqinvoicedetail> GetInvoiceDetail(long invoiceid);

        ghqinvoice GetInvoice(long invoiceid);


        List<ghqinvoicedetail> GhqInvoicedetails(long id);

        void UpdateGhqInvoicedetails(ghqinvoicedetail ghqinvoicedetail);

        ghqticketlpodetails AddGhqticketlpodetails(ghqticketlpodetails ghqticketlpodetail);

        List<ghqticketlpodetails> GetLpoDetail(long invoiceid);

        List<ghqinvoicedetail> GetInvoiceDetail();

        void Updatecorporateinvoice(ghqinvoice ghqinvoice);
        ghqinvoice AddGhqinvoice(ghqinvoice ghqinvoice);

        List<ghqinvoice> GetUploadList();
        void AddGhqInvoicedetails(ghqinvoicedetail ghqinvoicedetail);

    }

    
}
