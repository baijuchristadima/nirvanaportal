﻿using Portal.Data;
using Portal.Data.Corporate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Service.Interface
{
    public interface ICorporateService
    {
        #region DataLoad
       
        List<corporatecompany> GetCorporateLoad();
        List<corporateinvoice> GetStaffverificationload();
        List<corporateinvoice> GetCustomerInvoicesLoad();        
        List<corporatesoa> GetSOALoad();
        List<corporateInvoicedetail> GetInvoiceDetail(long invoiceid);

        corporateinvoice GetInvoice(long invoiceid);

        List<corporateInvoicedetail> GetInvoiceDetail();

        void UpdateInvoicedetails(corporateInvoicedetail corporateInvoicedetail);
        void Updatecorporatecompany(corporatecompany corporatecompany);

        void Updatecorporateinvoice(corporateinvoice corporateinvoice);
        void AddInvoicedetails(corporateInvoicedetail corporateInvoicedetail);
        List<rmdatavalue> GetrmdatavalueDetail();
        void Updatermdatavalue(rmdatavalue rmdatavalue);
        #endregion
    }
}
