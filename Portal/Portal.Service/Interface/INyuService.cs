﻿using Nirvana_Portal.Data.Nyu;
using Portal.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Portal.Service.Interface
{
    public interface INyuService
    {       
        List<nyuinvoice> GetStaffverificationload();
        List<nyuinvoice> GetCustomerInvoicesLoad();
        List<nyusoa> GetSOALoad();
        List<nyuInvoicedetail> GetInvoiceDetail(long invoiceid);
        nyuinvoice GetInvoice(long invoiceid);
        List<nyuInvoicedetail> GetInvoiceDetail();
        void UpdateInvoicedetails(nyuInvoicedetail nyuInvoicedetail);
        void AddInvoicedetails(nyuInvoicedetail nyuInvoicedetail);
        void Updatecorporateinvoice(nyuinvoice nyuinvoice);
       
    }
}
