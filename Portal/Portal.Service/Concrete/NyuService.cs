﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nirvana_Portal.Data.Nyu;
using Portal.Data;
using Portal.Repository;
using Portal.Service.Interface;

namespace Portal.Service.Concrete
{

    public class NyuService : INyuService
    {
        private IRepository<nyuinvoice> NyuInvoiceRepository;
        private IRepository<nyuInvoicedetail> NyuInvoicedetailsRepository;
        private IRepository<nyusoa> NyuSoaRepository;        
        public NyuService(IRepository<nyuinvoice> nyuinvoiceRepository, IRepository<nyuInvoicedetail> nyuInvoicedetailRepository, IRepository<nyusoa> nyusoaRepository)
        {
            this.NyuInvoiceRepository = nyuinvoiceRepository;
            this.NyuInvoicedetailsRepository = nyuInvoicedetailRepository;
            this.NyuSoaRepository = nyusoaRepository;
           

        }
        public List<nyuinvoice> GetStaffverificationload()
        {
            return NyuInvoiceRepository.GetAll().Where(x => x.verifiedby == null && x.verifiedon == null && x.paystatus==1).OrderBy(x => x.id).ToList();
        }
        public List<nyuinvoice> GetCustomerInvoicesLoad()
        {
            return NyuInvoiceRepository.GetAll().Where(x => x.status > 0 && x.verifiedby != null && x.verifiedon != null).Include(b => b.verifieduser).OrderBy(x => x.modifieddate).ToList();
        }
        public List<nyusoa> GetSOALoad()
        {
            return NyuSoaRepository.GetAll().Where(x => x.status > 0 && x.paystatus==1).OrderBy(x => x.id).Include(x=>x.nyuinvoice).ToList();
        }
        public List<nyuInvoicedetail> GetInvoiceDetail(long invoiceid)
        {
            return NyuInvoicedetailsRepository.GetAll().Where(x=>x.invoiceid== invoiceid).Include(x => x.nyuinvoice).ToList();
        }
        public nyuinvoice GetInvoice(long invoiceid)
        {
            return NyuInvoiceRepository.Get(invoiceid);
        }

        public List<nyuInvoicedetail> GetInvoiceDetail()
        {
            return NyuInvoicedetailsRepository.GetAll().Include(x => x.nyuinvoice).ToList();
        }
        public void UpdateInvoicedetails(nyuInvoicedetail nyuInvoicedetail)
        {
            NyuInvoicedetailsRepository.Update(nyuInvoicedetail);
        }

        public void AddInvoicedetails(nyuInvoicedetail nyuInvoicedetail)
        {
            NyuInvoicedetailsRepository.Insert(nyuInvoicedetail);
        }
        public void Updatecorporateinvoice(nyuinvoice nyuinvoice)
        {
            NyuInvoiceRepository.Update(nyuinvoice);
        }        
    }

}
