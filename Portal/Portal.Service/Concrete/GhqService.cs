﻿using Microsoft.EntityFrameworkCore;
using Nirvana_Portal.Data.Ghq;
using Portal.Data;
using Portal.Data.Ghq;
using Portal.Repository;
using Portal.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Portal.Service.Concrete
{
    public class GhqService : IGhqService
    {
        private IRepository<ghqinvoice> GhqInvoiceRepository;
        private IRepository<ghqinvoicedetail> GhqInvoicedetailsRepository;
        private IRepository<ghqsoa> GhqSoaRepository;
        private IRepository<ghqticketlpodetails> Ghqticketlpodetails;
       
        public GhqService(IRepository<ghqinvoice> ghqinvoiceRepository, IRepository<ghqinvoicedetail> ghqInvoicedetailRepository, IRepository<ghqsoa> ghqsoaRepository, IRepository<ghqticketlpodetails> ghqticketlpodetails)
        {
            this.GhqInvoiceRepository = ghqinvoiceRepository;
            this.GhqInvoicedetailsRepository = ghqInvoicedetailRepository;
            this.GhqSoaRepository = ghqsoaRepository;
            this.Ghqticketlpodetails = ghqticketlpodetails;
        }

        public List<ghqinvoice> GetStaffverificationload()
        {
            return GhqInvoiceRepository.GetAll().Where(x => x.nttverifiedby == null && x.nttverifiedon == null && x.paystatus==1).OrderBy(x => x.id).ToList();
        }
        public List<ghqinvoice> GetCustomerInvoicesLoad()
        {
            List<ghqinvoice> list = GhqInvoiceRepository.GetAll().Where(x => x.status > 0 && x.nttverifiedby != null && x.nttverifiedon != null && x.ghqverifiedon == null && x.ghqverifiedby == null && x.paystatus == 1).Include(b => b.accveriuser).OrderBy(x => x.nttverifiedon).ToList();
            return list;
        }
        public List<ghqinvoice> GetCustomerApprovedInvoicesLoad()
        {
            List<ghqinvoice> list = GhqInvoiceRepository.GetAll().Where(x => x.status > 0 && x.nttverifiedby != null && x.nttverifiedon != null && x.ghqverifiedby != null && x.ghqverifiedon != null && x.paystatus == 1).Include(b => b.accveriuser).OrderBy(x => x.nttverifiedon).ToList();
            return list;
        }

        public List<ghqsoa> GetSOALoad()
        {
            List<ghqsoa> list = new List<ghqsoa>();
           list = GhqSoaRepository.GetAll().Where(x => x.status > 0).OrderBy(x => x.id).ToList();
            return list;
        }
        public List<ghqinvoicedetail> GetInvoiceDetail()
        {
            return GhqInvoicedetailsRepository.GetAll().Where(x=>x.status>0).Include(x=>x.ghqinvoice).ToList();
        }
        public List<ghqinvoicedetail> GetInvoiceDetail(long invoiceid)
        {
            return GhqInvoicedetailsRepository.GetAll().Where(x=>(x.invoiceid== invoiceid) &&(x.status != 0)).Include(x=>x.ghqinvoice).ToList();
        }

        public ghqinvoice GetInvoice(long invoiceid)
        {
            return GhqInvoiceRepository.Get(invoiceid);
        }          

        public List<ghqinvoicedetail> GhqInvoicedetails (long id)
        {
            return GhqInvoicedetailsRepository.GetAll().Where(x=>x.id==id).ToList();
        }
        public void UpdateGhqInvoicedetails(ghqinvoicedetail ghqinvoicedetail)
        {
            GhqInvoicedetailsRepository.Update(ghqinvoicedetail);
        }

        public ghqticketlpodetails AddGhqticketlpodetails(ghqticketlpodetails ghqticketlpodetail)
        {
            Ghqticketlpodetails.Insert(ghqticketlpodetail);
            return ghqticketlpodetail;
        }

        public List<ghqticketlpodetails> GetLpoDetail(long invoiceid)
        {
            return Ghqticketlpodetails.GetAll().Where(x => x.lpoid == invoiceid).Include(x => x.ghqinvoice).ToList();
        }

        public void Updatecorporateinvoice(ghqinvoice ghqinvoice)
        {
            GhqInvoiceRepository.Update(ghqinvoice);
        }
        public ghqinvoice AddGhqinvoice(ghqinvoice ghqinvoice)
        {
            GhqInvoiceRepository.Insert(ghqinvoice);
            return ghqinvoice;
        }

        public List<ghqinvoice> GetUploadList()
        {
            return GhqInvoiceRepository.GetAll().Where(x => x.uploadedby != null && x.uploadedon != null && x.status >= 1).Include(x=>x.uploaduser).OrderBy(x => x.id).ToList();
        }

        public void AddGhqInvoicedetails(ghqinvoicedetail ghqinvoicedetail)
        {
            GhqInvoicedetailsRepository.Insert(ghqinvoicedetail);
        }
    }
}
