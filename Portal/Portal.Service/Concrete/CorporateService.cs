﻿using Microsoft.EntityFrameworkCore;
using Portal.Data;
using Portal.Data.Corporate;
using Portal.Repository;
using Portal.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Service.Concrete
{
    public class CorporateService : ICorporateService
    {

        private IRepository<corporatecompany> CorporatecompanyRepository;
        private IRepository<corporateinvoice> CorporateInvoiceRepository;
        private IRepository<corporateInvoicedetail> CorporateInvoiceDetailRepository;
        private IRepository<corporatesoa> CorporateSOARepository;
        private IRepository<rmdatavalue> RmdatavalueRepository;
      
        public CorporateService(IRepository<corporatecompany> corporatecompanyRepository, IRepository<corporateinvoice> corporateInvoiceRepository, IRepository<corporateInvoicedetail> corporateInvoiceDetailRepository, IRepository<corporatesoa> corporateSOARepository, IRepository<rmdatavalue> rmdatavalueRepository)
        {
            this.CorporatecompanyRepository = corporatecompanyRepository;
            this.CorporateInvoiceRepository = corporateInvoiceRepository;
            this.CorporateInvoiceDetailRepository = corporateInvoiceDetailRepository;
            this.CorporateSOARepository = corporateSOARepository;
            this.RmdatavalueRepository = rmdatavalueRepository;           
        }
        public List<corporatecompany> GetCorporateLoad()
        {
            return CorporatecompanyRepository.GetAll().Include(x => x.createduser).OrderBy(x => x.id).ToList();
        }
        public List<corporateinvoice> GetStaffverificationload()
        {
            return CorporateInvoiceRepository.GetAll().Where(x => x.verifiedby == null && x.verifiedon == null && x.paystatus == 1).OrderBy(x => x.id).Include(x => x.corporatecompany).ToList();
        }
        public List<corporateinvoice> GetCustomerInvoicesLoad()
        {
            return CorporateInvoiceRepository.GetAll().Where(x => x.status > 0 && x.verifiedby != null && x.verifiedon != null).Include(b => b.corporatecompany).Include(b => b.verifieduser).OrderBy(x => x.modifieddate).ToList();
        }
        public List<corporatesoa> GetSOALoad()
        {
            return CorporateSOARepository.GetAll().Where(x => x.status > 0 && x.paystatus == 1).Include(b => b.corporateinvoice).Include(b => b.corporateinvoice.corporatecompany).OrderBy(x => x.id).ToList();
        }
        public List<corporateInvoicedetail> GetInvoiceDetail(long invoiceid)
        {
            return CorporateInvoiceDetailRepository.GetAll().Where(x => x.invoiceid == invoiceid).Include(x => x.corporateinvoice).ToList();
        }
        public corporateinvoice GetInvoice(long invoiceid)
        {
            return CorporateInvoiceRepository.Get(invoiceid);
        }

        public List<corporateInvoicedetail> GetInvoiceDetail()
        {
            return CorporateInvoiceDetailRepository.GetAll().Include(x => x.corporateinvoice).Include(x => x.corporateinvoice.corporatecompany).ToList();
        }
        public void UpdateInvoicedetails(corporateInvoicedetail corporateInvoicedetail)
        {
            CorporateInvoiceDetailRepository.Update(corporateInvoicedetail);
        }

        public void AddInvoicedetails(corporateInvoicedetail corporateInvoicedetail)
        {
            CorporateInvoiceDetailRepository.Insert(corporateInvoicedetail);
        }

        public void Updatecorporatecompany(corporatecompany corporatecompany)
        {
            CorporatecompanyRepository.Update(corporatecompany);
        }

        public void Updatecorporateinvoice(corporateinvoice corporateinvoice)
        {
            CorporateInvoiceRepository.Update(corporateinvoice);
        }
        public List<rmdatavalue> GetrmdatavalueDetail()
        {
            return RmdatavalueRepository.GetAll().Include(x => x.corporateInvoicedetail).ToList();
        }
        public void Updatermdatavalue(rmdatavalue rmdatavalue)
        {
            RmdatavalueRepository.Update(rmdatavalue);
        }


    }
}
