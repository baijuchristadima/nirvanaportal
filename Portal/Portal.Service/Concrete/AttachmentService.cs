﻿using Portal.Data;
using Portal.Repository;
using Portal.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Service.Concrete
{
   public class AttachmentService : IAttachmentService
    {
        private IRepository<attachment> AttachmentRepository;
        public AttachmentService(IRepository<attachment> attachmentRepository)
        {           
            this.AttachmentRepository = attachmentRepository;
        }
        public List<attachment> Getinforattachments()
        {
            return AttachmentRepository.GetAll().ToList();
        }
    }
}
