//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NirvanaReports
{
    using System;
    using System.Collections.Generic;
    
    public partial class corporateInvoicedetail
    {
        public long id { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
        public System.DateTime createddate { get; set; }
        public System.DateTime modifieddate { get; set; }
        public string ipaddress { get; set; }
        public int status { get; set; }
        public long invoiceid { get; set; }
        public string bookno { get; set; }
        public Nullable<System.DateTime> from { get; set; }
        public Nullable<System.DateTime> to { get; set; }
        public string provider { get; set; }
        public string guest { get; set; }
        public string servicetype { get; set; }
        public double fare { get; set; }
        public double tax { get; set; }
        public double vat { get; set; }
        public double sc { get; set; }
        public string route { get; set; }
        public int billinginvoiceLine { get; set; }
        public Nullable<System.DateTime> issuedate { get; set; }
        public string type { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual corporateinvoice corporateinvoice { get; set; }
    }
}
