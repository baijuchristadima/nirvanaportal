//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NirvanaReports
{
    using System;
    using System.Collections.Generic;
    
    public partial class nyusoa
    {
        public long id { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
        public System.DateTime createddate { get; set; }
        public System.DateTime modifieddate { get; set; }
        public string ipaddress { get; set; }
        public int status { get; set; }
        public long invoiceid { get; set; }
        public double balance { get; set; }
        public int paystatus { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual nyuinvoice nyuinvoice { get; set; }
    }
}
