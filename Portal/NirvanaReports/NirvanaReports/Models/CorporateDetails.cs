﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NirvanaReports.Models
{
    public class CorporateDetails
    {
        public long id { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
        public System.DateTime createddate { get; set; }
        public System.DateTime modifieddate { get; set; }
        public string ipaddress { get; set; }
        public int status { get; set; }
        public long invoiceid { get; set; }
        public string bookno { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public string provider { get; set; }
        public string guest { get; set; }
        public string servicetype { get; set; }
        public double fare { get; set; }
        public double tax { get; set; }
        public double vat { get; set; }
        public double sc { get; set; }
        public string route { get; set; }
        public int billinginvoiceLine { get; set; }
        public DateTime issuedate { get; set; }
        public string type { get; set; }
    }
}