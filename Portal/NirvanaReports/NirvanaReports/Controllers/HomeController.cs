﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using NirvanaReports.Models;

namespace NirvanaReports.Controllers
{
    public class HomeController : Controller
    {
        Db_NTTPortalTestEntities1 _context;

        public ActionResult GetCorporateInvoice(long Id = 3270)
        {
            _context = new Db_NTTPortalTestEntities1();
            List<CorporateDetails> allCustomer = new List<CorporateDetails>();
            var corporateinvoice = _context.corporateinvoices.Where(x => x.id == Id).FirstOrDefault();
            allCustomer = _context.corporateInvoicedetails.Where(x => x.invoiceid == Id).Select(x => new CorporateDetails
            {
                id = x.id,
                invoiceid = x.invoiceid,
                customercode = corporateinvoice.corporatecompany.code,
                customername = corporateinvoice.corporatecompany.name,
                customerphone = corporateinvoice.corporatecompany.phone,
                customeremail = corporateinvoice.corporatecompany.mail,
                customertrnno = corporateinvoice.corporatecompany.regno,
                invoiceno = corporateinvoice.invoiceno,
                lpono = corporateinvoice.lpono,
                invoicedate = corporateinvoice.invoicedate == null ? DateTime.Now : corporateinvoice.invoicedate.Value,
                lpodate = corporateinvoice.lpodate == null ? DateTime.Now : corporateinvoice.lpodate.Value,
                guest = x.guest,
                servicetype = x.servicetype,
                from = x.from == null ? DateTime.Now : x.from.Value,
                to = x.to == null ? DateTime.Now : x.to.Value,
                billinginvoiceLine = x.billinginvoiceLine,
                tax = x.tax,
                vat = x.vat,
                sc = x.sc,
                provider = x.provider,
                issuedate = x.issuedate == null ? DateTime.Now : x.issuedate.Value,
                fare = x.fare,
                bookno = x.bookno,
                route = x.route,
                total = x.fare + x.sc + x.tax + x.vat
            }).ToList();


            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), getInvoiceFormat(corporateinvoice.corporatecompany.invformat)));

            rd.SetDataSource(allCustomer);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", Regex.Replace(corporateinvoice.invoiceno, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + ".pdf");
        }

        public ActionResult GetNYUInvoice(long Id = 3270)
        {
            _context = new Db_NTTPortalTestEntities1();
            List<CorporateDetails> allCustomer = new List<CorporateDetails>();
            var nyuinvoices = _context.nyuinvoices.Where(x => x.id == Id).FirstOrDefault();
            allCustomer = _context.nyuInvoicedetails.Where(x => x.invoiceid == Id).Select(x => new CorporateDetails
            {
                id = x.id,
                invoiceid = x.invoiceid,
                invoiceno = nyuinvoices.invoiceno,
                lpono = nyuinvoices.lpono,
                invoicedate = nyuinvoices.invoicedate == null ? DateTime.Now : nyuinvoices.invoicedate.Value,
                guest = x.pax,
                servicetype = x.servicetype,
                from = x.fromdate == null ? DateTime.Now : x.fromdate.Value,
                to = x.todate == null ? DateTime.Now : x.todate.Value,
                invoiceorder = x.invoiceorder,
                tax = x.tax,
                vat = x.vat,
                provider = x.provider,
                fare = x.fare,
                bookno = x.bookno,
                route = x.route,
                program = x.program,
                project = x.project,
                fund = x.fund,
                account = x.account,
                org = x.org,
                total = x.fare + x.tax + x.vat
            }).ToList();


            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "NyuInvoices.rpt"));

            rd.SetDataSource(allCustomer);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", Regex.Replace(nyuinvoices.invoiceno, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + ".pdf");
        }

        public ActionResult GetGHQInvoice(long Id = 3270)
        {
            _context = new Db_NTTPortalTestEntities1();
            List<CorporateDetails> allCustomer = new List<CorporateDetails>();
            var corporateinvoice = _context.ghqinvoices.Where(x => x.id == Id).FirstOrDefault();
            allCustomer = _context.ghqinvoicedetails.Where(x => x.invoiceid == Id).Select(x => new CorporateDetails
            {
                id = x.id,
                invoiceid = x.invoiceid,
                invoiceno = corporateinvoice.invoiceno,
                lpono = corporateinvoice.lpono,
                invoicedate = corporateinvoice.invoicedate == null ? DateTime.Now : corporateinvoice.invoicedate.Value,
                lpodate = corporateinvoice.lpodate == null ? DateTime.Now : corporateinvoice.lpodate.Value,
                guest = x.pax,
                tic_class = x.tic_class,
                tax = x.tax,
                vat = 0,
                sc = x.sc,
                provider = x.code,
                fare = x.fare,
                bookno = x.bookno,
                route = x.route,
                yq = x.yq,
                total = x.fare + x.sc + x.tax
            }).ToList();


            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), getInvoiceFormat("GHQInvoices.rpt")));

            rd.SetDataSource(allCustomer);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", Regex.Replace(corporateinvoice.invoiceno, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + ".pdf");
        }

        private string getInvoiceFormat(string format)
        {
            switch (format)
            {
                case "1": return "CorporateInvoice1.rpt";
                case "2": return "CorporateInvoice2.rpt";
                case "3": return "CorporateInvoice3.rpt";
                case "4": return "CorporateInvoice4.rpt";
                case "5": return "CorporateInvoice5.rpt";
                case "6": return "CorporateInvoice6.rpt";
                case "7": return "CorporateInvoice7.rpt";
                default: return "CorporateInvoice1.rpt";
            }
        }
        public class CorporateDetails
        {
            public long id { get; set; }
            public long invoiceid { get; set; }
            public string customercode { get; set; }
            public string customername { get; set; }
            public string customerphone { get; set; }
            public string customeremail { get; set; }
            public string customertrnno { get; set; }
            public string invoiceno { get; set; }
            public DateTime invoicedate { get; set; }
            public string lpono { get; set; }
            public DateTime lpodate { get; set; }
            public string guest { get; set; }
            public string servicetype { get; set; }
            public DateTime from { get; set; }
            public DateTime to { get; set; }
            public string bookno { get; set; }
            public double fare { get; set; }
            public double tax { get; set; }
            public double vat { get; set; }
            public double sc { get; set; }
            public double total { get; set; }
            public string provider { get; set; }
            public string route { get; set; }
            public int billinginvoiceLine { get; set; }
            public DateTime issuedate { get; set; }
            public string type { get; set; }
            public string invoiceorder { get; set; }
            public string program { get; set; }
            public string project { get; set; }
            public string fund { get; set; }
            public string account { get; set; }
            public string org { get; set; }
            public string tic_class { get; set; }
            public double yq { get; set; }
        }

    }
}