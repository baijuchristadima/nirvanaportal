//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NirvanaReports
{
    using System;
    using System.Collections.Generic;
    
    public partial class nyuinvoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public nyuinvoice()
        {
            this.nyuInvoicedetails = new HashSet<nyuInvoicedetail>();
            this.nyusoas = new HashSet<nyusoa>();
        }
    
        public long id { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
        public System.DateTime createddate { get; set; }
        public System.DateTime modifieddate { get; set; }
        public string ipaddress { get; set; }
        public int status { get; set; }
        public string lpono { get; set; }
        public string invoiceno { get; set; }
        public Nullable<System.DateTime> invoicedate { get; set; }
        public string travelcode { get; set; }
        public string authcode { get; set; }
        public string verifiedby { get; set; }
        public Nullable<System.DateTime> verifiedon { get; set; }
        public int paystatus { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual AspNetUser AspNetUser2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<nyuInvoicedetail> nyuInvoicedetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<nyusoa> nyusoas { get; set; }
    }
}
